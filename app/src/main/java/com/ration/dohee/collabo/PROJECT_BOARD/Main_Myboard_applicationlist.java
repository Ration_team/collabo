package com.ration.dohee.collabo.PROJECT_BOARD;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.ration.dohee.collabo.Collabocreate_Activity;
import com.ration.dohee.collabo.DATA.ActionBarData;
import com.ration.dohee.collabo.DATA.UNIQ;
import com.ration.dohee.collabo.DATA.User_INFO;
import com.ration.dohee.collabo.Https.Apply_Results_Box.Apply_create;
import com.ration.dohee.collabo.Https.Apply_Results_Box.Apply_list;
import com.ration.dohee.collabo.Https.Apply_Results_Box.Apply_member;
import com.ration.dohee.collabo.Https.HttpServiceClient;
import com.ration.dohee.collabo.R;
import com.ration.dohee.collabo.Util.ActionBar;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Main_Myboard_applicationlist extends ActionBar {


    Recruit_Apply_List_Adapter adapter;

    public static String data;

    @BindView(R.id.collabocreatedbtn)
    Button collabocreatedbtn;
    TextView noguesttext;
    Apply_list Result;
    ArrayList<Apply_member> master;
    ArrayList<Apply_member> member;
    @BindView(R.id.applicationlist)
    RecyclerView applicationlist;
    LinearLayoutManager mLayoutManager;
    @BindView(R.id.guestnumber)
    TextView guestnumber;

    int number;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main__myboard_applicationlist);
        ButterKnife.bind(this);
        SetActionBar(findViewById(ActionBarData.ActionBar));
        number=0;
        noguesttext = (TextView) findViewById(R.id.noguesttext);
        Result = new Apply_list();
        master = new ArrayList<>();
        member = new ArrayList<>();
        adapter = new Recruit_Apply_List_Adapter(getApplicationContext(),guestnumber);

        guestnumber.setText("'"+number+"명'선택됨");
//        applicationlist = (RecyclerView) findViewById(R.id.applicationlist);
//        collabocreatedbtn = (Button) findViewById(R.id.collabocreatedbtn);

        applicationlist.setHasFixedSize(false);


        mLayoutManager = new LinearLayoutManager(this);
        applicationlist.setLayoutManager(mLayoutManager);

        Retros();

    }


    @OnClick(R.id.collabocreatedbtn)
    public void onViewClicked() {
        boolean datacheck = false;
        StringBuilder builder = new StringBuilder();
        final StringBuilder builderforpost = new StringBuilder();

        String data = null;
        String depart = "|";
        for (Apply_member item : adapter.getItems()) {
            if (adapter.getItems().size() == 1) {
                depart = "";
            }
            if (item.isChecked()) {
                datacheck = true;
                builder.append(item.getMb_id() + "\r\n");

                builderforpost.append(item.getMb_id()+depart);
            }
        }
        if (datacheck) {
            data = builder.toString();
        }

        if (!(data == null)) {
            SweetAlertDialog dlg = new SweetAlertDialog(Main_Myboard_applicationlist.this, SweetAlertDialog.WARNING_TYPE);
            dlg.setCancelText("취소");
            dlg.setConfirmText("확인");
            dlg.setTitleText("콜라보를 생성하시겠습니까?");
            dlg.setContentText(data);
            dlg.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    // TODO: 2017-10-01 apply request part

                    sweetAlertDialog.dismiss();


                    String guests = builderforpost.toString();
                    HashMap<String, String> param = new HashMap<String, String>();
                    param.put("members", guests);
                    param.put("mb_id", User_INFO.getID());
                    param.put("unique_id", UNIQ.unique_id);
                    param.put("board_name", getIntent().getStringExtra("board_name"));
                    param.put("idx", getIntent().getIntExtra("idx", 0) + "");

                    HttpServiceClient.Retros_Teamcreate(param).enqueue(new Callback<Apply_create>() {
                        @Override
                        public void onResponse(Call<Apply_create> call, Response<Apply_create> response) {
                            Apply_create result = response.body();
                            if (result.getError().equals("false")) {
                                Toaster("선정완료!!!");
                                // TODO: 2017-10-01 팀 생성 화면 이동

                                Intent inte = new Intent(getApplicationContext(), Collabocreate_Activity.class);
                                inte.putExtra("member",builderforpost.toString());
                                startActivity(inte);
                            } else if (result.getError().equals("true")) {
                                Log.e("error true", result.getResult());
                            } else {
                                Log.e("Retro Error", response.errorBody().toString());
                            }
                        }

                        @Override
                        public void onFailure(Call<Apply_create> call, Throwable t) {
                            Log.e("Server Error", t.getMessage());
                        }
                    });
                }
            });
            dlg.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    sweetAlertDialog.dismiss();
                }
            });
            dlg.show();
        } else {
            Toaster("선택된 사람이 없습니다.");
        }
    }


    private void initList() {
        applicationlist.setAdapter(adapter);

    }

    private void Retros() {
        HashMap<String, String> map = new HashMap<>();
        map.put("unique_id", UNIQ.unique_id);
        map.put("board_name", getIntent().getStringExtra("board_name"));
        map.put("mb_id", User_INFO.getID());
        map.put("idx", getIntent().getIntExtra("idx", 0) + "");
        HttpServiceClient.Retros_RecruitApplyList(map).enqueue(new Callback<Apply_list>() {
            @Override
            public void onResponse(Call<Apply_list> call, Response<Apply_list> response) {
                if (response.body().getError().equals("false")) {
                    Result = response.body();
                    master = Result.getMaster();
                    member = Result.getMembers();
                    try {
                        Log.e("size", member.get(0).getMb_id() + " / " + member.size());
                    } catch (Exception e) {
                        noguesttext.setVisibility(View.VISIBLE);
                    }
                    adapter.setItems(member);

                    initList();

                } else {
                    Log.e("Retro Error", response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<Apply_list> call, Throwable t) {
                Log.e("Server Error", t.getMessage());
            }
        });
    }
}