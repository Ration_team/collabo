package com.ration.dohee.collabo.MENU;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.ration.dohee.collabo.DATA.ActionBarData;
import com.ration.dohee.collabo.R;
import com.ration.dohee.collabo.Util.ActionBar;
import com.ration.dohee.collabo.Util.RecyclerViewAdapter;
import com.ration.dohee.collabo.Util.myItem;

import java.util.ArrayList;

/**
 * Created by User on 2017-06-25.
 */

public class MyCareer_Activity extends ActionBar implements View.OnClickListener{

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
   // private RecyclerView.LayoutManager mLayoutManager;
   LinearLayoutManager mLayoutManager;
    private ArrayList<myItem> mItems;
    private static int count=0;

    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;

    private boolean flag=false;
    Button mycareer_send_btn,mycareer_cancel_btn;
    TextView mycareer_allcheck_tv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mycareer);
        SetActionBar(findViewById(ActionBarData.ActionBar));
        mRecyclerView = (RecyclerView) findViewById(R.id.mycareer_recyclerview);
        mycareer_send_btn=(Button)findViewById(R.id.mycareer_send_btn);
        mycareer_cancel_btn=(Button)findViewById(R.id.mycareer_cancel_btn);
        mycareer_allcheck_tv=(TextView)findViewById(R.id.mycareer_allcheck_tv);

        // RecyclerView's size 고정
        mRecyclerView.setHasFixedSize(false);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        initAdapter(false);

        mycareer_send_btn.setOnClickListener(this);
        mycareer_cancel_btn.setOnClickListener(this);
        mycareer_allcheck_tv.setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if(dy > 0) //check for scroll down
                {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (loading)
                    {
                        if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            loading = false;
                            Log.v("...", "Last Item Wow !");
                            //Do pagination.. i.e. fetch new data
                        }
                    }
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.mycareer_send_btn){
            //flag가 true, 이메일로 보내기
            //flag가 false, 체크박스 활성화
            if(!flag){
                initAdapter(true);
                mycareer_send_btn.setBackgroundResource(R.drawable.button03);
                mycareer_cancel_btn.setVisibility(View.VISIBLE);
                mycareer_allcheck_tv.setVisibility(View.VISIBLE);
                flag=true;
            }
            else{
                //다이어로그...이메일 보내기
                //다이어로그 - customDialogLayout 만들어서 다시해야함
                String data = "";
                final ArrayList<myItem> items = ((RecyclerViewAdapter) mAdapter)
                        .getmItems();


                for (int i = 0; i < items.size(); i++) {
                    myItem myitems = items.get(i);
                    if (myitems.isSelected == true) {
                        count++;
                       if(count==1){
                            data=myitems.title.toString();
                        }
                    }
                }
                if(count>1){
                    AlertDialog.Builder builder=new AlertDialog.Builder(this);
                    builder.setMessage("email@email.com"+"\n"
                            +"위의 메일로"+data+"외 "+(count-1)+" 건을 \n"+"보내시겠습니까?\n"
                    +"(이메일은 초기 인증한 이메일로만 가능합니다)")
                            .setPositiveButton("보내기", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Toast.makeText(MyCareer_Activity.this, "Success", Toast.LENGTH_SHORT).show();
                                }
                            })
                            .setNegativeButton("취소", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Toast.makeText(MyCareer_Activity.this, "cancel", Toast.LENGTH_SHORT).show();
                                    count=0;
                                }
                            })
                            .show();
                }
                else{
                    AlertDialog.Builder builder=new AlertDialog.Builder(this);
                    builder.setMessage("email@email.com"+"\n"
                            +"위의 메일로"+data+"보내시겠습니까?\n"
                            +"(이메일은 초기 인증한 이메일로만 가능합니다)")
                            .setPositiveButton("보내기", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Toast.makeText(MyCareer_Activity.this, "Success", Toast.LENGTH_SHORT).show();
                                }
                            })
                            .setNegativeButton("취소", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Toast.makeText(MyCareer_Activity.this, "cancel", Toast.LENGTH_SHORT).show();
                                    count=0;
                                }
                            })
                            .show();
                }
            }
        }
        else if(v.getId()==R.id.mycareer_cancel_btn){
            initAdapter(false);
            mycareer_cancel_btn.setVisibility(View.GONE);
            mycareer_send_btn.setBackgroundResource(R.drawable.button07);
            mycareer_allcheck_tv.setVisibility(View.GONE);
            flag=false;
        }
    }
    public void initAdapter(boolean isVisible){
        //프로젝트 이름 세팅...
        mItems = new ArrayList<>();
        mAdapter = new RecyclerViewAdapter(mItems,isVisible);
        mRecyclerView.setAdapter(mAdapter);

        mItems.add(new myItem("#InsideOut", "2001.11.12"));
        mItems.add(new myItem("#Mini", "2002.12.13"));
        mItems.add(new myItem("#ToyStroy", "2004.2.1"));
        mItems.add(new myItem("#InsideOut", "2001.11.12"));
        mItems.add(new myItem("#Mini", "2002.12.13"));
        mItems.add(new myItem("#ToyStroy", "2004.2.1"));
        mItems.add(new myItem("#InsideOut", "2001.11.12"));
        mItems.add(new myItem("#Mini", "2002.12.13"));
        mItems.add(new myItem("#ToyStroy", "2004.2.1"));
    }
}
