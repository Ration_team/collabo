package com.ration.dohee.collabo.Https;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samsung on 2017-10-13.
 */

public class Profile_View{

    @SerializedName("error")
    String error;

    @SerializedName("result")
    Profile_Data result;

    public String getError() {
        return error;
    }

    public Profile_Data getResult() {
        return result;
    }
}
