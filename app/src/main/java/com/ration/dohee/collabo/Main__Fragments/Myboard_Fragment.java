package com.ration.dohee.collabo.Main__Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.ration.dohee.collabo.Https.Board_Recruit_Result;
import com.ration.dohee.collabo.Https.Board_Recruit_Results;
import com.ration.dohee.collabo.Https.ContestList_BannerLatest;
import com.ration.dohee.collabo.Https.HttpContestInfo.Default_result;
import com.ration.dohee.collabo.Https.HttpServiceClient;
import com.ration.dohee.collabo.Login_Activity;
import com.ration.dohee.collabo.Main__Fragments.Util.Myboard_lists;
import com.ration.dohee.collabo.PROJECT_BOARD.project_seleceted;
import com.ration.dohee.collabo.R;
import com.ration.dohee.collabo.Util.FragmentsForSign;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Myboard_Fragment extends FragmentsForSign {
    LinearLayout main, sign;
    Button btn;
    Myboard_lists adapter;


    ArrayList<Board_Recruit_Result> result;
    ListView myBoardList;
    Unbinder unbinder;

    public Myboard_Fragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.activity_myboard, container, false);
        main = (LinearLayout) view.findViewById(R.id.myboard_main_view);
        sign = (LinearLayout) view.findViewById(R.id.myboard_sign_view);
        myBoardList=(ListView)view.findViewById(R.id.my_board_list);
        adapter=new Myboard_lists();
        btn = (Button) view.findViewById(R.id.myboard_sign_btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity().getApplicationContext(), Login_Activity.class));
            }
        });




        unbinder = ButterKnife.bind(this, view);
        return view;
    }




    public void Retro() {
        HttpServiceClient.Retros_MyBoardList().enqueue(new Callback<Board_Recruit_Results>() {
            @Override
            public void onResponse(Call<Board_Recruit_Results> call, Response<Board_Recruit_Results> response) {
                if (response.isSuccessful()) {
                    if(response.body().getError().equals("false"))
                    {
                        Board_Recruit_Results results=response.body();
                        if(results.getResult()!=null) {
                            result = results.getResult();
                            adapter.items.clear();
                            adapter.items = result;
                            settingListview();
                        }
                    }
                    else{

                    }

                } else {
                    Log.e("Server Error", response.errorBody().toString());

                }
            }
            @Override
            public void onFailure(Call<Board_Recruit_Results> call, Throwable t) {
                Log.e("Retros Error", t.getMessage());
            }
        });

    }

    private void settingListview()
    {
        myBoardList.setAdapter(adapter);

        myBoardList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(getActivity().getApplicationContext(),project_seleceted.class);
                intent.putExtra("idx",adapter.items.get(position).getIdx());
                intent.putExtra("board_name",adapter.items.get(position).getBoard_name());
                Log.e("idx",adapter.items.get(position).getIdx()+"");
                intent.putExtra("host",true);
                startActivity(intent);
            }
        });
        myBoardList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {

                SweetAlertDialog dlg=new SweetAlertDialog(getActivity(),SweetAlertDialog.WARNING_TYPE);
                dlg.setTitleText(" ");
                dlg.setContentText("삭제 하시겠습니까?");
                dlg.setConfirmText("삭제");
                dlg.setCancelText("취소");
                dlg.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        HashMap<String,String> map=new HashMap<String, String>();
                        map.put("idx",""+adapter.items.get(position).getIdx());
                        map.put("board_name",adapter.items.get(position).getBoard_name());


                        HttpServiceClient.Retros_MyboardDelete(map).enqueue(new Callback<Default_result>() {
                            @Override
                            public void onResponse(Call<Default_result> call, Response<Default_result> response) {
                                if(response.isSuccessful()){
                                    if(response.body().getError().equals("false"))
                                    {
                                        adapter.items.remove(position);
                                        SweetAlertDialog dlg=new SweetAlertDialog(getActivity(),SweetAlertDialog.SUCCESS_TYPE);
                                        dlg.setTitle(" ");
                                        dlg.setContentText("삭제 되었습니다.");
                                        dlg.setConfirmText("확인");
                                        dlg.setCanceledOnTouchOutside(true);
                                        dlg.setCancelable(true);
                                        dlg.show();
                                        adapter.notifyDataSetChanged();
                                    }
                                    else{
                                        Toast.makeText(getActivity(), "삭제실패", Toast.LENGTH_SHORT).show();
                                        Log.e("response Error",response.body().getError()+response.body().getMessage());
                                    }
                                }
                                else{
                                    Log.e("Error",response.errorBody().toString());
                                }
                            }

                            @Override
                            public void onFailure(Call<Default_result> call, Throwable t) {
                                Log.e("SERVER ERROR",t.getMessage());
                            }
                        });
                    }
                });
                dlg.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                });
                dlg.show();

                return true;
            }
        });

    }
    @Override
    public void onStart() {
        super.onStart();
        SettingView(main, sign);
        Retro();




    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }
}
