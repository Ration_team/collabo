package com.ration.dohee.collabo.PROJECT_BOARD;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.ration.dohee.collabo.DATA.DATA;
import com.ration.dohee.collabo.DATA.UNIQ;
import com.ration.dohee.collabo.DATA.User_INFO;
import com.ration.dohee.collabo.Https.Favor_delresult;
import com.ration.dohee.collabo.Https.Favor_result;
import com.ration.dohee.collabo.Https.Favorite_info;
import com.ration.dohee.collabo.Https.HttpServiceClient;
import com.ration.dohee.collabo.R;
import com.sackcentury.shinebuttonlib.ShineButton;

import java.util.LinkedList;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by User on 2017-06-28.
 */

public class IT_CustomAdapterListView extends BaseAdapter {
    TextView title,writer,date,num;
    ShineButton favor;
    CircleImageView cimg;
    String board_name;
    IT_ListViewItem ref;
    boolean Flag=false;
    int j;
    public LinkedList<IT_ListViewItem> items=new LinkedList<>();


    Favorite_info fav;

    public IT_CustomAdapterListView(String name) {
        board_name=name;
        if(name.equals(""))
            Flag=true;
    }


    private class View_holder{
        TextView title,writer,date,num;
        ShineButton favor;

        CircleImageView cimg;
    }
    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public IT_ListViewItem getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Context context=parent.getContext();
        View_holder viewHolder=new View_holder();
        if(convertView==null){
            LayoutInflater inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate(R.layout.it_project_board_list,parent,false);

//            viewHolder.title=(TextView)convertView.findViewById(R.id.it_project_board_title_tv);
//            viewHolder.date=(TextView)convertView.findViewById(R.id.it_project_board_date_tv);
//            viewHolder.num=(TextView)convertView.findViewById(R.id.it_project_board_num_tv);
//            viewHolder.writer=(TextView)convertView.findViewById(R.id.it_project_board_writer_tv);
//            viewHolder.favor=(ShineButton)convertView.findViewById(R.id.favorite_nice);
//            viewHolder.cimg=(CircleImageView)convertView.findViewById(R.id.it_project_board_imageview);
//            convertView.setTag(viewHolder);
        }
//        else{
//            viewHolder=(View_holder)convertView.getTag();
//        }
        title=(TextView)convertView.findViewById(R.id.it_project_board_title_tv);
        date=(TextView)convertView.findViewById(R.id.it_project_board_date_tv);
        num=(TextView)convertView.findViewById(R.id.it_project_board_num_tv);
        writer=(TextView)convertView.findViewById(R.id.it_project_board_writer_tv);
        favor=(ShineButton)convertView.findViewById(R.id.favorite_nice);
        cimg=(CircleImageView)convertView.findViewById(R.id.it_project_board_imageview);


        ref=items.get(position);

        favor.setChecked(false);

        title.setText(ref.getTitle());
        title.setSelected(true);
        date.setText(ref.getDate().substring(2,10));
        num.setText(ref.getNum()+"");
        writer.setText(ref.getWriter());
        if(Flag)
            board_name= ref.board_name;

        if(User_INFO.getfavors(board_name)!=null){
        for (int i=0;i<User_INFO.getfavors(board_name).size();i++)
        {
            if(User_INFO.getfavors(board_name).get(i).getIdx()==ref.getIdx())
            {
                Log.e("das","dasd");
                ref.isfavor=true;
                favor.setChecked(true);
                j=i;
                ref.favid=User_INFO.getfavors(board_name).get(i).getId();
                break;
            }
        }
        }
        favor.setOnClickListener(new View.OnClickListener() {
            IT_ListViewItem res=ref;
            @Override
            public void onClick(View v) {
                final View view=v;
                favor=(ShineButton)v.findViewById(R.id.favorite_nice);
                //즐겨찾기 해제
                if(!favor.isChecked())
                {
                    res.isfavor=false;
                    for(int i=0;i<User_INFO.getfavors(board_name).size();i++)
                    {
                        if(res.favid==User_INFO.getfavors(board_name).get(i).getId())
                        {
                           j=i;
                            break;
                        }
                    }
                    Call<Favor_delresult> repos= HttpServiceClient.getApiService().Favorite_Delete(
                            User_INFO.getfavors(board_name).get(j).getId(), UNIQ.unique_id, User_INFO.getID(),0
                    );
                    repos.enqueue(new Callback<Favor_delresult>() {
                        @Override
                        public void onResponse(Call<Favor_delresult> call, Response<Favor_delresult> response) {
                            if(response.isSuccessful())
                            {
                                if(response.body().getError().equals("false")){
                                    User_INFO.getfavors(board_name).remove(j);

                                }
                            }
                            else{
                                Log.e("Server Error",response.errorBody().toString());
                            }
                        }

                        @Override
                        public void onFailure(Call<Favor_delresult> call, Throwable t) {
                            Log.e("Retros Error",t.getMessage().toString());

                        }
                    });
                }
                //즐겨찾기 등록
                else if (favor.isChecked())
                {
                    Call<Favor_result> repos= HttpServiceClient.getApiService().Favorite_Add(
                            board_name,res.getIdx(),UNIQ.unique_id,User_INFO.getID(),res.getTitle()
                    );
                    repos.enqueue(new Callback<Favor_result>() {
                        @Override
                        public void onResponse(Call<Favor_result> call, Response<Favor_result> response) {
                            if(response.isSuccessful())
                            {
                                if(response.body().getError().equals("false")) {
                                    Favorite_info ref=response.body().getResult().get(0);
                                    res.isfavor=true;
                                    res.favid=ref.getId();
                                    User_INFO.getfavors(board_name).add(ref);
                                }
                            }
                            else{
                                Log.e("Server Error",response.errorBody().toString());
                            }
                        }

                        @Override
                        public void onFailure(Call<Favor_result> call, Throwable t) {
                            Log.e("Retros Error",t.getMessage().toString());
                        }
                    });
                }
            }
        });
        Glide.with(parent.getContext()).load(DATA.ImgRootURL+ref.getImgurl()).into(cimg);


        Log.e("imgurl",DATA.ImgRootURL+ref.getImgurl());


        return convertView;
    }
}
