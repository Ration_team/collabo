package com.ration.dohee.collabo.Board_NOTICE;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ration.dohee.collabo.DATA.ActionBarData;
import com.ration.dohee.collabo.DATA.DATA;
import com.ration.dohee.collabo.DATA.User_INFO;
import com.ration.dohee.collabo.Https.Board_Commits;
import com.ration.dohee.collabo.Https.Board_Result;
import com.ration.dohee.collabo.Https.Board_Results;
import com.ration.dohee.collabo.Https.Board_Statment;
import com.ration.dohee.collabo.Https.HttpServiceClient;
import com.ration.dohee.collabo.R;
import com.ration.dohee.collabo.Util.ActionBar;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ration.dohee.collabo.DATA.UNIQ.unique_id;

public class Notice_Seleceted_Activity extends ActionBar {
    TextView TITLE, CONTENT, DATE, HIT_COUNT;
    ImageView FILE,IMG;
    EditText titleedt,contentedt;
    Button delete,edit,submit,filebtn,cancel,imgdelbtn;
    LinearLayout btnlin,tvlin,edtlin;
    int STATE;
    String BoardID;
    File imgfile;
    int idx;
    boolean issetimg,isdelimg;

    @Override
    protected void onStart() {
        super.onStart();
        SetActionBar(((View)findViewById(ActionBarData.ActionBar)));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice__seleceted_);
        idx = getIntent().getIntExtra("idx", -1);
        btnlin=(LinearLayout)findViewById(R.id.notice_selected_btns_Lin);
        tvlin=(LinearLayout)findViewById(R.id.notice_selected_view_Lin);
        edtlin=(LinearLayout)findViewById(R.id.notice_selected_edit_Lin);
        submit=(Button)findViewById(R.id.notice_selected_submit_btn);
        TITLE = (TextView) findViewById(R.id.notice_selected_title_tv);
        DATE = (TextView) findViewById(R.id.notice_selected_date_tv);
        FILE = (ImageView) findViewById(R.id.notice_selected_file_imgv);
        filebtn=(Button)findViewById(R.id.notice_selected_file_btn);
        cancel=(Button)findViewById(R.id.notice_selected_cancel_btn);
        titleedt=(EditText)findViewById(R.id.notice_selected_title_edt);
        contentedt=(EditText)findViewById(R.id.notice_selected_content_edt);
        CONTENT = (TextView) findViewById(R.id.notice_selected_content_tv);
        HIT_COUNT = (TextView) findViewById(R.id.notice_selected_hitcount_tv);
        delete=(Button)findViewById(R.id.notice_selected_delete_btn);
        edit=(Button)findViewById(R.id.notice_selected_modify_btn);
        IMG=(ImageView)findViewById(R.id.notice_selected_img_imgv);
        imgdelbtn=(Button)findViewById(R.id.notice_selected_imgdel_btn);
        imgdelbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IMG.setVisibility(View.GONE);
                imgdelbtn.setVisibility(View.GONE);
                isdelimg=true;
            }
        });
        Log.e("IDS",User_INFO.getID()+"   L  "+BoardID);

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtlin.setVisibility(View.VISIBLE);
                tvlin.setVisibility(View.GONE);
                STATE=1;
                edit.setVisibility(View.GONE);
                submit.setVisibility(View.VISIBLE);
                delete.setVisibility(View.GONE);
                cancel.setVisibility(View.VISIBLE);
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                STATE=2;
                edtlin.setVisibility(View.GONE);
                tvlin.setVisibility(View.VISIBLE);
                submit.setVisibility(View.GONE);
                edit.setVisibility(View.VISIBLE);
                delete.setVisibility(View.VISIBLE);
                cancel.setVisibility(View.GONE);
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RetroFit();
            }
        });
        filebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setData(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult(intent, DATA.GALLERY_DIRECT_CODE);
            }
        });
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // delete TODO...
                Map<String,String> map=new HashMap<String, String>();
                map.put("board_name","board_notice");
                map.put("idx",idx+"");
                map.put("mb_id", User_INFO.getID());
                map.put("unique_id", unique_id);
                HttpServiceClient.Retros_BoardDelete(map).enqueue(new Callback<Board_Statment>() {
                    @Override
                    public void onResponse(Call<Board_Statment> call, Response<Board_Statment> response) {
                        if(response.isSuccessful()){
                        if(response.body().getError().equals("false"))
                        {
                            Toaster("삭제완료");
                            finish();
                            startActivity(new Intent(getApplicationContext(),Notice_Activity.class));
                        }

                        else {
                            Toaster("삭제 에러");
                        }
                        }
                        else{
                            Toaster("서버 상태가 좋지않음"+response.errorBody());
                        }

                    }

                    @Override
                    public void onFailure(Call<Board_Statment> call, Throwable t) {
                        Toaster("연결 에러");
                        Log.e("delete Error", t.getMessage());
                    }
                });


            }
        });
        HttpServiceClient.Retros_BoardView("board_notice", idx).enqueue(new Callback<Board_Results>() {
            @Override
            public void onResponse(Call<Board_Results> call, Response<Board_Results> response) {
                if(response.isSuccessful()){
                Board_Results ref = response.body();
                if (ref.getError().equals("false")) {
                    Log.e("View Success", ref.getError());
                    TextFieldInit(ref.getResult());
                    BoardID=ref.getResult().getMb_id();
                    if(User_INFO.getID()!=null||!(User_INFO.getID().equals(""))) {
                        if (User_INFO.getID().equals(BoardID)) {
                            btnlin.setVisibility(View.VISIBLE);
                        }
                    }

                } else {
                    Log.e("Error true", ref.getError() + "  :  " + ref.getMessage());
                }
                } else{
                    Toaster("서버 상태가 좋지않음"+response.errorBody());
                }
            }
            @Override
            public void onFailure(Call<Board_Results> call, Throwable t) {
                Log.e("Failed", t.getMessage().toString());
                Toast.makeText(Notice_Seleceted_Activity.this, t.getMessage().toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

//    private void ShowDialog() {
//        LayoutInflater dlayout= LayoutInflater.from(this);
//        final View mView=dlayout.inflate(R.layout.dialog,null);
//        final Dialog mDialog=new Dialog(this);
//        mDialog.setContentView(mView);
//        mDialog.setCanceledOnTouchOutside(true);
//        Button img_select,img_delete;
//        img_delete=(Button)mView.findViewById(R.id.dialog_img_delete_btn);
//        img_select=(Button)mView.findViewById(R.id.dialog_img_select_btn);
//        img_select.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toaster("이미지 바꾸기");
//                mDialog.cancel();
//            }
//        });
//        img_delete.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toaster("이미지 지우기");
//                mDialog.cancel();
//            }
//        });
//        mDialog.show();
//
//    }


    void TextFieldInit(Board_Result res) {

        TITLE.setText(res.getWr_subject());
        DATE.setText(res.getWr_date());
        HIT_COUNT.setText("조회수 "+res.getHit_count());
        CONTENT.setText(res.getWr_content());
        titleedt.setText(TITLE.getText());
        contentedt.setText(CONTENT.getText());
        if(res.getWr_file().equals("")||res.getWr_file().equals("null")||res.getWr_file().equals(" "))
        {
            imgdelbtn.setVisibility(View.GONE);
            issetimg=false;
            return;
        }
//        Glide.with(getApplicationContext()).load(file).diskCacheStrategy(DiskCacheStrategy.NONE).into(img);
        Glide.with(getApplicationContext()).load("http://collabo.boasmedia.co.kr/uploads/" + res.getWr_file()).placeholder(R.drawable.defaulting).into(FILE);
        Glide.with(getApplicationContext()).load("http://collabo.boasmedia.co.kr/uploads/" + res.getWr_file()).placeholder(R.drawable.defaulting).into(IMG);

        //이미지 크기 지정
        LinearLayout.LayoutParams params=(LinearLayout.LayoutParams)FILE.getLayoutParams();
        params.height=500;
        params.bottomMargin=50;
        FILE.setLayoutParams(params);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            imgfile=SendPicture(data); //갤러리에서 가져오기
            imgdelbtn.setVisibility(View.VISIBLE);
            IMG.setVisibility(View.VISIBLE);
            Glide.with(this).load(imgfile.getPath()).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(IMG);
        }
    }
    void RetroFit() {
        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(Notice_Seleceted_Activity.this);
        progressDialog.setMessage("Uploading");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();

        Map<String, RequestBody> map = new HashMap<>();
        String isimgdel=isdelimg?"on":"off";
        Log.e("UNIQ", unique_id + "");
        map.put("board_name", ToRequesBody("board_notice"));
        map.put("mb_id", ToRequesBody(User_INFO.getID()));
        map.put("wr_subject", ToRequesBody(titleedt.getText().toString()));
        map.put("wr_content", ToRequesBody(contentedt.getText().toString()));
        map.put("unique_id", ToRequesBody(unique_id));
        map.put("idx",ToRequesBody(idx+""));
        map.put("is_delete",ToRequesBody(isimgdel));
        try {
            RequestBody fileBody = RequestBody.create(MediaType.parse("image/jpeg"), imgfile);
            map.put("wr_file\"; filename=\"" + imgfile.getPath() + "\"", fileBody);
            map.put("wr_file", fileBody);
            Log.e("filename", imgfile.getPath());
        }
        catch (NullPointerException e)
        {
            map.put("wr_file",ToRequesBody(""));
        }
        catch (Exception e){}



        HttpServiceClient.Retros_BoardEdit(map).enqueue(new Callback<Board_Commits>() {
            @Override
            public void onResponse(Call<Board_Commits> call, Response<Board_Commits> response) {

                if(response.body().getError().equals("true"))
                {
                    Toaster(response.body().getMsg());
                    return;

                }

                if(response.isSuccessful()){
                Log.e("EROOR2", call.toString());
                progressDialog.dismiss();
                Board_Commits ref = response.body();
                Log.e("Success", ref.getError());
                Toast.makeText(Notice_Seleceted_Activity.this, "성공", Toast.LENGTH_SHORT).show();
                try {
                    imgfile.delete();
                }catch (Exception e){}
                Snackbar.make(getWindow().getDecorView().getRootView(),ref.getMsg(),Snackbar.LENGTH_SHORT).show();
                Intent inte=new Intent(getApplicationContext(), Notice_Seleceted_Activity.class);
                inte.putExtra("idx",ref.getResult().getIdx());
                startActivity(inte);
                finish();
                }
                 else{
                    Toaster("서버 상태가 좋지않음"+response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<Board_Commits> call, Throwable t) {
                Toast.makeText(Notice_Seleceted_Activity.this, "실패" + t.getMessage().toString(), Toast.LENGTH_SHORT).show();
                Log.e("EROOR", t.getMessage().toString());
                progressDialog.dismiss();
                Log.e("EROOR2", call.toString());
                try {
                    imgfile.delete();
                }catch (Exception e){}
            }

        });
    }
    @Override
    public void onBackPressed() {
        if(STATE==1){
            edtlin.setVisibility(View.GONE);
            tvlin.setVisibility(View.VISIBLE);
            submit.setVisibility(View.GONE);
            edit.setVisibility(View.VISIBLE);
            delete.setVisibility(View.VISIBLE);
            cancel.setVisibility(View.GONE);
            STATE=2;
            isdelimg=false;
            return;
        }
        super.onBackPressed();
        finish();

    }
}