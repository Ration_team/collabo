package com.ration.dohee.collabo.ListUtil;

/**
 * Created by Samsung on 2017-07-26.
 */

public class CareerItem {
    public String title;
    public String date;

    public String getTitle() {
        return title;
    }

    public String getDate() {
        return date;
    }
}
