package com.ration.dohee.collabo.Https;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samsung on 2017-07-19.
 */

public class IdCheckResult {
    @SerializedName("error")
    String error;

    public void setError(String error) {
        this.error = error;
    }

    public String getError() {
        return error;
    }
}
