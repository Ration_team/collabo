package com.ration.dohee.collabo;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.ration.dohee.collabo.DATA.DATA;
import com.ration.dohee.collabo.DATA.UNIQ;
import com.ration.dohee.collabo.DATA.User_INFO;
import com.ration.dohee.collabo.Https.HttpServiceClient;
import com.ration.dohee.collabo.Https.Result;
import com.ration.dohee.collabo.Https.Results;
import com.ration.dohee.collabo.Util.CustomActivityForFonts;
import com.rengwuxian.materialedittext.MaterialEditText;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login_Activity extends CustomActivityForFonts {
    Button btnlogin,btnjoin;
    MaterialEditText id,pass;
    String ID,PASS,Login_INFO;
    boolean Flags[]={false,false};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);
        id=(MaterialEditText)findViewById(R.id.login_id_input_edt);
        pass=(MaterialEditText)findViewById(R.id.login_pass_input_edt);
        btnlogin = (Button)findViewById(R.id.login_complete_btn);
        btnjoin=(Button)findViewById(R.id.btnJoin);

        id.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String ref=s.toString();
                if(ref.length()>0){
                    ID=id.getText().toString();
                    Flags[0]=true;
                }
                else if(ref.length()==0){
                    Flags[0]=false;
                }
            }
            @Override
            public void afterTextChanged(Editable s) {}
        });
        pass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String ref=s.toString();
                if(ref.length()>0){
                    PASS=pass.getText().toString();
                    Flags[1]=true;
                }
                else if(ref.length()==0){
                    Flags[1]=false;
                }
            }
            @Override
            public void afterTextChanged(Editable s) {}
        });
        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(CheckInFlags(Flags)){
                    HttpServiceClient.Retros_SIGN(ID,PASS)
                            .enqueue(new Callback<Results>() {
                                @Override
                                public void onResponse(Call<Results> call, Response<Results> response) {
                                    // http 연결 성공
                                    if (response.isSuccessful())
                                        HttpComplete(response.body());
                                    else{
                                        Log.e("Server Error",response.errorBody()+"");
                                    }
                                }
                                @Override
                                public void onFailure(Call<Results> call, Throwable t) {
                                    // http 연결 실패
                                    Log.e("Retros Throwable" , t.getMessage());
                                }
                            });
                    btnlogin.setClickable(false);
                }else{
                    Toast.makeText(Login_Activity.this, "값을 입력해주세요", Toast.LENGTH_SHORT).show();
                }

            }
        });
        btnjoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Login_Activity.this,Join_Activity.class);
                startActivity(intent);
            }
        });


    }


    void Auto_LoginChecked(String id,String pass,String email){
        SetUser_INFO(id,pass,email);
        SetLogin(true);
    }
    void HttpComplete(Results results){


               String error=results.getError();

                if(error.equals("false")){
                    Result res=results.getResult();
                    String email=res.getMb_email();
                    String auth=res.getMb_auth();

                    if(auth.equals("active")){
                        UNIQ.unique_id=res.getUnique_id();
                        Toast.makeText(Login_Activity.this, "Welcome to Collabo!", Toast.LENGTH_SHORT).show();
                        Intent intent =new Intent(Login_Activity.this, MainActivity.class);
//                        intent.putExtra("Login_Activity",true);// 로딩및 로그인 완료  플래그 전달
                        Auto_LoginChecked(ID,PASS,email);
                        intent.putExtra("logon",true);
                        Log.e("islogin", DATA._isLogin()+"\n"+User_INFO.getID()+ User_INFO.getPASS());
                        Log.e("id?",ID+"///"+PASS);
                        Log.e("UNIQ",UNIQ.unique_id);
                        startActivity(intent);
                        return;
                    }
                    else{
                        Toast.makeText(this, "등록하신 이메일로 인증을 마쳐주세요", Toast.LENGTH_SHORT).show();
                    }

                }
                else if(error.equals("true")){
                    String error_msg=results.getMsg();
                    Toast.makeText(Login_Activity.this, error_msg, Toast.LENGTH_SHORT).show();
                    Log.e("Login Error msg",error_msg);
                }



        btnlogin.setClickable(true);
    }


}
