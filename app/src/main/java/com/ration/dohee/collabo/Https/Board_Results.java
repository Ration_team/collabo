package com.ration.dohee.collabo.Https;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samsung on 2017-07-13.
 */

public class Board_Results {

    @SerializedName("error")
    String error;
    @SerializedName("result")
    Board_Result result;
    @SerializedName("message")
    String message;

    public String getError() {
        return error;
    }

    public Board_Result getResult() {
        return result;
    }

    public String getMessage() {
        return message;
    }



}