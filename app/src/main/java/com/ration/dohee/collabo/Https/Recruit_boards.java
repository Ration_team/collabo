package com.ration.dohee.collabo.Https;

        import com.google.gson.annotations.SerializedName;

/**
 * Created by Samsung on 2017-08-08.
 */

public class Recruit_boards {

    @SerializedName("error")
    String error;
    @SerializedName("result")
    Recruit_board result;
    @SerializedName("message")
    String msg;
    @SerializedName("board_count")
    String board_count;

    public String getMsg() {
        return msg;
    }

    public String getError() {
        return error;
    }

    public Recruit_board getResult() {
        return result;
    }
}
