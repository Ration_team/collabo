package com.ration.dohee.collabo.Https;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samsung on 2017-10-26.
 */

public class ContestList_BannerItem {
        @SerializedName("idx")
        int idx;
        @SerializedName("wr_subject")
        String wr_subject;
        @SerializedName("wr_file")
        String file;

        public int getIdx() {
            return idx;
        }

        public String getWr_subject() {
            return wr_subject;
        }

        public String getFile() {
            return file;
        }
    }

