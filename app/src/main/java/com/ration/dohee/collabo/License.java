package com.ration.dohee.collabo;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.ration.dohee.collabo.Util.ActionBar;

/**
 * Created by Samsung on 2017-07-26.
 */

public class License extends ActionBar{
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_license);

        TextView text=(TextView)findViewById(R.id.textarea);
        text.setText("" +
                "1.RomainPiel/Shimmer-android\n" +
                "\n" +
                "    Copyright 2014 Romain Piel\n" +
                "\n" +
                "    Licensed under the Apache License, Version 2.0 (the \"License\");\n" +
                "    you may not use this file except in compliance with the License.\n" +
                "    You may obtain a copy of the License at\n" +
                "\n" +
                "    http://www.apache.org/licenses/LICENSE-2.0\n" +
                "\n" +
                "    Unless required by applicable law or agreed to in writing, software\n" +
                "    distributed under the License is distributed on an \"AS IS\" BASIS,\n" +
                "    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\n" +
                "    See the License for the specific language governing permissions and\n" +
                "    limitations under the License.\n" +
                "\n" +
                "    3.LeMinhAn/BannerSlider-master\n" +
                "\n" +
                "    Copyright 2016 Saeed Shahini\n" +
                "\n" +
                "    Licensed under the Apache License, Version 2.0 (the \"License\"); you may not use this file except in compliance with the License. You may obtain a copy of the License at\n" +
                "\n" +
                "    http://www.apache.org/licenses/LICENSE-2.0\n" +
                "    Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an \"AS IS\" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.\n" +
                "\n" +
                "    4.square/retrofit\n" +
                "\n" +
                "    Copyright 2013 Square, Inc.\n" +
                "\n" +
                "    Licensed under the Apache License, Version 2.0 (the \"License\");\n" +
                "    you may not use this file except in compliance with the License.\n" +
                "    You may obtain a copy of the License at\n" +
                "\n" +
                "    http://www.apache.org/licenses/LICENSE-2.0\n" +
                "\n" +
                "    Unless required by applicable law or agreed to in writing, software\n" +
                "    distributed under the License is distributed on an \"AS IS\" BASIS,\n" +
                "    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\n" +
                "    See the License for the specific language governing permissions and\n" +
                "    limitations under the License.\n" +
                "\n" +
                "    5.Devlight/NavigationTabBar\n" +
                "\n" +
                "    라이센스 안보임\n" +
                "\n" +
                "    6.rengwuxian/MaterialEditText\n" +
                "\n" +
                "    Copyright 2014 rengwuxian\n" +
                "\n" +
                "    Licensed under the Apache License, Version 2.0 (the \"License\");\n" +
                "    you may not use this file except in compliance with the License.\n" +
                "    You may obtain a copy of the License at\n" +
                "\n" +
                "    http://www.apache.org/licenses/LICENSE-2.0\n" +
                "\n" +
                "    Unless required by applicable law or agreed to in writing, software\n" +
                "    distributed under the License is distributed on an \"AS IS\" BASIS,\n" +
                "    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\n" +
                "    See the License for the specific language governing permissions and\n" +
                "    limitations under the License.\n" +
                "\n" +
                "    7.tsengvn/typekit\n" +
                "\n" +
                "    Copyright (c) 2015 Hien Ngo\n" +
                "\n" +
                "    Licensed under the Apache License, Version 2.0 (the \"License\");\n" +
                "    you may not use this file except in compliance with the License.\n" +
                "    You may obtain a copy of the License at\n" +
                "\n" +
                "    http://www.apache.org/licenses/LICENSE-2.0\n" +
                "\n" +
                "    Unless required by applicable law or agreed to in writing, software\n" +
                "    distributed under the License is distributed on an \"AS IS\" BASIS,\n" +
                "    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\n" +
                "    See the License for the specific language governing permissions and\n" +
                "    limitations under the License.\n" +
                "\n" +
                "    8.cachapa/ExpandableLayout\n" +
                "\n" +
                "    Copyright 2016 Daniel Cachapa.\n" +
                "\n" +
                "    Licensed under the Apache License, Version 2.0 (the \"License\");\n" +
                "    you may not use this file except in compliance with the License.\n" +
                "    You may obtain a copy of the License at\n" +
                "\n" +
                "    http://www.apache.org/licenses/LICENSE-2.0\n" +
                "\n" +
                "    Unless required by applicable law or agreed to in writing, software\n" +
                "    distributed under the License is distributed on an \"AS IS\" BASIS,\n" +
                "    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\n" +
                "    See the License for the specific language governing permissions and\n" +
                "    limitations under the License.\n" +
                "\n" +
                "    9.IvBaranov/MaterialFavoriteButton\n" +
                "\n" +
                "    Copyright 2015 Ivan Baranov\n" +
                "\n" +
                "    Licensed under the Apache License, Version 2.0 (the \"License\");\n" +
                "    you may not use this file except in compliance with the License.\n" +
                "    You may obtain a copy of the License at\n" +
                "\n" +
                "    http://www.apache.org/licenses/LICENSE-2.0\n" +
                "\n" +
                "    Unless required by applicable law or agreed to in writing, software\n" +
                "    distributed under the License is distributed on an \"AS IS\" BASIS,\n" +
                "    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\n" +
                "    See the License for the specific language governing permissions and\n" +
                "    limitations under the License.\n" +
                "\n" +
                "    10.bumptech/glide\n" +
                "\n" +
                "    BSD, part MIT and Apache 2.0. See the LICENSE file for details.\n" +
                "\n" +
                "    11.roughike/SwipeSelector\n" +
                "\n" +
                "    SwipeSelector library for Android\n" +
                "    Copyright (c) 2016 Iiro Krankka (http://github.com/roughike).\n" +
                "\n" +
                "    Licensed under the Apache License, Version 2.0 (the \"License\");\n" +
                "    you may not use this file except in compliance with the License.\n" +
                "    You may obtain a copy of the License at\n" +
                "\n" +
                "    http://www.apache.org/licenses/LICENSE-2.0\n" +
                "\n" +
                "    Unless required by applicable law or agreed to in writing, software\n" +
                "    distributed under the License is distributed on an \"AS IS\" BASIS,\n" +
                "    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\n" +
                "    See the License for the specific language governing permissions and\n" +
                "    limitations under the License.\n" +
                "\n" +
                "\n" +
                "    12. pedant/sweet-alert-dialog\n" +
                "\n" +
                "    The MIT License (MIT)\n" +
                "\n" +
                "    Copyright (c) 2014 Pedant(http://pedant.cn)\n" +
                "\n" +
                "    Permission is hereby granted, free of charge, to any person obtaining a copy\n" +
                "    of this software and associated documentation files (the \"Software\"), to deal\n" +
                "    in the Software without restriction, including without limitation the rights\n" +
                "    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell\n" +
                "    copies of the Software, and to permit persons to whom the Software is\n" +
                "    furnished to do so, subject to the following conditions:\n" +
                "\n" +
                "    The above copyright notice and this permission notice shall be included in all\n" +
                "    copies or substantial portions of the Software.\n" +
                "\n" +
                "    THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR\n" +
                "    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,\n" +
                "    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE\n" +
                "    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER\n" +
                "    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,\n" +
                "    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE\n" +
                "    SOFTWARE.\n" +
                "\n" +
                "\n" +
                "    KingJA/SwitchButton\n" +
                "\n" +
                "    Copyright 2017 KingJA\n" +
                "\n" +
                "    Licensed under the Apache License, Version 2.0 (the \"License\");\n" +
                "    you may not use this file except in compliance with the License.\n" +
                "    You may obtain a copy of the License at\n" +
                "\n" +
                "    http://www.apache.org/licenses/LICENSE-2.0\n" +
                "\n" +
                "    Unless required by applicable law or agreed to in writing, software\n" +
                "    distributed under the License is distributed on an \"AS IS\" BASIS,\n" +
                "    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\n" +
                "    See the License for the specific language governing permissions and\n" +
                "    limitations under the License.\n" +
                "\n" +
                "\n" +
                "\n" +
                "    hdodenhof/CircleImageView\n" +
                "\n" +
                "    Copyright 2014 - 2017 Henning Dodenhof\n" +
                "\n" +
                "    Licensed under the Apache License, Version 2.0 (the \"License\");\n" +
                "    you may not use this file except in compliance with the License.\n" +
                "    You may obtain a copy of the License at\n" +
                "\n" +
                "    http://www.apache.org/licenses/LICENSE-2.0\n" +
                "\n" +
                "    Unless required by applicable law or agreed to in writing, software\n" +
                "    distributed under the License is distributed on an \"AS IS\" BASIS,\n" +
                "    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\n" +
                "    See the License for the specific language governing permissions and\n" +
                "    limitations under the License.\n" +
                "\n" +
                "\n" +
                "\n" +
                "    square/retrofit\n" +
                "\n" +
                "    Copyright 2013 Square, Inc.\n" +
                "\n" +
                "    Licensed under the Apache License, Version 2.0 (the \"License\");\n" +
                "    you may not use this file except in compliance with the License.\n" +
                "    You may obtain a copy of the License at\n" +
                "\n" +
                "    http://www.apache.org/licenses/LICENSE-2.0\n" +
                "\n" +
                "    Unless required by applicable law or agreed to in writing, software\n" +
                "    distributed under the License is distributed on an \"AS IS\" BASIS,\n" +
                "    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\n" +
                "    See the License for the specific language governing permissions and\n" +
                "    limitations under the License.\n" +
                "\n" +
                "\n" +
                "    czy1121/noticeview\n" +
                "\n" +
                "    Copyright 2016 czy1121\n" +
                "\n" +
                "    Licensed under the Apache License, Version 2.0 (the \"License\");\n" +
                "    you may not use this file except in compliance with the License.\n" +
                "    You may obtain a copy of the License at\n" +
                "\n" +
                "    http://www.apache.org/licenses/LICENSE-2.0\n" +
                "\n" +
                "    Unless required by applicable law or agreed to in writing, software\n" +
                "    distributed under the License is distributed on an \"AS IS\" BASIS,\n" +
                "    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\n" +
                "    See the License for the specific language governing permissions and\n" +
                "    limitations under the License.\n" +
                "\n" +
                "\n" +
                "\n" +
                "    Nightonke/JellyToggleButton\n" +
                "\n" +
                "    Copyright 2016 Nightonke\n" +
                "\n" +
                "    Licensed under the Apache License, Version 2.0 (the \"License\");\n" +
                "    you may not use this file except in compliance with the License.\n" +
                "    You may obtain a copy of the License at\n" +
                "\n" +
                "    http://www.apache.org/licenses/LICENSE-2.0\n" +
                "\n" +
                "    Unless required by applicable law or agreed to in writing, software\n" +
                "    distributed under the License is distributed on an \"AS IS\" BASIS,\n" +
                "    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\n" +
                "    See the License for the specific language governing permissions and\n" +
                "    limitations under the License.\n" +
                "\n" +
                "\n" +
                "\n" +
                "    JakeWharton/butterknife\n" +
                "\n" +
                "    Copyright 2013 Jake Wharton\n" +
                "\n" +
                "    Licensed under the Apache License, Version 2.0 (the \"License\");\n" +
                "    you may not use this file except in compliance with the License.\n" +
                "    You may obtain a copy of the License at\n" +
                "\n" +
                "    http://www.apache.org/licenses/LICENSE-2.0\n" +
                "\n" +
                "    Unless required by applicable law or agreed to in writing, software\n" +
                "    distributed under the License is distributed on an \"AS IS\" BASIS,\n" +
                "    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\n" +
                "    See the License for the specific language governing permissions and\n" +
                "    limitations under the License.\n");
    }


    //


}