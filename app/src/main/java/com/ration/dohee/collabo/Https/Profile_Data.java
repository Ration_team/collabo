package com.ration.dohee.collabo.Https;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samsung on 2017-10-13.
 */

public class Profile_Data {

    @SerializedName("mb_id")
    String mb_id;
    @SerializedName("mb_email")
    String mb_email;
    @SerializedName("mb_tel")
    String mb_tel;
    @SerializedName("profile")
    String profile;
    @SerializedName("wr_file")
    String wr_file;

    public String getMb_id() {
        return mb_id;
    }

    public String getMb_email() {
        return mb_email;
    }

    public String getMb_tel() {
        return mb_tel;
    }

    public String getProfile() {
        return profile;
    }

    public String getWr_file() {
        return wr_file;
    }

}
