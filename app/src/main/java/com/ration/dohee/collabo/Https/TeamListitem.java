package com.ration.dohee.collabo.Https;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samsung on 2017-10-26.
 */

public class TeamListitem {
    @SerializedName("idx")
    int idx;
    @SerializedName("name")
    String team_name;
    @SerializedName("master")
    String host;
    @SerializedName("member")
    String member;

    public int getIdx() {
        return idx;
    }

    public String getTeam_name() {
        return team_name;
    }

    public String getHost() {
        return host;
    }

    public String getMember() {
        return member;
    }
}
