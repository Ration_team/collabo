package com.ration.dohee.collabo.Https;

import com.google.gson.annotations.SerializedName;
import com.ration.dohee.collabo.Https.HttpContestInfo.ContestInfoList;

import java.util.ArrayList;

/**
 * Created by Samsung on 2017-10-20.
 */

public class Search_Results {

    @SerializedName("error")
    String error;
    @SerializedName("message")
    String msg;
    @SerializedName("contest")
    ArrayList<ContestInfoList> contest;
    @SerializedName("recruit")
    ArrayList<Board_Recruit_Result> recruit;
    public int state;

    public String getError() {
        return error;
    }

    public String getMsg() {
        return msg;
    }

    public ArrayList<ContestInfoList> getContest() {
        return contest;
    }

    public ArrayList<Board_Recruit_Result> getRecruit() {
        return recruit;
    }
}
