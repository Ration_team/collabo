package com.ration.dohee.collabo.Util;

import java.io.Serializable;

/**
 * Created by User on 2017-06-25.
 */

public class myItem implements Serializable {
    public String title;
    public String date;
    public boolean isSelected;

    public myItem(String title,String date){
        this.title=title;
        this.date=date;
    }
}