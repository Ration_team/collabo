package com.ration.dohee.collabo.Main__Fragments.Util;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ration.dohee.collabo.Https.Recruit_boards;
import com.ration.dohee.collabo.R;
import com.sackcentury.shinebuttonlib.ShineButton;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Samsung on 2017-10-01.
 */

public class Favorite_List_RecruitAdapter extends RecyclerView.Adapter<Favorite_List_RecruitAdapter_Viewholder> {

    public ArrayList<Recruit_boards> items;

    private Context context;

    public Favorite_List_RecruitAdapter(Context context) {
        super();

        this.context = context;
    }

    public void setItems(ArrayList<Recruit_boards> items) {
        this.items = new ArrayList<>();
        this.items.addAll(items);
    }

    public ArrayList<Recruit_boards> getItems() {
        return items;
    }

    @Override
    public Favorite_List_RecruitAdapter_Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {


        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.it_project_board_list, parent, false);

        Favorite_List_RecruitAdapter_Viewholder viewHolder = new Favorite_List_RecruitAdapter_Viewholder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final Favorite_List_RecruitAdapter_Viewholder holder, final int position) {
        // TODO: 2017-10-01  Recycler List Settings  With Favorite Logical
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}


class Favorite_List_RecruitAdapter_Viewholder extends RecyclerView.ViewHolder {

    
    ImageView projectBoardListReuploadImg;
    
    TextView itProjectBoardTitleTv;
  
    CircleImageView itProjectBoardImageview;
    
    TextView itProjectBoardWriterTv;

    TextView itProjectBoardDateTv;
    
    TextView tv1;
   
    TextView itProjectBoardNumTv;
    
    TextView tv2;
   
    ShineButton favoriteNice;

    public Favorite_List_RecruitAdapter_Viewholder(View v) {
        super(v);
        projectBoardListReuploadImg=(ImageView)v.findViewById(R.id.project_board_list_reupload_img);
        itProjectBoardTitleTv=(TextView)v.findViewById(R.id.it_project_board_title_tv);
        itProjectBoardImageview=(CircleImageView)v.findViewById(R.id.it_project_board_imageview);
        itProjectBoardWriterTv=(TextView)v.findViewById(R.id.it_project_board_writer_tv);
        itProjectBoardDateTv=(TextView)v.findViewById(R.id.it_project_board_date_tv);
        tv1=(TextView)v.findViewById(R.id.tv1);
        itProjectBoardNumTv=(TextView)v.findViewById(R.id.it_project_board_num_tv);
        tv2=(TextView)v.findViewById(R.id.tv2);
        favoriteNice=(ShineButton)v.findViewById(R.id.favorite_nice);
        

    }
}


