package com.ration.dohee.collabo.DATA;

import android.content.SharedPreferences;

/**
 * Created by Samsung on 2017-06-23.
 */

public class DATA {
    public static enum Profile_state{
        keep,changed,delete
    }
    public static boolean _isLogin=false;
    public final static String RootURL="http://collabo.boasmedia.co.kr";
    public final static String ImgRootURL="http://collabo.boasmedia.co.kr/uploads/";
    public final static String Defaultimg="http://collabo.boasmedia.co.kr/img/profile.gif";
    public final static int GALLERY_DIRECT_CODE=100;
    public static boolean _isLogin() {
        if(User_INFO.getID().equals("ref")&&User_INFO.getPASS().equals("ref"))
            _isLogin=false;

        return _isLogin;
    }
    public static void set_isLogin(SharedPreferences sp) {
        DATA._isLogin =sp.getBoolean("Login",false);
    }
    public static void set_isLogin(SharedPreferences sp,boolean logined) {
        DATA._isLogin =logined;
        SharedPreferences.Editor edit=sp.edit();
        edit.putBoolean("Login",logined);
        edit.commit();
    }
}
