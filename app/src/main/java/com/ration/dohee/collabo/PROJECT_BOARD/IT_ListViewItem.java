package com.ration.dohee.collabo.PROJECT_BOARD;

/**
 * Created by User on 2017-06-28.
 */

public class IT_ListViewItem {


    public String board_name;
    public String imgurl;
    public String title;
    public String writer;
    public String date;
    public int num;
    public int idx;
    public int favid;

    public boolean isfavor=false;


    public int getIdx() {
        return idx;
    }

    public void setIdx(int idx) {
        this.idx = idx;
    }

    public String getImgurl() {
        return imgurl;
    }

    public String getTitle() {
        return title;
    }

    public String getWriter() {
        return writer;
    }

    public String getDate() {
        return date;
    }

    public int getNum() {
        return num;
    }

    public IT_ListViewItem(String img, String title, String writer
    , String date, int num, int idx){
        this.imgurl=img;
        this.title=title;
        this.writer=writer;
        this.date=date;
        this.num=num;
        this.idx=idx;

    }
    public IT_ListViewItem(String img, String title, String writer
            , String date, int num, int idx,String board_name){
        this.imgurl=img;
        this.title=title;
        this.writer=writer;
        this.date=date;
        this.num=num;
        this.idx=idx;
        this.board_name=board_name;

    }
}
