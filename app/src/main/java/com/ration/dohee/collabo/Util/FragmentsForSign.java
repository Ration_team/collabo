package com.ration.dohee.collabo.Util;

import android.view.View;
import android.view.ViewGroup;

import com.ration.dohee.collabo.DATA.DATA;

/**
 * Created by Samsung on 2017-07-18.
 */

public class FragmentsForSign extends android.support.v4.app.Fragment {

    public void SettingView (ViewGroup main, ViewGroup sign)
    {
        if(DATA._isLogin())
        {

            main.setVisibility(View.VISIBLE);
            sign.setVisibility(View.GONE);
        }
        else{
            main.setVisibility(View.GONE);
            sign.setVisibility(View.VISIBLE);
        }
    }
}
