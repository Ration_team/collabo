package com.ration.dohee.collabo.Https.Apply_Results_Box;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samsung on 2017-09-24.
 */

public class Apply_member {
    @SerializedName("id")
    int id;
    @SerializedName("idx")
    int idx;
    @SerializedName("wr_subject")
    String title;
    @SerializedName("mb_id")
    String mb_id;
    @SerializedName("mb_img")
    String mb_img;
    @SerializedName("selected")
    String selected;
    public boolean checked;

    public int getId() {
        return id;
    }

    public int getIdx() {
        return idx;
    }

    public String getTitle() {
        return title;
    }

    public String getMb_id() {
        return mb_id;
    }

    public String getMb_img() {
        return mb_img;
    }

    public String getSelected() {
        return selected;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
