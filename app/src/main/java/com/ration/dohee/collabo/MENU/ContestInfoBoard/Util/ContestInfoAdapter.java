package com.ration.dohee.collabo.MENU.ContestInfoBoard.Util;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ration.dohee.collabo.DATA.DATA;
import com.ration.dohee.collabo.DATA.UNIQ;
import com.ration.dohee.collabo.DATA.User_INFO;
import com.ration.dohee.collabo.Https.Favor_delresult;
import com.ration.dohee.collabo.Https.Favor_result;
import com.ration.dohee.collabo.Https.Favorite_info;
import com.ration.dohee.collabo.Https.HttpServiceClient;
import com.ration.dohee.collabo.R;
import com.sackcentury.shinebuttonlib.ShineButton;

import java.util.LinkedList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by User on 2017-08-19.
 */

public class ContestInfoAdapter extends BaseAdapter {
    ImageView img;
    ContestInfoItem mContestInfoItem;
    ShineButton favor;
    TextView txtSubject,txtStartDate,txtResvDate,txtCategory;
    Context mContext;

    int tempValue;
    public LinkedList<ContestInfoItem> items=new LinkedList<>();
    public ContestInfoAdapter(){

    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Context mContext=parent.getContext();

        if(convertView==null){
            LayoutInflater inflater=(LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate(R.layout.contest_list,parent,false);
        }

        txtSubject=(TextView)convertView.findViewById(R.id.contestInfo_list_subject);
        txtCategory=(TextView)convertView.findViewById(R.id.contestInfo_list_category);
        txtStartDate=(TextView)convertView.findViewById(R.id.contestInfo_list_start_date);
        txtResvDate=(TextView)convertView.findViewById(R.id.contestInfo_list_resv_date);
        img=(ImageView)convertView.findViewById(R.id.contestInfo_list_img);
        favor=(ShineButton)convertView.findViewById(R.id.contest_list_favorite_Btn);


        favor.setChecked(false);
        mContestInfoItem=items.get(position);
        txtSubject.setText(mContestInfoItem.Subject);
        txtCategory.setText(" "+mContestInfoItem.Category);
        try {
            txtStartDate.setText(" " + mContestInfoItem.StartDate.substring(2, 10));
            txtResvDate.setText(" ~ " + mContestInfoItem.ResvDate.substring(2, 10));
        }catch(StringIndexOutOfBoundsException e)
        {
            txtStartDate.setText(" " + mContestInfoItem.StartDate.substring(0, 8));
            txtResvDate.setText(" ~ " + mContestInfoItem.ResvDate.substring(0, 8));
        }
        if(User_INFO.getConInfoFavor()!=null){

            for(int i=0;i<User_INFO.getConInfoFavor().size();i++){
                Log.e("favors",User_INFO.getConInfoFavor().get(i).getIdx()+""+mContestInfoItem.idx);
                if(User_INFO.getConInfoFavor().get(i).getIdx()==mContestInfoItem.idx){
                    mContestInfoItem.isfavor=true;
                    Log.e("Checked here",User_INFO.getConInfoFavor().get(i).getIdx()+""+mContestInfoItem.idx);
                    favor.setChecked(true);
                    tempValue=i;
                    mContestInfoItem.favid=User_INFO.getConInfoFavor().get(i).getId();
                    break;
                }
            }
        }
        favor.setOnClickListener(new View.OnClickListener() {
            ContestInfoItem item=mContestInfoItem;
            @Override
            public void onClick(View v) {
                final View view=v;
                favor=(ShineButton)view.findViewById(R.id.contest_list_favorite_Btn);

                if(!favor.isChecked()){
                    for(int i=0;i<User_INFO.getConInfoFavor().size();i++){
                        if(item.favid==User_INFO.getConInfoFavor().get(i).getId()){
                            tempValue=i;
                            break;
                        }
                    }
                    Call<Favor_delresult> repos= HttpServiceClient.getApiService().Favorite_Delete(
                            User_INFO.getConInfoFavor().get(tempValue).getId(), UNIQ.unique_id,User_INFO.getID(),0
                    );
                    repos.enqueue(new Callback<Favor_delresult>() {
                        @Override
                        public void onResponse(Call<Favor_delresult> call, Response<Favor_delresult> response) {
                            if(response.isSuccessful()){
                                if(response.body().getError().equals("false")){

                                    User_INFO.getConInfoFavor().remove(tempValue);
                                    item.isfavor=false;
                                    item.favid=-1;
                                }
                            }
                            else{
                                Log.e("Server Error",response.errorBody().toString());
                            }
                        }

                        @Override
                        public void onFailure(Call<Favor_delresult> call, Throwable t) {
                            Log.e("Retros Error",t.getMessage().toString());
                        }
                    });
                }
                else if(favor.isChecked()){
                    final Call<Favor_result> repos=HttpServiceClient.ContestInfo_Retros_FavorAdd(item.idx,item.Subject);
                    repos.enqueue(new Callback<Favor_result>() {
                        @Override
                        public void onResponse(Call<Favor_result> call, Response<Favor_result> response) {
                            if(response.isSuccessful()){
                                if(response.body().getError().equals("false")){

                                    Favorite_info ref=response.body().getResult().get(0);
                                    item.isfavor=true;
                                    item.favid=ref.getId();
                                    User_INFO.getConInfoFavor().add(ref);
                                }
                            }
                            else{
                                Log.e("Server Error",response.errorBody().toString());
                            }
                        }

                        @Override
                        public void onFailure(Call<Favor_result> call, Throwable t) {
                            Log.e("Retros Error",t.getMessage().toString());
                        }
                    });
                }
            }
        });
        Glide.with(parent.getContext()).load(DATA.ImgRootURL+mContestInfoItem.ImgUrl).error(R.drawable.default_img).into(img);

        Log.e("Img TEst",DATA.ImgRootURL+mContestInfoItem.ImgUrl);
        return convertView;
    }
}
