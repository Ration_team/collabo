package com.ration.dohee.collabo.PROJECT_BOARD;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ration.dohee.collabo.DATA.DATA;
import com.ration.dohee.collabo.Https.Apply_Results_Box.Apply_member;
import com.ration.dohee.collabo.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Samsung on 2017-09-24.
 */
public class Recruit_Apply_List_Adapter extends RecyclerView.Adapter<Recruit_Apply_List_ViewHolder>{
    public ArrayList<Apply_member> items;
    private Context context;
    int number;
    TextView guests;
    public Recruit_Apply_List_Adapter(Context context,TextView guests) {
        super();
        this.guests=guests;
        this.context=context;
    }

    public void setItems(ArrayList<Apply_member> items) {
       this.items=new ArrayList<>();
        this.items.addAll(items);
    }

    public ArrayList<Apply_member> getItems() {
        return items;
    }

    @Override
    public Recruit_Apply_List_ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_main__application_list__custom_adapter_list_view, parent, false);

        Recruit_Apply_List_ViewHolder viewHolder=new Recruit_Apply_List_ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final Recruit_Apply_List_ViewHolder holder,final int position) {
        Glide.with(context).load(DATA.ImgRootURL+"/"+items.get(position).getMb_img()).into(holder.img);


        Log.e( "onBindViewHolder: ",DATA.ImgRootURL+"/"+items.get(position).getMb_img() );
        holder.nametv.setText(items.get(position).getMb_id());
        holder.seleccheck.setChecked(false);
        holder.seleccheck.setTag(items.get(position));
        holder.seleccheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckBox cb = (CheckBox) v;
                Apply_member item = (Apply_member) cb.getTag();

                item.setChecked(cb.isChecked());
                items.get(position).setChecked(item.isChecked());

                if(cb.isChecked())
                {
                    number++;
                }
                else{
                    number--;
                }
                guests.setText("'"+number+"명'선택됨");
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}

class Recruit_Apply_List_ViewHolder extends RecyclerView.ViewHolder {

    TextView nametv;
    CheckBox seleccheck;
    CircleImageView img;
    public Recruit_Apply_List_ViewHolder(View v) {
        super(v);
        nametv= (TextView) v.findViewById(R.id.tv_name);
        seleccheck=(CheckBox)v.findViewById(R.id.applicationlistcheck);
        img=(CircleImageView)v.findViewById(R.id.iv_img);
    }
}
