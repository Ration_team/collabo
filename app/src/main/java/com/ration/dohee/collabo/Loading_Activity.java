package com.ration.dohee.collabo;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.android.gms.ads.MobileAds;
import com.google.firebase.iid.FirebaseInstanceId;
import com.ration.dohee.collabo.DATA.DATA;
import com.ration.dohee.collabo.DATA.UNIQ;
import com.ration.dohee.collabo.DATA.User_INFO;
import com.ration.dohee.collabo.Https.HttpServiceClient;
import com.ration.dohee.collabo.Https.Result;
import com.ration.dohee.collabo.Https.Results;
import com.ration.dohee.collabo.Util.CustomActivityForFonts;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Loading_Activity extends CustomActivityForFonts {
    final static int MY_PERMISSION_CODE_TWO=10150;
    final static int MY_PERMISSION_CODE_ONE=10151;
    Handler hd = new Handler();
    int time=2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);
        String token= FirebaseInstanceId.getInstance().getToken();
        MobileAds.initialize(this, getString(R.string.admob_app_id));
//        Intent inte =new Intent(this,MyFirebaseInstanceIDService.class);
//        startService(inte);
//        inte=new Intent(this,MyFirebaseMessagingService.class);
//        startService(inte);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        User_INFO.UserSetting(getSharedPreferences("Auto_Login",MODE_PRIVATE));
        DATA.set_isLogin(getSharedPreferences("Auto_Login",MODE_PRIVATE));
        Log.e("islogin",DATA._isLogin()+"\n"+User_INFO.getID()+User_INFO.getPASS());
        if(ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED
                &&ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED)
        {
            CheckPermissions();
        }
        else if(DATA._isLogin())
        {
            User_INFO.setID(getSharedPreferences("Auto_Login",MODE_PRIVATE).getString("id","refe"));
            User_INFO.setPASS(getSharedPreferences("Auto_Login",MODE_PRIVATE).getString("pass",""));
            Logging();
        }
        else if(!DATA._isLogin())
        {
            startActivity(new Intent(getApplicationContext(),MainActivity.class));
            finish();
        }




    }




    void Logging(){
        HttpServiceClient.Retros_SIGN(User_INFO.getID(), User_INFO.getPASS())
                .enqueue(new Callback<Results>() {
                    @Override
                    public void onResponse(Call<Results> call, Response<Results> response) {
                        // http 연결 성공
                        if (response.isSuccessful())
                            HttpComplete(response.body());
                        else{
                            Log.e("Server Error",response.errorBody()+"");
                        }
                        Log.e("?","?");
                    }
                    @Override
                    public void onFailure(Call<Results> call, Throwable t) {
                        // http 연결 실패
                        Log.e("Retros Throwable" , t.getMessage());
                        Log.e("?","!");
                    }
                });
    }
    void HttpComplete(Results results){


        String error=results.getError();

        if(error.equals("false")){
            Result res=results.getResult();
            String auth=res.getMb_auth();

            if(auth.equals("active")){
                User_INFO.setEMAIL(res.getMb_email());
                UNIQ.unique_id=res.getUnique_id();
                Toast.makeText(this, "Wellcom to Collabo", Toast.LENGTH_SHORT).show();
                Log.e("UNIQ",UNIQ.unique_id);

                HttpServiceClient.Retros_FavoriteList();
                Intent intent =new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtra("logon",true);
                startActivity(intent);
                return;
            }
        }
        else if(error.equals("true")){
            String error_msg=results.getMsg();
            Toast.makeText(this, error_msg, Toast.LENGTH_SHORT).show();
            Log.e("Login Error msg",error_msg);
            startActivity(new Intent(getApplicationContext(),Login_Activity.class));
        }
    }
    public void CheckPermissions(){

        boolean flag[]={false,false};
        if(ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED
                &&ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED)
        {
            String permissions[]={Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE};
            if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)){
                permissions=new String[1];
                permissions[0]=Manifest.permission.READ_EXTERNAL_STORAGE;
                flag[0]=true;
            }
            if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)){
                permissions=new String[1];
                permissions[0]=Manifest.permission.WRITE_EXTERNAL_STORAGE;
                flag[1]=true;
            }
            if(flag[0]&&flag[1]){
                permissions=new String[2];
                permissions[0]=Manifest.permission.WRITE_EXTERNAL_STORAGE;
                permissions[1]=Manifest.permission.READ_EXTERNAL_STORAGE;
            }
            else if (!(flag[0]&&flag[1])){
                ActivityCompat.requestPermissions(this,permissions,MY_PERMISSION_CODE_TWO);
                return;
            }
            ActivityCompat.requestPermissions(this,permissions,MY_PERMISSION_CODE_ONE);
        }else{
            Log.e("?","$");
            hd.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Log.e("?",">");
                    startActivity(new Intent(getApplicationContext(),MainActivity.class));
                    finish();
                }
            }, time);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode==MY_PERMISSION_CODE_TWO){
            if(grantResults.length==2&&grantResults[0]== PackageManager.PERMISSION_GRANTED&&grantResults[1]==PackageManager.PERMISSION_GRANTED){
                hd.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Log.e("?",">");
                        startActivity(new Intent(getApplicationContext(),MainActivity.class));
                        finish();
                    }
                }, time);
            }
            else{
                Toast.makeText(this, "권한설정후 사용 가능합니다11", Toast.LENGTH_SHORT).show();
                finishAffinity();
            }
        }
        else if (requestCode==MY_PERMISSION_CODE_ONE){
            if(grantResults.length>0&&grantResults[0]==PackageManager.PERMISSION_GRANTED){
                hd.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Log.e("?",">");
                        startActivity(new Intent(getApplicationContext(),MainActivity.class));
                        finish();
                    }
                }, time);
            }
            else {
                Toast.makeText(this, "권한설정후 사용 가능합니다22", Toast.LENGTH_SHORT).show();
                finishAffinity();
            }
        }
    }
}
