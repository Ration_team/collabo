package com.ration.dohee.collabo.Https;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samsung on 2017-07-20.
 */

public class Board_Commits {
    @SerializedName("error")
    String error;
    @SerializedName("message")
    String msg;
    @SerializedName("result")
    Board_commit result;


    public String getError() {
        return error;
    }

    public String getMsg() {
        return msg;
    }

    public Board_commit getResult() {
        return result;
    }
}
