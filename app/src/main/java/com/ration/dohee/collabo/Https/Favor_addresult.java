package com.ration.dohee.collabo.Https;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samsung on 2017-08-22.
 */

public class Favor_addresult {
    @SerializedName("error")
    String error;
    @SerializedName("result")
    Favorite_info result;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Favorite_info getResult() {
        return result;
    }

    public void setResult(Favorite_info result) {
        this.result = result;
    }
}
