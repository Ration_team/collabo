package com.ration.dohee.collabo.Util;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.ration.dohee.collabo.Board_NOTICE.Notice_Activity;
import com.ration.dohee.collabo.DATA.ActionBarData;
import com.ration.dohee.collabo.MENU.ContestInfoBoard.ContestInfoListActivity;
import com.ration.dohee.collabo.MENU.Guide_Activity;
import com.ration.dohee.collabo.MENU.MyCareer_Activity;
import com.ration.dohee.collabo.MENU.Option_Activity;
import com.ration.dohee.collabo.MainActivity;
import com.ration.dohee.collabo.R;
import com.ration.dohee.collabo.SearchActivity;

import static android.view.Gravity.START;

/**
 * Created by User on 2017-06-21.
 */

public class ActionBar extends CustomActivityForFonts implements NavigationView.OnNavigationItemSelectedListener{
    public DrawerArrowDrawable drawerArrowDrawable;
    public float offset;
    public boolean flipped;
    public DrawerLayout drawer;
    public ImageView imageView;
    public ImageButton searchBtn;
    public NavigationView navigationView;
    public ImageButton imageButton;

    public void SetActionBar(View actionbarView){
        drawer = (DrawerLayout) findViewById(ActionBarData.DrawerLayout);
        imageView = (ImageView) actionbarView.findViewById(R.id.drawer_indicator);
        navigationView = (NavigationView) findViewById(ActionBarData.DrawerLayout_Navmenu);
        imageButton=(ImageButton) findViewById(R.id.imageButton);
        searchBtn = (ImageButton)findViewById(R.id.searchBtn);
        actionBarInit();

    }
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void actionBarInit(){

        final Resources resources = getResources();

        drawerArrowDrawable = new DrawerArrowDrawable(resources);
        drawerArrowDrawable.setStrokeColor(Color.parseColor("#ffffff"));
        this.imageView.setImageDrawable(drawerArrowDrawable);

        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setBackgroundColor(Color.WHITE);
        this.drawer.setDrawerListener(new DrawerLayout.SimpleDrawerListener() {
            @Override public void onDrawerSlide(View drawerView, float slideOffset) {
                offset = slideOffset;

                // Sometimes slideOffset ends up so close to but not quite 1 or 0.
                if (slideOffset >= .995) {
                    flipped = true;
                    drawerArrowDrawable.setFlip(flipped);
                } else if (slideOffset <= .005) {
                    flipped = false;
                    drawerArrowDrawable.setFlip(flipped);
                }
                drawerArrowDrawable.setParameter(offset);
            }
        });

        this.imageView.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                if (drawer.isDrawerVisible(START)) {
                    drawer.closeDrawer(START);
                } else {
                    drawer.openDrawer(START);
                }
            }
        });
        this.searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ActionBar.this,SearchActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        this.imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ActionBar.this,MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

                Log.e("Token register", FirebaseInstanceId.getInstance().getToken()+"");
            }
        });

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();
        Intent inte=null;
        if (id == R.id.menu_home) {
            inte=new Intent(this, MainActivity.class);
        }
        else if(id==R.id.menu_guide)
        {
            inte=new Intent(this, Guide_Activity.class);

        }
        else if(id ==R.id.menu_Career){
            inte=new Intent(this, MyCareer_Activity.class);
        }
        else if(id ==R.id.menu_contestlist){
            inte=new Intent(this,ContestInfoListActivity.class);
        }
        else if(id ==R.id.menu_notice){
            inte=new Intent(getApplicationContext(),Notice_Activity.class);
        }
        else if(id ==R.id.menu_Option){
           inte=new Intent (this,Option_Activity.class);
//        }
//        else if(id==R.id.test_write){
//            inte=new Intent(this, TEST_WRITE_ACTIVITY.class);
//        }else if(id==R.id.test_write_img){
//            inte=new Intent(this, TEST_WRITE_IMG_ACTIVITY.class);
        }
        if(inte!=null) {
            inte.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(inte);
        }

        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(ActionBarData.DrawerLayout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
}
