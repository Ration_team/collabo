package com.ration.dohee.collabo.Https;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samsung on 2017-07-20.
 */

public class Board_commit {
    @SerializedName("idx")
    int idx;
    @SerializedName("wr_subject")
    String wr_subject;
    @SerializedName("wr_content")
    String wr_content;
    @SerializedName("wr_date")
    String wr_date;
    @SerializedName("hit_count")
    String hit_count;
    @SerializedName("wr_file")
    String wr_file;
    @SerializedName("mb_id")
    String mb_id;

    @SerializedName("board_count")
    int board_sumcount;

    public int getBoard_sumcount() {
        return board_sumcount;
    }

    public int getIdx() {
        return idx;
    }

    public String getWr_subject() {
        return wr_subject;
    }

    public String getWr_content() {
        return wr_content;
    }

    public String getWr_date() {
        return wr_date;
    }

    public String getHit_count() {
        return hit_count;
    }

    public String getWr_file() {
        return wr_file;
    }

    public String getMb_id() {
        return mb_id;
    }


}
