package com.ration.dohee.collabo.MENU;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.nightonke.jellytogglebutton.JellyToggleButton;
import com.nightonke.jellytogglebutton.State;
import com.ration.dohee.collabo.DATA.ActionBarData;
import com.ration.dohee.collabo.DATA.DATA;
import com.ration.dohee.collabo.DATA.UNIQ;
import com.ration.dohee.collabo.DATA.User_INFO;
import com.ration.dohee.collabo.Https.HttpServiceClient;
import com.ration.dohee.collabo.Https.Logout_Results;
import com.ration.dohee.collabo.License;
import com.ration.dohee.collabo.Login_Activity;
import com.ration.dohee.collabo.MainActivity;
import com.ration.dohee.collabo.R;
import com.ration.dohee.collabo.Util.ActionBar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**`
 * Created by User on 2017-06-21.
 */

public class Option_Activity extends ActionBar implements View.OnClickListener,JellyToggleButton.OnStateChangeListener{

    JellyToggleButton option_alert_btn;
    Button option_sign_btn;

    TextView Licens;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_option);
        SetActionBar((View)findViewById(ActionBarData.ActionBar));
        option_alert_btn=(JellyToggleButton) findViewById(R.id.option_alert_btn);
        option_sign_btn=(Button)findViewById(R.id.option_sign_btn);

        option_alert_btn.setOnStateChangeListener(this);
        option_sign_btn.setOnClickListener(this);
        Licens=(TextView)findViewById(R.id.License);

        Licens.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),License.class));
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        option_sign_btn.setBackground(getResources().getDrawable(DATA._isLogin()?R.drawable.logoutbg:R.drawable.loginbg));
        option_sign_btn.setText(getResources().getString(DATA._isLogin()?R.string.option_logout_txt:R.string.option_login));
    }
    // 알림 on:off
    @Override
    public void onStateChange(float process, State state, JellyToggleButton jtb) {
        if(state.equals(State.LEFT)){
            //ON
        }
        if(state.equals(State.RIGHT)){
            //OFF
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.option_sign_btn:{
                Log.e("dsa", String.valueOf(DATA._isLogin()));
                if(DATA._isLogin()){
                    HttpServiceClient.Retros_LOGOUT()
                            .enqueue(new Callback<Logout_Results>() {
                                @Override
                                public void onResponse(Call<Logout_Results> call, Response<Logout_Results> response) {
                                    HttpComplete(response.body());
                                }

                                @Override
                                public void onFailure(Call<Logout_Results> call, Throwable t) {
                                    Log.e("Retros Throwable" , t.getMessage());
                                }
                            });
                }
                else{
                    startActivity(new Intent(this, Login_Activity.class));
                    finish();
                }
            }
            break;
        }
    }

    void HttpComplete(Logout_Results results){
        String error=results.getError();

        if(error.equals("false")){

            UNIQ.unique_id=null;
            SharedPreferences pref=getSharedPreferences("Auto_Login",MODE_PRIVATE);
            DATA.set_isLogin(pref,false);
            User_INFO.UserSetter("","","",pref);
            Intent intent=new Intent(this,MainActivity.class);
            try{
            Log.e("islogin", DATA._isLogin()+"\n"+User_INFO.getID()+ User_INFO.getPASS());

            Log.e("UNIQ",UNIQ.unique_id);}
            catch (Exception e){Log.e("da",e.getMessage());}
            startActivity(intent);
            finish();
            Toast.makeText(Option_Activity.this, results.getMsg(), Toast.LENGTH_SHORT).show();

        }
        else if(error.equals("true")){
            String error_msg=results.getMsg();
            Toast.makeText(Option_Activity.this, error_msg, Toast.LENGTH_SHORT).show();
            Log.e("Login Error msg",error_msg);
        }

    }
}
