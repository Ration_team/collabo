package com.ration.dohee.collabo.Main__Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.ration.dohee.collabo.DATA.User_INFO;
import com.ration.dohee.collabo.Https.Board_Recruit_Result;
import com.ration.dohee.collabo.Https.Board_Recruit_Results;
import com.ration.dohee.collabo.Https.HttpContestInfo.ContestInfoList;
import com.ration.dohee.collabo.Https.HttpContestInfo.ContestInfotListResult;
import com.ration.dohee.collabo.Https.HttpServiceClient;
import com.ration.dohee.collabo.Login_Activity;
import com.ration.dohee.collabo.MENU.ContestInfoBoard.ContestInfoViewActivity;
import com.ration.dohee.collabo.MENU.ContestInfoBoard.Util.ContestInfoAdapter;
import com.ration.dohee.collabo.MENU.ContestInfoBoard.Util.ContestInfoItem;
import com.ration.dohee.collabo.PROJECT_BOARD.IT_CustomAdapterListView;
import com.ration.dohee.collabo.PROJECT_BOARD.IT_ListViewItem;
import com.ration.dohee.collabo.PROJECT_BOARD.project_seleceted;
import com.ration.dohee.collabo.R;
import com.ration.dohee.collabo.Util.FragmentsForSign;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import lib.kingja.switchbutton.SwitchMultiButton;
import retrofit2.Response;

public class Favorite_Fragment extends FragmentsForSign {

    ListView favoriteListFavorboardRecycle;
    IT_CustomAdapterListView IT_Adapter;
    ContestInfoAdapter Con_Adapter;
    LinkedList<IT_ListViewItem> RecruitItem;
    LinkedList<ContestInfoItem> ConInfoItem;
    int MaxPager=0;
    Unbinder unbinder;
    int idx=0;

    android.os.Handler handle;
    public Favorite_Fragment() {
    }

    SwitchMultiButton s;
    //    private List<String> TextList = Arrays.asList("매칭게시판", "공모전");
    private List<String> TextList;
    LinearLayout main, sign;
    Button btn;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.activity_favorite, container, false);
        main = (LinearLayout) view.findViewById(R.id.myfavorite_main_view);
        sign = (LinearLayout) view.findViewById(R.id.myfavorite_sign_view);

        handle=new android.os.Handler(){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                SettingList();
            }
        };


        favoriteListFavorboardRecycle=(ListView)view.findViewById(R.id.favorite_list_favorboard_listview);
        IT_Adapter=new IT_CustomAdapterListView("");
        Con_Adapter=new ContestInfoAdapter();
        RecruitItem=new LinkedList<>();
        ConInfoItem=new LinkedList<>();
        TextList = Arrays.asList("매칭게시판", "공모전정보");
        s = (SwitchMultiButton) view.findViewById(R.id.favorite_toggle_boardtype_switchbtn);
        s.setText(TextList);
        s.setOnSwitchListener(onSwitchListener);
        btn = (Button) view.findViewById(R.id.myfavorite_sign_btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity().getApplicationContext(), Login_Activity.class));
            }
        });

        favoriteListFavorboardRecycle.setAdapter(IT_Adapter);
        favoriteListFavorboardRecycle.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(favoriteListFavorboardRecycle.getAdapter()==IT_Adapter){
                    Intent inte = new Intent(getContext(), project_seleceted.class);
                    IT_ListViewItem ref = IT_Adapter.items.get(position);
                    inte.putExtra("idx", ref.getIdx());
                    inte.putExtra("isfavor", ref.isfavor);
                    inte.putExtra("board_name", ref.board_name);
                    startActivity(inte);
                }
                else if(favoriteListFavorboardRecycle.getAdapter()==Con_Adapter){
                    Intent intent=new Intent(getContext(),ContestInfoViewActivity.class);
                    ContestInfoItem ref=Con_Adapter.items.get(position);
                    intent.putExtra("idx",ref.idx);
                    startActivity(intent);
                }
            }
        });




        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    private SwitchMultiButton.OnSwitchListener onSwitchListener = new SwitchMultiButton.OnSwitchListener() {
        @Override
        public void onSwitch(int position, String tabText) {
            if (tabText.equals(TextList.get(0))) {
                favoriteListFavorboardRecycle.setAdapter(IT_Adapter);

            } else if (tabText.equals(TextList.get(1))) {
                favoriteListFavorboardRecycle.setAdapter(Con_Adapter);

            }
        }
    };

    @Override
    public void onStart() {
        super.onStart();
        SettingView(main, sign);

        RetroThread thread=new RetroThread(handle);
        thread.start();

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }





    private void SettingList() {


        IT_Adapter.items.clear();
        Con_Adapter.items.clear();
        IT_Adapter.items.addAll(RecruitItem);
        Con_Adapter.items.addAll(ConInfoItem);

        favoriteListFavorboardRecycle.setAdapter(IT_Adapter);




    }


    class RetroThread extends Thread{
        android.os.Handler handler;

        public RetroThread(android.os.Handler handler) {
            this.handler = handler;
        }

        @Override
        public void run() {
            super.run();
            idx=0;
            Response response=null;
            ConInfoItem.clear();
            RecruitItem.clear();

//            board_recruit 게시판 긁어오기
            do {
                try {
                    response = HttpServiceClient.Retros_RecruitList("board_recruit", idx).execute();
                    Board_Recruit_Results refs=(Board_Recruit_Results)response.body();
                    if (refs.getError().equals("false")) {

                        Board_Recruit_Result res;
                        MaxPager = refs.getPage();
                        idx++;

                        for (int j = 0; j < User_INFO.getfavors("board_recruit").size(); j++) {
                            for (int i = 0; i < refs.getResult().size(); i++) {
                                res = refs.getResult().get(i);
                                if (User_INFO.getfavors("board_recruit").get(j).getIdx() == res.getIdx()) {
                                    IT_ListViewItem temp = new IT_ListViewItem(res.getWr_file(), res.getWr_subject(), res.getMb_id(), res.getWr_date(), res.getNum(), res.getIdx());
                                    temp.board_name = "board_recruit";
                                    RecruitItem.add(temp);
                                }
                            }
                        }
                    }
                }catch (Exception e)
                {
                    Log.e("exception ",e.getMessage()+"");
                    break;
                }

            }while(idx<MaxPager);

            idx=0;
//            board_project 게시판 긁어오기
            do {
                try {
                    response = HttpServiceClient.Retros_RecruitList("board_project", idx).execute();
                    Board_Recruit_Results refs=(Board_Recruit_Results)response.body();
                    if (refs.getError().equals("false")) {

                        Board_Recruit_Result res;
                        MaxPager = refs.getPage();
                        idx++;

                        for (int j = 0; j < User_INFO.getfavors("board_project").size(); j++) {
                            for (int i = 0; i < refs.getResult().size(); i++) {
                                res = refs.getResult().get(i);
                                if (User_INFO.getfavors("board_project").get(j).getIdx() == res.getIdx()) {
                                    IT_ListViewItem temp = new IT_ListViewItem(res.getWr_file(), res.getWr_subject(), res.getMb_id(), res.getWr_date(), res.getNum(), res.getIdx());
                                    temp.board_name = "board_project";
                                    RecruitItem.add(temp);
                                }
                            }
                        }
                    }
                }catch (Exception e)
                {
                    Log.e("exception ",e.getMessage()+"");
                    break;
                }

            }while(idx<MaxPager);




            idx=0;

            do {
                try {
                    response = HttpServiceClient.retrosListResult("contest_list", idx).execute();

                    ContestInfotListResult ref = (ContestInfotListResult)response.body();
                    if (ref.getError().equals("false")) {
                        MaxPager = ref.getPage();
                        idx++;



                        for (int j = 0; j < User_INFO.getConInfoFavor().size(); j++) {
                            for (int i = 0; i < ref.getResult().size(); i++) {
                                ContestInfoList res = ref.getResult().get(i);
                                if (User_INFO.getConInfoFavor().get(j).getIdx() == res.getIdx()) {
                                    ConInfoItem.add(new ContestInfoItem(res.getWr_file(), res.getWr_subject(),
                                            res.getCategory(), res.getStart_date(), res.getResv_date(), res.getIdx()));
                                }
                            }
                        }
                    }
                }
                catch(Exception e){
                    Log.e("excetion",e.getMessage()+"");
                    break;

                }
            }while (idx < MaxPager) ;

            handler.sendEmptyMessage(1);
        }
    }



}
