package com.ration.dohee.collabo.Https;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samsung on 2017-09-15.
 */

public class Recruit_Apply {
    @SerializedName("error")
    String error;
    @SerializedName("message")
    String msg;

    public String getError() {
        return error;
    }

    public String getMsg() {
        return msg;
    }
}
