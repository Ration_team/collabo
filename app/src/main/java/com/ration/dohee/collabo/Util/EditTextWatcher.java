package com.ration.dohee.collabo.Util;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * Created by Samsung on 2017-08-08.
 */

public abstract class EditTextWatcher implements TextWatcher {



    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }
    @Override
    public abstract void onTextChanged(CharSequence s, int start, int before, int count);
    @Override
    public void afterTextChanged(Editable s) {

    }
}
