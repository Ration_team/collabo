package com.ration.dohee.collabo.Https;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Samsung on 2017-07-13.
 */

public class Board_ListResults {
    @SerializedName("error")
    String error;
    @SerializedName("result")
    ArrayList<Board_ListResult> result;

    public String getError() {
        return error;
    }

    public ArrayList<Board_ListResult> getResult() {
        return result;
    }
}
