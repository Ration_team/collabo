package com.ration.dohee.collabo.Https;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Samsung on 2017-08-09.
 */

public class Favor_result {
    @SerializedName("error")
    String error;

    @SerializedName("result")
    ArrayList<Favorite_info> result;

    @SerializedName("message")
    String message;

    @SerializedName("total_count")
    int total_count;

    @SerializedName("page_count")
    int page_count;

    public void setError(String error) {
        this.error = error;
    }



    public void setMessage(String message) {
        this.message = message;
    }

    public int getTotal_count() {
        return total_count;
    }

    public void setTotal_count(int total_count) {
        this.total_count = total_count;
    }

    public int getPage_count() {
        return page_count;
    }

    public void setPage_count(int page_count) {
        this.page_count = page_count;
    }

    public String getMessage() {
        return message;
    }

    public String getError() {
        return error;
    }

    public ArrayList<Favorite_info> getResult() {
        return result;
    }

    public void setResult(ArrayList<Favorite_info> result) {
        this.result = result;
    }
}
