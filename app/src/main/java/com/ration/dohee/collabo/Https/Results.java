package com.ration.dohee.collabo.Https;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samsung on 2017-07-07.
 */


public class Results{
    @SerializedName("error")
    String error;
    @SerializedName("result")
    Result result;
    @SerializedName("error_msg")
    String msg;

    public String getMsg() {
        return msg;
    }

    public String getError() {
        return error;
    }

    public Result getResult() {
        return result;
    }

}