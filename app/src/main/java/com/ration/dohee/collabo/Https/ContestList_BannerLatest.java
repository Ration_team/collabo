package com.ration.dohee.collabo.Https;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Samsung on 2017-10-26.
 */

public class ContestList_BannerLatest {

    @SerializedName("error")
    String error;
    @SerializedName("result")
    ArrayList<ContestList_BannerItem> result;

    public String getError() {
        return error;
    }

    public ArrayList<ContestList_BannerItem> getResult() {
        return result;
    }

}


