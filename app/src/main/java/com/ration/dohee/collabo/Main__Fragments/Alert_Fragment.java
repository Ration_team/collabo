package com.ration.dohee.collabo.Main__Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.ration.dohee.collabo.Login_Activity;
import com.ration.dohee.collabo.R;
import com.ration.dohee.collabo.Util.FragmentsForSign;

/**
 * Created by User on 2017-06-22.
 */

public class Alert_Fragment extends FragmentsForSign {
    public Alert_Fragment(){}
    LinearLayout main,sign;
    Button btn;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view=inflater.inflate(R.layout.activity_alert,container,false);
        main=(LinearLayout)view.findViewById(R.id.myalert_main_view);
        sign=(LinearLayout)view.findViewById(R.id.myalert_sign_view);
        SettingView(main,sign);

        btn=(Button)view.findViewById(R.id.myalert_sign_btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity().getApplicationContext(), Login_Activity.class));
            }
        });
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        SettingView(main,sign);
    }

}
