package com.ration.dohee.collabo.Main__Fragments;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ration.dohee.collabo.Board_NOTICE.Notice_Seleceted_Activity;
import com.ration.dohee.collabo.DATA.DATA;
import com.ration.dohee.collabo.DATA.UNIQ;
import com.ration.dohee.collabo.DATA.User_INFO;
import com.ration.dohee.collabo.Https.Board_ListResult;
import com.ration.dohee.collabo.Https.Board_ListResults;
import com.ration.dohee.collabo.Https.ContestList_BannerLatest;
import com.ration.dohee.collabo.Https.HttpContestInfo.Default_result;
import com.ration.dohee.collabo.Https.HttpServiceClient;
import com.ration.dohee.collabo.Https.Profile_Data;
import com.ration.dohee.collabo.Https.Profile_View;
import com.ration.dohee.collabo.ListUtil.CareerListAdapter;
import com.ration.dohee.collabo.ListUtil.Notice_Board_item;
import com.ration.dohee.collabo.Login_Activity;
import com.ration.dohee.collabo.MENU.ContestInfoBoard.ContestInfoViewActivity;
import com.ration.dohee.collabo.PROJECT_BOARD.IT_Activity;
import com.ration.dohee.collabo.R;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;
import ezy.ui.view.NoticeView;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ss.com.bannerslider.banners.RemoteBanner;
import ss.com.bannerslider.events.OnBannerClickListener;
import ss.com.bannerslider.views.BannerSlider;

import static android.app.Activity.RESULT_OK;
import static com.ration.dohee.collabo.DATA.DATA.Profile_state.changed;
import static com.ration.dohee.collabo.DATA.DATA.Profile_state.delete;
import static com.ration.dohee.collabo.R.id.main_contest_btn;

public class Home_Fragment extends Fragment implements View.OnClickListener {

    ArrayList<Collabo_Banner> Banners;
    DATA.Profile_state img_state=DATA.Profile_state.keep;
    Button main_contents_it_btn,main_project_btn;
    ImageButton main_profile_btn;
    ImageView pic;
    TextView name, mail, listnonetv;
    ExpandableLayout ex;
    Profile_Data profiledata;
    BannerSlider banner;
    TextView edit_profile_img;
    LinearLayout edit_profile_Btn;
    ListView list;
    File file;
    CareerListAdapter adapter;
    ArrayList<Notice_Board_item> board_items = new ArrayList<>();
    @BindView(R.id.main_profile_tv)
    TextView mainProfileTv;
    @BindView(R.id.edit_profile_edt)
    EditText editProfileEdt;
    Unbinder unbinder;

    public Home_Fragment() {
    }

    NoticeView vNotice;
    TextView edit_profile_tv;
    boolean editabled = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.activity_home, container, false);
        ex = (ExpandableLayout) view.findViewById(R.id.expandable_layout_1);
        pic = (CircleImageView) view.findViewById(R.id.main_profile_img_imgview);
        banner = (BannerSlider) view.findViewById(R.id.contest_info);
        main_profile_btn = (ImageButton) view.findViewById(R.id.main_profile_btn);
        main_project_btn=(Button)view.findViewById(main_contest_btn);
        main_contents_it_btn = (Button) view.findViewById(R.id.main_project_btn);
        edit_profile_tv = (TextView) view.findViewById(R.id.edit_profile_tv);
        list = (ListView) view.findViewById(R.id.main_home_profile_career_list);
        listnonetv = (TextView) view.findViewById(R.id.main_home_career_tv);
        name = (TextView) view.findViewById(R.id.main_profile_name);
        mail = (TextView) view.findViewById(R.id.main_profile_mail);
        edit_profile_img = (TextView) view.findViewById(R.id.edit_profile_img);
        edit_profile_Btn = (LinearLayout) view.findViewById(R.id.edit_profile_Btn);



        GetNoticeList();
        edit_profile_tv.setText("수정하기");
        edit_profile_img.setVisibility(View.INVISIBLE);
        vNotice = (NoticeView) view.findViewById(R.id.notice);
        edit_profile_Btn.setVisibility(View.INVISIBLE);
        edit_profile_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editabled) {

                    SweetAlertDialog dlg=new SweetAlertDialog(getContext(),SweetAlertDialog.WARNING_TYPE);
                    dlg.setCancelText("지우기");
                    dlg.setTitleText("사진 변경");
                    dlg.setContentText("취소 하시려면 뒤로가기를 눌러주세요.");
                    dlg.setConfirmText("갤러리");
                    dlg.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismiss();
                            Intent intent = new Intent(Intent.ACTION_PICK);
                            intent.setData(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            intent.setType("image/*");
                            startActivityForResult(intent, DATA.GALLERY_DIRECT_CODE);
                        }
                    });
                    dlg.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismiss();
                            Glide.with(getContext()).load(DATA.Defaultimg).asGif().into(pic);
                            img_state= delete;
                        }
                    });
                    dlg.setCanceledOnTouchOutside(true);
                    dlg.show();

                }
            }
        });
        edit_profile_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (editabled) {//수정 완료 시
                    // TODO: 2017-09-27 profile edit통신로직 포함 필요
//                    DATA.Profile_state        keep == 기존사진 유지
//                                              changed == 바뀐사진 업로드
//                                              delete == 디폴트 사진으로 변경


                    HashMap<String, RequestBody> map=new HashMap<String, RequestBody>();

                    map.put("mb_id",ToRequesBody(User_INFO.getID()));
                    map.put("mb_password",ToRequesBody(User_INFO.getPASS()));
                    map.put("unique_id",ToRequesBody(UNIQ.unique_id));
                    map.put("profile",ToRequesBody(editProfileEdt.getText().toString()));
                    map.put("mb_tel",ToRequesBody(""));
                    if (file != null) {
                        RequestBody fileBody = RequestBody.create(MediaType.parse("image/jpeg"), file);
                        map.put("wr_file\"; filename=\"" + file.getPath() + "\"", fileBody);
                        map.put("wr_file", fileBody);
                        Log.e("filename", file.getPath());
                    } else {
                        map.put("wr_file", ToRequesBody("none"));
                    }


                    HttpServiceClient.Retros_ProfileEdit(map).enqueue(new Callback<Default_result>() {
                        @Override
                        public void onResponse(Call<Default_result> call, Response<Default_result> response) {
                            if(response.body().getError().equals("false"))
                            {
                                Toast.makeText(getActivity(), "수정완료!!", Toast.LENGTH_SHORT).show();
                            }
                            else
                            {
                                Toast.makeText(getActivity(), "수정실패", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<Default_result> call, Throwable t) {
                            Log.e("SERVER ERROR",t.getMessage());
                        }
                    });
                    mainProfileTv.setText(editProfileEdt.getText().toString());
                    edit_profile_img.setVisibility(View.GONE);
                    edit_profile_tv.setText("수정하기");
                    editProfileEdt.setVisibility(View.GONE);
                    mainProfileTv.setVisibility(View.VISIBLE);
                    editabled = !editabled;
                } else if (!editabled) {
                    //수정 시도시

                    edit_profile_img.setVisibility(View.VISIBLE);
                    editProfileEdt.setVisibility(View.VISIBLE);
                    mainProfileTv.setVisibility(View.INVISIBLE);
                    editProfileEdt.setText(mainProfileTv.getText().toString());
                    edit_profile_tv.setText("완료하기");
                    editabled = !editabled;

                }
            }
        });
        if (DATA._isLogin()) {
            name.setText(User_INFO.getID());
            mail.setText(User_INFO.getEMAIL());
            mail.setVisibility(View.VISIBLE);
        } else {
            mail.setVisibility(View.INVISIBLE);
        }

        adapter = new CareerListAdapter();

        list.setAdapter(adapter);
        list.setDividerHeight(10);

        ex.collapse();
        main_contents_it_btn.setOnClickListener(this);
        main_project_btn.setOnClickListener(this);
        main_profile_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (DATA._isLogin()) {
                    ex.toggle();
                    if (ex.isExpanded())
                        edit_profile_Btn.setVisibility(View.VISIBLE);
                    else {
                        edit_profile_Btn.setVisibility(View.GONE);
                    }

                } else {
                    startActivity(new Intent(getActivity().getApplicationContext(), Login_Activity.class));
                }
            }
        });

        Banners=new ArrayList<>();
        HttpServiceClient.GetBannerItems().enqueue(new Callback<ContestList_BannerLatest>() {
            @Override
            public void onResponse(Call<ContestList_BannerLatest> call, Response<ContestList_BannerLatest> response) {
                if(response.body().getError().equals("false"))
                {
                    Log.e("Banner pre",response.body().getError()+"");
                    Log.e("Banner pre",response.body().getResult()==null?"null":"not null");
                   if(response.body().getResult()!=null)
                   {
                       ContestList_BannerLatest repo=response.body();
                       Log.e("Banner pre",response.body().getResult().size()+"");
                       for(int i=0;i<response.body().getResult().size();i++) {
                            Log.e("Banner",DATA.ImgRootURL+repo.getResult().get(i).getFile()+i);
                            Collabo_Banner ban=new Collabo_Banner(DATA.ImgRootURL+repo.getResult().get(i).getFile(),repo.getResult().get(i).getIdx());
                            ban.setScaleType(ImageView.ScaleType.FIT_XY);
                            Banners.add(i,ban);
                            banner.addBanner(Banners.get(i));

                       }
                   }
                }
            }

            @Override
            public void onFailure(Call<ContestList_BannerLatest> call, Throwable t) {
                Log.e("Banner error",t.getMessage()+"");
            }
        });
        banner.setOnBannerClickListener(new OnBannerClickListener() {
            @Override
            public void onClick(int position) {
                Intent intent=new Intent(getContext(),ContestInfoViewActivity.class);
                intent.putExtra("idx",Banners.get(position).getIdx());
                startActivity(intent);
            }
        });

        banner.setScaleX(1);
        banner.setScaleY(1);


        SettingProfile();

        unbinder = ButterKnife.bind(this, view);
        return view;

    }

    @Override
    public void onStart() {
        super.onStart();

    }

    private void SettingProfile(){

        HttpServiceClient.Retros_ProfileView().enqueue(new Callback<Profile_View>() {
            @Override
            public void onResponse(Call<Profile_View> call, Response<Profile_View> response) {
                if(response.body().getError().equals("false"))
                {
                    profiledata=response.body().getResult();
                    name.setText(User_INFO.getID());
                    mail.setText(User_INFO.getEMAIL());
                    mainProfileTv.setText(profiledata.getProfile());
                    Glide.with(getActivity()).load(DATA.ImgRootURL+profiledata.getWr_file()).into(pic);


                }
                else{
                    Log.e("response error",response.errorBody().toString());
                    Toast.makeText(getActivity(), "프로필 불러오기 실패", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<Profile_View> call, Throwable t) {

                Log.e("SERVER ERROR",t.getMessage());
            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (DATA._isLogin()) {
            name.setText(User_INFO.getID());
            mail.setText(User_INFO.getEMAIL());
            mail.setVisibility(View.VISIBLE);
        } else {
            mail.setVisibility(View.INVISIBLE);
        }
    }

    void GetNoticeList() {
        HttpServiceClient.Retros_BoardList("board_notice", 0).enqueue(new Callback<Board_ListResults>() {
            public void onResponse(Call<Board_ListResults> call, Response<Board_ListResults> response) {
                Board_ListResults ref = response.body();
                ArrayList<Board_ListResult> res = ref.getResult();
                if (ref.getError().equals("false")) {

                    for (int i = 0; i < res.size(); i++) {
                        Notice_Board_item board_item;
                        String title, date, id;
                        int idxarr;
                        title = res.get(i).getWr_subject();
                        date = res.get(i).getWr_date();
                        idxarr = res.get(i).getIdx();
                        id = res.get(i).getMb_id();
                        board_item = new Notice_Board_item(title, date, idxarr, id);
                        board_items.add(board_item);
                        Log.e("titles", board_items.size() + "");
                    }
                    ArrayList<String> board_titles = new ArrayList<>();
                    Log.e("titles", board_items.size() + "");
                    for (int size = 0; size < board_items.size(); size++) {
                        if (size == 5) break;
                        board_titles.add(board_items.get(size).getTitle());
                        Log.e("titles", board_items.get(size).getTitle());
                        Log.e("titles", board_titles.get(size));
                    }
                    vNotice.start(board_titles);
                    vNotice.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            int position = vNotice.getIndex();
                            Intent inte = new Intent(getActivity().getApplicationContext(), Notice_Seleceted_Activity.class);
                            inte.putExtra("idx", board_items.get(position).getIdx());
                            inte.putExtra("Vid", board_items.get(position).getId());
                            startActivity(inte);
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<Board_ListResults> call, Throwable t) {
                Log.e("BoardList Error", t.getMessage().toString());
            }
        });
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        Intent inte = null;
        switch (id) {
            case main_contest_btn:{
                inte = new Intent(getActivity(), IT_Activity.class);
                inte.putExtra("board_name","board_project");
                break;
            }
            case R.id.main_project_btn: {
                inte = new Intent(getActivity(), IT_Activity.class);
                inte.putExtra("board_name","board_recruit");
                break;
            }
        }
        startActivity(inte);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (file != null) {
                file.delete();
            }
            file = SendPicture(data); //갤러리에서 가져오기

            Log.e("???", file.getPath());
            //pic.setImageURI(data.getData());
            Glide.with(getContext()).load(file).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(pic);
            img_state= changed;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    public int exifOrientationToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
            return 90;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
            return 180;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return 270;
        }
        return 0;
    }
    public File SaveBitmapToFileCache(Bitmap bitmap, String strFilePath,
                                      String filename) {

        File file = new File(strFilePath);

        // If no folders
        if (!file.exists()) {
            file.mkdirs();
        }

        File fileCacheItem = new File(strFilePath + filename);
        if(fileCacheItem.exists())
        {
            fileCacheItem.delete();
        }
        OutputStream out = null;

        try {
            fileCacheItem.createNewFile();
            out = new FileOutputStream(fileCacheItem);

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return fileCacheItem ;
    }
    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().getContentResolver().query(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);

    }
    public File SendPicture(Intent data ) {
        File ref,file=null;
        boolean flag=true;
        ref=new File("/storage/emulated/0/collabo/tester.jpeg");
        if(ref.exists()){
            ref.delete();
        }
        Uri imgUri = data.getData();
        String imagePath = getRealPathFromURI(imgUri); // path 경로
        ExifInterface exif = null;
        try {
            exif = new ExifInterface(imagePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Bitmap bitmap = null;
        Bitmap bit=null;
        try {
            bit = BitmapFactory.decodeFile(imagePath);
            ref=SaveBitmapToFileCache(bit,"/storage/emulated/0/collabo/","tester2.jpeg");
            bit.recycle();
            Log.e("filesize",ref.length()+"bytes");
            if(ref.length()<1048576){
                flag=false;
            }
            ref.delete();
        }catch (OutOfMemoryError e)
        {
            Log.e("OOM",e.getMessage());
            flag=false;

        }
        catch(Exception e)
        {
            Log.e("ERROR",e.getMessage());
        }



        BitmapFactory.Options option = new BitmapFactory.Options                                                                            ();
        option.inSampleSize = 2;

        try {

            int exifOrientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            int exifDegree = exifOrientationToDegrees(exifOrientation);
            if(flag) {
                bitmap = BitmapFactory.decodeFile(imagePath, option);//경로를 통해 비트맵으로 전환
                Log.e("resize","true");
            }
            else{
                Log.e("resize","false");
                bitmap=BitmapFactory.decodeFile(imagePath);
            }
            Matrix matrix = new Matrix();
            matrix.postRotate(exifDegree);
            bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            file = SaveBitmapToFileCache(bitmap, "/storage/emulated/0/collabo/", "tester.jpeg");
            if (file.length() >= 1048576) {
                bitmap = BitmapFactory.decodeFile(file.getPath(), option);
                file = SaveBitmapToFileCache(bitmap, "/storage/emulated/0/collabo/", "tester.jpeg");
            }
            Log.e("File PATH",file.getPath());
        } catch (Exception e) {
            Log.e("IMAGEFILE", e.getMessage());
        }
        finally {
            if(bitmap!=null)
                bitmap.recycle();
        }
        return file;

    }

    public RequestBody ToRequesBody(String ref)
    {
        return RequestBody.create(MediaType.parse("text/plain"), ref);
    }


}

class Collabo_Banner extends RemoteBanner{

    int idx;
    protected Collabo_Banner(Parcel in) {
        super(in);
    }

    public Collabo_Banner(String url,int idx) {
        super(url);
        this.idx=idx;
    }

    public int getIdx() {
        return idx;
    }
}


