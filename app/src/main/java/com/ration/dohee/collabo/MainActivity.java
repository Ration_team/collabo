package com.ration.dohee.collabo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.ration.dohee.collabo.DATA.ActionBarData;
import com.ration.dohee.collabo.Https.HttpServiceClient;
import com.ration.dohee.collabo.Main__Fragments.Alert_Fragment;
import com.ration.dohee.collabo.Main__Fragments.Favorite_Fragment;
import com.ration.dohee.collabo.Main__Fragments.Home_Fragment;
import com.ration.dohee.collabo.Main__Fragments.Myboard_Fragment;
import com.ration.dohee.collabo.Main__Fragments.Mycollabo_Fragment;
import com.ration.dohee.collabo.Util.ActionBar;
import com.ration.dohee.collabo.Util.BackPressedHandler;

import java.util.ArrayList;

import devlight.io.library.ntb.NavigationTabBar;

public class MainActivity extends ActionBar {
    BackPressedHandler b;
    private ViewPager mViewPager;
    private PagerAdapter mPagerAdapter;
    BroadcastReceiver tokenReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String token = intent.getStringExtra("token");
            if(token != null)
            {

                Log.e("Token is Activated",token);
                //send token to your server or what you want to do
            }

        }
    };

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        LocalBroadcastManager.getInstance(this).registerReceiver(tokenReceiver,
                new IntentFilter("tokenReceiver"));
        if(getIntent().getBooleanExtra("logon",false)&&FirebaseInstanceId.getInstance().getToken()!=null) {
            String token;
            FirebaseMessaging.getInstance().subscribeToTopic("news");
            token=FirebaseInstanceId.getInstance().getToken();
            HttpServiceClient.Retros_SetToken(token);
            Log.e("Token is Updated",token
                    +"");

            Log.e("Token", FirebaseInstanceId.getInstance().getToken() + "");


        }




        SetActionBar((View) findViewById(ActionBarData.ActionBar));
        mViewPager = (ViewPager) findViewById(R.id.main_viewpager_contents_vp);
        mPagerAdapter = new PagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mPagerAdapter);
        initUI(mViewPager);
        b = new BackPressedHandler(this, "한번더 뒤로가기 버튼 클릭시 종료됩니다");

    }

    private void initUI(ViewPager viewPager) {
        final String[] colors = getResources().getStringArray(R.array.default_preview);
        final NavigationTabBar navigationTabBar = (NavigationTabBar) findViewById(R.id.main_bottombar_contents_navtapbar);
        final ArrayList<NavigationTabBar.Model> models = new ArrayList<>();
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_favorites),
                        Color.parseColor(colors[0]))
                        .title("Favorite_Fragment")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_board),
                        Color.parseColor(colors[1]))
                        .title("Myboard")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_home),
                        Color.parseColor(colors[2]))
                        .title("Home_Fragment")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_alert),
                        Color.parseColor(colors[3]))
                        .title("Alert_Fragment")
                        .badgeTitle("Alert_Fragment")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_folders),
                        Color.parseColor(colors[4]))
                        .title("folder")
                        .build()
        );

        navigationTabBar.setModels(models);
        if (Collabocreate_Activity.collabo_create_result == false) {
            navigationTabBar.setViewPager(viewPager, 2);
        } else if (Collabocreate_Activity.collabo_create_result == true) {
            navigationTabBar.setViewPager(viewPager, 4);
        }
        Log.d("값은 펄스임", String.valueOf(Collabocreate_Activity.collabo_create_result));
        navigationTabBar.setBgColor(Color.rgb(246, 241, 237));
        navigationTabBar.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(final int position) {
                navigationTabBar.getModels().get(position).hideBadge();
            }

            @Override
            public void onPageScrollStateChanged(final int state) {

            }
        });

        navigationTabBar.postDelayed(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < navigationTabBar.getModels().size(); i++) {
                    final NavigationTabBar.Model model = navigationTabBar.getModels().get(i);
                    navigationTabBar.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            model.showBadge();
                        }
                    }, i * 100);
                }
            }
        }, 500);
    }

    private class PagerAdapter extends FragmentStatePagerAdapter {

        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new Favorite_Fragment();
                case 1:
                    return new Myboard_Fragment();
                case 2:
                    return new Home_Fragment();
                case 3:
                    return new Alert_Fragment();
                case 4:
                    return new Mycollabo_Fragment();
            }
            return null;
        }


        @Override
        public int getCount() {
            return 5;  // 총 5개의 page를 보여줍니다.
        }

    }


    @Override
    public void onBackPressed() {
        b.OnBackPressedClose(0);
    }
}


