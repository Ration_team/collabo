package com.ration.dohee.collabo.MENU.ContestInfoBoard.Util;

/**
 * Created by User on 2017-08-19.
 */

public class ContestInfoItem {
    public String ImgUrl;
    public String Subject;
    public String Category;
    public String StartDate;
    public String ResvDate;
    public int idx;
    public boolean isfavor=false;
    public int favid;

    public ContestInfoItem(String imgUrl,String Subject,String Category,String startDate,String resvDate,int idx){
        this.ImgUrl=imgUrl;
        this.Subject=Subject;
        this.Category=Category;
        this.StartDate=startDate;
        this.ResvDate=resvDate;
        this.idx=idx;
    }
}
