package com.ration.dohee.collabo.MENU;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.ration.dohee.collabo.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import ss.com.bannerslider.banners.DrawableBanner;
import ss.com.bannerslider.views.BannerSlider;

public class Guide_Activity extends AppCompatActivity {


    @BindView(R.id.Guide_Slider)
    BannerSlider GuideSlider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guide_);
        ButterKnife.bind(this);

        GuideSlider.addBanner(new DrawableBanner(R.drawable.guide0));
        GuideSlider.addBanner(new DrawableBanner(R.drawable.guide1));
        GuideSlider.addBanner(new DrawableBanner(R.drawable.guide2));
        GuideSlider.addBanner(new DrawableBanner(R.drawable.guide3));
        GuideSlider.addBanner(new DrawableBanner(R.drawable.guide4));

    }
}
