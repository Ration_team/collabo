package com.ration.dohee.collabo.ListUtil;

/**
 * Created by Samsung on 2017-06-23.
 */

public class Notice_Board_item{
    String Title,Date,id;
    int idx;

    public Notice_Board_item(String title, String date,int idx,String id) {
        Title = title;

        Date = date;

        this.idx=idx;
        this.id=id;
    }



    public String getId() {
        return id;
    }



    public void setIdx(int idx) {
        this.idx = idx;
    }

    public int getIdx() {
        return idx;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }


    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }
}