package com.ration.dohee.collabo.PROJECT_BOARD;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.ration.dohee.collabo.DATA.ActionBarData;
import com.ration.dohee.collabo.R;
import com.ration.dohee.collabo.Util.ActionBar;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WebActivity extends ActionBar {

    @BindView(R.id.Webview)
    WebView webview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);
        ButterKnife.bind(this);
        SetActionBar((View) findViewById(ActionBarData.ActionBar));
        String url = getIntent().getStringExtra("url");
        webview.setWebViewClient(new WebViewClient());
        webview.getSettings().setJavaScriptEnabled(true);
        webview.loadUrl(url);
    }
}
