package com.ration.dohee.collabo.Https.Apply_Results_Box;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Samsung on 2017-09-24.
 */

public class Apply_list {
    @SerializedName("error")
    String error;
    @SerializedName("master")
    ArrayList<Apply_member> master;
    @SerializedName("apply_list")
    ArrayList<Apply_member> members;

    public String getError() {
        return error;
    }

    public ArrayList<Apply_member> getMaster() {
        return master;
    }

    public ArrayList<Apply_member> getMembers() {
        return members;
    }
}
