package com.ration.dohee.collabo.PROJECT_BOARD;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.ration.dohee.collabo.DATA.ActionBarData;
import com.ration.dohee.collabo.DATA.DATA;
import com.ration.dohee.collabo.Https.Board_Recruit_Result;
import com.ration.dohee.collabo.Https.Board_Recruit_Results;
import com.ration.dohee.collabo.Https.Favorite_info;
import com.ration.dohee.collabo.Https.HttpServiceClient;
import com.ration.dohee.collabo.Login_Activity;
import com.ration.dohee.collabo.R;
import com.ration.dohee.collabo.Util.ActionBar;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by User on 2017-06-27.
 */

public class IT_Activity extends ActionBar implements AbsListView.OnScrollListener {
    int idx = 0;
    ImageButton project_board_write_btn;
    IT_CustomAdapterListView adapter;
    ListView it_project_board_list;
    boolean mLockListView = false;
    ArrayList<Favorite_info> favors;
    ArrayList<Board_Recruit_Result> result;
    ArrayList<IT_ListViewItem> resultitems;
    String board_name;
    ArrayList<Integer> checkPage = new ArrayList<>();
    @BindView(R.id.title)
    TextView title;
    AdView mAdView;
    InterstitialAd mInterstitialAd;

    @Override
    protected void onStart() {
        super.onStart();
        SetActionBar((View) findViewById(ActionBarData.ActionBar));

        it_project_board_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent inte = new Intent(getApplicationContext(), project_seleceted.class);
                IT_ListViewItem ref = adapter.items.get(position);
                inte.putExtra("idx", ref.getIdx());
                inte.putExtra("isfavor", ref.isfavor);
                inte.putExtra("board_name", board_name);
                startActivity(inte);
            }
        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        onStart();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_it);
        ButterKnife.bind(this);
        board_name = getIntent().getStringExtra("board_name");
//        admob
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice("01D06A96016428712381997520D4CF5A").build();
        mAdView.loadAd(adRequest);
        mAdView.setAdListener(new AdListener(){
            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                Log.e("Banner Ad Error","Error Code: "+i);
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
                Log.e("Banner Ad Succes","Successsss");
            }
        });

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.admob_video_write));
        mInterstitialAd.loadAd(new AdRequest.Builder().addTestDevice("01D06A96016428712381997520D4CF5A").build());
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
                Log.i("Ads", "onAdFailedToLoad");
                mInterstitialAd.loadAd(new AdRequest.Builder().addTestDevice("01D06A96016428712381997520D4CF5A").build());
                Toaster("광고가 다 떨어졌어요 ! ");
                Intent intent = new Intent(IT_Activity.this, project_write.class);
                intent.putExtra("board_name", board_name);
                startActivity(intent);
            }
            @Override
            public void onAdClosed() {
                // Code to be executed when when the interstitial ad is closed.
                Log.i("Ads", "onAdClosed");
                mInterstitialAd.loadAd(new AdRequest.Builder().addTestDevice("01D06A96016428712381997520D4CF5A").build());
                Intent intent = new Intent(IT_Activity.this, project_write.class);
                intent.putExtra("board_name", board_name);
                startActivity(intent);
            }
        });

        title.setText(board_name.equals("board_recruit")?"프로젝트":"공모전");
        project_board_write_btn = (ImageButton) findViewById(R.id.project_board_write_btn);
        adapter = new IT_CustomAdapterListView(board_name);
        it_project_board_list = (ListView) findViewById(R.id.it_project_board_listview);
        it_project_board_list.setOnScrollListener(this);
        resultitems = new ArrayList<>();
        it_project_board_list.setAdapter(adapter);
        it_project_board_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), project_seleceted.class);
                IT_ListViewItem ref = adapter.items.get(position);
                intent.putExtra("idx", ref.getIdx());
                intent.putExtra("host",false);
                startActivity(intent);
            }
        });
        if (!DATA._isLogin()) {
            SweetAlertDialog dlg = new SweetAlertDialog(IT_Activity.this, SweetAlertDialog.WARNING_TYPE);
            dlg.setCancelText("취소");
            dlg.setTitleText("로그인");
            dlg.setContentText("로그인이 필요합니다");
            dlg.setConfirmText("로그인");
            dlg.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    sweetAlertDialog.dismiss();
                    finish();
                }
            });
            dlg.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    Intent inte = new Intent(getApplicationContext(), Login_Activity.class);
                    startActivity(inte);
                    finish();
                }
            });
            dlg.show();
        }


        project_board_write_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (DATA._isLogin()) {


                    SweetAlertDialog dlg = new SweetAlertDialog(IT_Activity.this, SweetAlertDialog.WARNING_TYPE);
                    dlg.setCancelText("돌아가기");
                    dlg.setConfirmText("시청하기");
                    dlg.setTitleText("글 작성");
                    dlg.setContentText("무분별한 게시글방지를 위해 광고 시청후 글작성이 가능합니다");
                    dlg.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismiss();
                            mInterstitialAd.show();
                        }
                    });
                    dlg.show();




                } else if (!DATA._isLogin()) {
                    SweetAlertDialog dlg = new SweetAlertDialog(IT_Activity.this, SweetAlertDialog.WARNING_TYPE);
                    dlg.setCancelText("돌아가기");
                    dlg.setConfirmText("로그인");
                    dlg.setTitleText("로그인이 필요합니다");
                    dlg.setContentText("");
                    dlg.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            startActivity(new Intent(getApplicationContext(), Login_Activity.class));
                            sweetAlertDialog.dismiss();
                        }
                    });
                    dlg.show();
                }
            }
        });
        mLockListView = false;
        idx = 0;
        adapter.items.clear();
        resultitems.clear();
        adapter.notifyDataSetChanged();
        Retro();
        checkPage.add(idx);
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        // 현재 가장 처음에 보이는 셀번호와 보여지는 셀번호를 더한값이
        // 전체의 숫자와 동일해지면 가장 아래로 스크롤 되었다고 가정합니다.
        int count = totalItemCount - visibleItemCount;
        if (firstVisibleItem >= count && totalItemCount != 0 && mLockListView == false) {
            idx++;

            Retro();
        }
    }


    public void Retro() {
        HttpServiceClient.Retros_RecruitList(board_name, idx).enqueue(new Callback<Board_Recruit_Results>() {
            @Override
            public void onResponse(Call<Board_Recruit_Results> call, Response<Board_Recruit_Results> response) {
                if (response.isSuccessful()) {
                    Board_Recruit_Results refs = response.body();
                    if (refs.getError().equals("false")) {

                        result = refs.getResult();


                        resultitems.clear();
                        for (int i = 0; i < result.size(); i++) {
                            Board_Recruit_Result res = result.get(i);
                            Log.e("wr_fiels",res.getWr_file());
                            resultitems.add(new IT_ListViewItem(res.getWr_file(), res.getWr_subject(),
                                    res.getMb_id(), res.getWr_date(), res.getNum(), res.getIdx()));
                        }

                        adapter.items.addAll(resultitems);
                        adapter.notifyDataSetChanged();
                    } else {
                        mLockListView = true;
                    }
                } else {
                    Log.e("Server Error", response.errorBody().toString());
                    Toaster(response.errorBody().toString());
                    mLockListView = true;
                }
            }

            @Override
            public void onFailure(Call<Board_Recruit_Results> call, Throwable t) {
                Log.e("Retros Error", t.getMessage());
                mLockListView = true;
            }
        });
    }
}
