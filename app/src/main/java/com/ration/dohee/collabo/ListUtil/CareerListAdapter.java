package com.ration.dohee.collabo.ListUtil;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ration.dohee.collabo.R;

import java.util.ArrayList;

/**
 * Created by Samsung on 2017-07-26.
 */

public class CareerListAdapter extends BaseAdapter {
    ArrayList <CareerItem> items=new ArrayList<>();
    public void add(CareerItem e){
        items.add(e);
    }
    @Override
    public int getCount() {
       return items.size();
    }

    @Override
    public CareerItem getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        TextView title,date;
        Context context = parent.getContext();
        CareerItem ref=items.get(position);
        if(convertView==null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.careerlist, parent, false);
        }

        title=((TextView)convertView.findViewById(R.id.career_list_item_title));
        date=((TextView)convertView.findViewById(R.id.career_list_item_date));


        title.setText(ref.getTitle());
        date.setText(ref.getDate());

        return convertView;
    }
}
