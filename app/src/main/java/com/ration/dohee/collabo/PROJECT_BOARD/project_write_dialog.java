package com.ration.dohee.collabo.PROJECT_BOARD;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.ration.dohee.collabo.R;
import com.ration.dohee.collabo.Util.CustomActivityForFonts;
import com.ration.dohee.collabo.Util.EditTextWatcher;

public class project_write_dialog extends CustomActivityForFonts {

    String link="none";
    Button cencelBtn,checkBtn;
    EditText linkurl_edt;
    int ResultOK=15;
    String https;
    boolean Flag=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_project_write_dialog);
        linkurl_edt=(EditText)findViewById(R.id.project_dialog_link_edt);
        linkurl_edt.addTextChangedListener(new EditTextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()>0)
                    Flag=true;
                else
                    Flag=false;
            }
        });
        checkBtn=(Button)findViewById(R.id.checkBtn);
        cencelBtn = (Button)findViewById(R.id.cencelBtn);

        cencelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPause();
            }
        });
        checkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Flag) {
                    link = linkurl_edt.getText().toString();
                    if(link.matches("http://.*")){
                        https="";
                    }
                    else{
                        https="http://";
                    }
                    onPause();
                }
                else{
                    Toaster("링크 URL 을 입력해주세요.");
                }
            }
        });
    }
    public void onPause() {
        super.onPause();
        Intent inte=new Intent();
        inte.putExtra("linkurl",https+link);
        setResult(ResultOK,inte);
        finish();
    }
}
