package com.ration.dohee.collabo.Util;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.ration.dohee.collabo.R;

import java.util.ArrayList;

/**
 * Created by User on 2017-06-25.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private ArrayList<myItem> mItems;
    private boolean mCheckBoxState = false;

    public RecyclerViewAdapter(ArrayList<myItem> mItems,boolean state) {
        this.mItems=mItems;
        this.mCheckBoxState=state;
    }
    @Override
    public RecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        // view's size , margins, paddings and layout parameters setting 가능

        //create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_recyclerviewadapter, parent, false);

        //create viewholder
        ViewHolder viewHolder=new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.mycareer_title_tv.setText(mItems.get(position).title);
        holder.mycareer_date_tv.setText(mItems.get(position).date);

        if(mCheckBoxState){
            holder.mycareer_chkselected.setVisibility(View.VISIBLE);
            holder.mycareer_chkselected.setChecked(mItems.get(position).isSelected);
            holder.mycareer_chkselected.setTag(mItems.get(position));
            holder.mycareer_chkselected.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CheckBox cb = (CheckBox) v;
                    myItem item = (myItem) cb.getTag();

                    item.isSelected=cb.isChecked();
                    mItems.get(position).isSelected=cb.isChecked();

                    Toast.makeText(
                            v.getContext(),
                            "Clicked on Checkbox: " + cb.getText() + " is "
                                    + cb.isChecked(), Toast.LENGTH_LONG).show();
                }
            });
        }
        else{
            holder.mycareer_chkselected.setVisibility(View.GONE);
        }
    }


    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public TextView mycareer_title_tv;
        public TextView mycareer_date_tv;
        public CheckBox mycareer_chkselected;

        public ViewHolder(View itemView) {
            super(itemView);
            mycareer_title_tv=(TextView)itemView.findViewById(R.id.mycareer_title_tv);
            mycareer_date_tv=(TextView)itemView.findViewById(R.id.mycareer_date_tv);
            mycareer_chkselected=(CheckBox)itemView.findViewById(R.id.mycareer_chkselected);
        }
    }
//    public void setCheckBoxState(boolean state){
//        mCheckBoxState = state;
//        notifyDataSetChanged();
//    }
    public ArrayList<myItem> getmItems(){
        return mItems;
    }
}

