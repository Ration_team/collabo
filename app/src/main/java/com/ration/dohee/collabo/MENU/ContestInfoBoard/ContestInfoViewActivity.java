package com.ration.dohee.collabo.MENU.ContestInfoBoard;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.ration.dohee.collabo.DATA.ActionBarData;
import com.ration.dohee.collabo.DATA.DATA;
import com.ration.dohee.collabo.DATA.User_INFO;
import com.ration.dohee.collabo.Https.Favor_delresult;
import com.ration.dohee.collabo.Https.Favor_result;
import com.ration.dohee.collabo.Https.Favorite_info;
import com.ration.dohee.collabo.Https.HttpContestInfo.ContestInfoView;
import com.ration.dohee.collabo.Https.HttpContestInfo.ContestInfoViewResult;
import com.ration.dohee.collabo.Https.HttpServiceClient;
import com.ration.dohee.collabo.PROJECT_BOARD.WebActivity;
import com.ration.dohee.collabo.R;
import com.ration.dohee.collabo.Util.ActionBar;
import com.sackcentury.shinebuttonlib.ShineButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by User on 2017-09-10.
 */

public class ContestInfoViewActivity extends ActionBar{
    int idx;
    int Favposition = 0;
    ContestInfoView data = null;
    @BindView(R.id.drawer_indicator)
    ImageView drawerIndicator;
    @BindView(R.id.imageButton)
    ImageButton imageButton;
    @BindView(R.id.searchBtn)
    ImageButton searchBtn;
    @BindView(R.id.contest_info_view_subject)
    TextView Subject;
    @BindView(R.id.contest_view_favorite_Btn)
    ShineButton ContestViewFavoriteBtn;
    @BindView(R.id.contest_info_view_topic)
    TextView Topic;
    @BindView(R.id.contest_info_view_category)
    TextView Category;
    @BindView(R.id.contest_info_view_host)
    TextView Host;
    @BindView(R.id.contest_info_view_inquiry)
    TextView Inquiry;
    @BindView(R.id.contest_info_view_supervision)
    TextView Supervision;
    @BindView(R.id.contest_info_view_link)
    TextView Link;
    @BindView(R.id.contest_info_view_prize)
    TextView Prize;
    @BindView(R.id.contest_info_view_qualification)
    TextView Qualification;
    @BindView(R.id.contest_info_view_reference)
    TextView Reference;
    @BindView(R.id.contest_info_view_start_date)
    TextView StartDate;
    @BindView(R.id.contest_info_view_resv_date)
    TextView ResvDate;
    @BindView(R.id.contest_info_view_resv_method)
    TextView ResvMethod;
    @BindView(R.id.contest_info_view_img)
    ImageView img;
    @BindView(R.id.union_navmenu_drawerlayout_menu)
    NavigationView unionNavmenuDrawerlayoutMenu;
    @BindView(R.id.union_navmenu_drawerlayout)
    DrawerLayout unionNavmenuDrawerlayout;

    private void Retros(){
        HttpServiceClient.retrosViewResult("contest_list",idx)
                .enqueue(new Callback<ContestInfoViewResult>() {
                    @Override
                    public void onResponse(Call<ContestInfoViewResult> call, Response<ContestInfoViewResult> response) {
                        if(response.isSuccessful()){
                            if(response.body().getError().equals("false")){
                                data=response.body().getResult();
                            }
                            if(data!=null){
                                ContestViewFavoriteBtn.setChecked(getIntent().getBooleanExtra("isfavor",false));
                                Subject.setText(data.getWr_subject());
                                Topic.setText(data.getTopic());
                                Category.setText(data.getCategory());
                                Host.setText(data.getHost());
                                Supervision.setText(data.getSupervision());
                                Inquiry.setText(data.getInquiry());
                                Link.setText(data.getLink());
                                Link.setTextColor(Color.BLUE);
                                Qualification.setText(data.getQualification());
                                Prize.setText(data.getPrize());
                                StartDate.setText(data.getStart_date().substring(2,10)+" ~ ");
                                ResvDate.setText(data.getResv_date().substring(2,10));
                                ResvMethod.setText(data.getResv_method());
                                Reference.setText(data.getReference());
                                Glide.with(getApplicationContext())
                                        .load(DATA.ImgRootURL+"/"+data.getWr_file()).into(img);
                            }
                            else if(data==null){
                                Toaster("게시글이 존재하지 않습니다.");
                            }
                            else{
                            }
                        }else{
                            Log.e("ERROR",response.errorBody().toString());
                        }
                    }

                    @Override
                    public void onFailure(Call<ContestInfoViewResult> call, Throwable t) {
                        Log.e("SERVER ERROR", t.getMessage());
                    }
                });
    }
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contest_info_view);
        SetActionBar(findViewById(ActionBarData.ActionBar));
        ButterKnife.bind(this);
        idx=getIntent().getIntExtra("idx",-1);
        Retros();

        Link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ContestInfoViewActivity.this, WebActivity.class)
                .putExtra("url",Link.getText().toString()));
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        SetActionBar(findViewById(ActionBarData.ActionBar));
    }
    @OnClick(R.id.contest_view_favorite_Btn)
    public void onViewClicked(){
        if(ContestViewFavoriteBtn.isChecked()){
            HttpServiceClient.ContestInfo_Retros_FavorAdd(idx,data.getWr_subject())
                    .enqueue(new Callback<Favor_result>() {
                        @Override
                        public void onResponse(Call<Favor_result> call, Response<Favor_result> response) {
                            if(response.isSuccessful()){
                                Favorite_info ref=response.body().getResult().get(0);
                                User_INFO.getConInfoFavor().add(new Favorite_info(ref.getId(),ref.getIdx()
                                        ,ref.getBoard_name(),ref.getWr_subject()));
                            }
                            else{
                                Log.e("ERROR",response.errorBody().toString());
                            }
                        }

                        @Override
                        public void onFailure(Call<Favor_result> call, Throwable t) {
                            Log.e("SERVER ERROR", t.getMessage());
                        }
                    });
        }
        else if(!ContestViewFavoriteBtn.isChecked()){
            for(int i=0;i<User_INFO.getConInfoFavor().size();i++){
                if(idx==User_INFO.getConInfoFavor().get(i).getIdx()){
                    Favposition=i;


                    break;
                }
            }
            HttpServiceClient.ContestInfo_Retros_FavorDel(Favposition).enqueue(new Callback<Favor_delresult>() {
                @Override
                public void onResponse(Call<Favor_delresult> call, Response<Favor_delresult> response) {
                    if(response.isSuccessful()){

                    }
                    else{
                        Log.e("ERROR",response.errorBody().toString());
                    }
                }

                @Override
                public void onFailure(Call<Favor_delresult> call, Throwable t) {
                    Log.e("SERVER ERROR", t.getMessage());
                }
            });
        }
    }
}