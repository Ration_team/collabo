package com.ration.dohee.collabo.Https;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ration.dohee.collabo.DATA.UNIQ;
import com.ration.dohee.collabo.DATA.User_INFO;
import com.ration.dohee.collabo.Https.Apply_Results_Box.Apply_create;
import com.ration.dohee.collabo.Https.Apply_Results_Box.Apply_list;
import com.ration.dohee.collabo.Https.HttpContestInfo.ContestInfoViewResult;
import com.ration.dohee.collabo.Https.HttpContestInfo.ContestInfotListResult;
import com.ration.dohee.collabo.Https.HttpContestInfo.Default_result;

import java.util.HashMap;
import java.util.Map;

import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.ration.dohee.collabo.DATA.User_INFO.initFavorList;

/**
 * Created by Samsung on 2017-07-07.
 */

public class HttpServiceClient {
    private static final String ROOT_URL = "http://collabo.boasmedia.co.kr/";
    static PostSet service;
    private static int FavorMaxPage=0;
    public HttpServiceClient() {

    }

    private static Retrofit getRetroClient() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
//        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
//        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//        httpClient.interceptors().add(interceptor);
        Gson gson=new GsonBuilder().setLenient().create();
        return new Retrofit.Builder()
                .baseUrl(ROOT_URL).client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    public static PostSet getApiService() {
        return getRetroClient().create(PostSet.class);
    }

    private static PostSet getservice(){
        if(service==null)
        {
            service=getApiService();
        }
        return service;
    }
    public static Call<Results> Retros_SIGN(String ID,String PASS) {
        HashMap<String, String> map = new HashMap<>();
        map.put("mb_id", ID);
        map.put("mb_password", PASS);
        Call<Results> repos = getservice().Sign_in(map);
        return repos;
    }

    public static Call<Logout_Results> Retros_LOGOUT(){
        HashMap<String, String> map=new HashMap<>();
        map.put("unique_id",UNIQ.unique_id);
        map.put("mb_id", User_INFO.getID());
        Call<Logout_Results> repos=getservice().Logout(map);
        return repos;
    }

    public static Call<Results> Retros_JOIN(String ID,String PASS,String Email,String Phone) {

        HashMap<String, String> map = new HashMap<>();
        map.put("mb_id", ID);
        map.put("mb_password", PASS);
        map.put("mb_email", Email);
        map.put("mb_tel", Phone);
        Call<Results> repos = getservice().Join_in(map);
        return repos;
    }

    public static Call<Recruit_Apply> Retros_RecruitApply(Map <String ,String > param)
    {

        Call<Recruit_Apply> repos= getservice().Recruit_Apply(param);
        return repos;

    }

    public static Call<Apply_list> Retros_RecruitApplyList(Map <String ,String > param)
    {

        Call<Apply_list> repos= getservice().Recruit_ApplyList(param);
        return repos;

    }

    public static Call<Board_Results> Retros_BoardView(String board_name,int idx) {
        Call<Board_Results> repos = getservice().Board_View(board_name,idx);
        return repos;
    }

    public static Call<Board_ListResults> Retros_BoardList(String board_name,int cur) {
        Call<Board_ListResults> repos;
        repos = getservice().Board_Notice_List(board_name,cur);
        return repos;
    }

    public static Call<Board_Recruit_Results> Retros_RecruitList(String board_name,int cur) {
        Call<Board_Recruit_Results> repos;
        repos = getservice().Board_Reqruit_List(board_name,cur,UNIQ.unique_id,User_INFO.getID());
        return repos;
    }

    public static Call<Board_Statment> Retros_BoardDelete(Map<String ,String> map) {
        Call<Board_Statment> repos = getservice().Board_Delete(map);
        return repos;
    }

    public static Call<IdCheckResult> Retros_IDCheck(String val) {
        Call<IdCheckResult> repos = getservice().Join_IDCHECK(val);
        return repos;
    }

    public static Call<Board_Commits> Retros_BoardCommit(Map<String, RequestBody> map) {
        Call<Board_Commits> repos = getservice().Board_Upload(map);
        return repos;
    }

    public static Call<Board_Commits> Retros_BoardEdit(Map<String, RequestBody> map) {
        Call<Board_Commits> repos = getservice().Board_Edit(map);
        return repos;
    }

    public static Call<Recruit_boards> Retros_ReqruitWrite(Map <String, RequestBody> map)
    {
        Call<Recruit_boards> repos = getservice().Recruit_write(map);
        return repos;
    }

    public static Call<Recruit_boards> Retros_RecruitView(String board_name,int idx){
        Call<Recruit_boards> repos=getservice().Recruit_View(board_name, idx);
        return repos;
    }

    public static Call<Favor_delresult> Retros_FavorDel(int position,String name)
    {
        Call<Favor_delresult> repos= HttpServiceClient.getApiService().Favorite_Delete(
                User_INFO.getfavors(name).get(position).getId(), UNIQ.unique_id, User_INFO.getID(),0);
        User_INFO.getfavors(name).remove(position);
        return repos;

    }

    public static Call<Favor_result> Retros_FavorAdd(int idx,String title,String name){
        Call<Favor_result> repos= HttpServiceClient.getApiService().Favorite_Add(
                name,idx,UNIQ.unique_id,User_INFO.getID(),title
        );
        return repos;
    }

    public static Call<Favor_delresult> ContestInfo_Retros_FavorDel(int position){
        Call<Favor_delresult> repos= HttpServiceClient.getApiService().Favorite_Delete(
                User_INFO.getConInfoFavor().get(position).getId(), UNIQ.unique_id, User_INFO.getID(),0);
        User_INFO.getConInfoFavor().remove(position);
        return repos;
    }
    public static Call<Favor_result> ContestInfo_Retros_FavorAdd(int idx,String title){
        Call<Favor_result> repos=HttpServiceClient.getApiService().Favorite_Add(
                "contest_list",idx,UNIQ.unique_id,User_INFO.getID(),title
        );
        return repos;
    }
    public static void Retros_FavoriteList()
    {
        HashMap<String,String> param=new HashMap<>();
        int cur=0;
         do {
            param.put("mb_id", User_INFO.getID());
            param.put("unique_id", UNIQ.unique_id);
            param.put("cur",cur+"");
            User_INFO.getRecruitFavor().clear();
            Call<Favor_result> repos=getApiService().Favorite_list(param);
            repos.enqueue(new Callback<Favor_result>() {
                @Override
                public void onResponse(Call<Favor_result> call, Response<Favor_result> response) {
                    if(response.isSuccessful())
                    {
                        Favor_result ref=response.body();
                        if(ref.getError().equals("false"))
                        {

                            initFavorList();
                            FavorMaxPage=ref.getPage_count();
                            for(int i=0;i<ref.getResult().size();i++) {
                                if(ref.getResult().get(i).getBoard_name().equals("board_recruit"))
                                    User_INFO.getRecruitFavor().add(ref.getResult().get(i));
                                if(ref.getResult().get(i).getBoard_name().equals("contest_list")) {
                                    User_INFO.getConInfoFavor().add(ref.getResult().get(i));
                                    Log.e("favor?",ref.getResult().get(i).getIdx()+"");
                                }
                                if(ref.getResult().get(i).getBoard_name().equals("board_project"))
                                    User_INFO.getContestFavor().add(ref.getResult().get(i));
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<Favor_result> call, Throwable t) {
                    Log.e("SERVER ERROR",t.getMessage());
                    return;
                }
            });
            cur++;
        }while (cur<FavorMaxPage);

    }

    public static Call<ContestInfotListResult> retrosListResult(String board_name, int cur){
        Call<ContestInfotListResult> repos;
        repos=getservice().getContestInfoListResult(board_name,cur);
        return repos;
    }

    public static Call<Apply_create> Retros_Teamcreate(Map<String ,String> map)
    {
        Call<Apply_create> repos=getservice().Create_Team(map);
        return repos;
    }
    public static Call<ContestInfoViewResult> retrosViewResult(String board_name, int idx){
        Call<ContestInfoViewResult> respos;
        respos=getservice().getContestInfoViewResult(board_name,idx);
        return respos;
    }

    public static Call<Board_Recruit_Results> Retros_MyBoardList() {
        Call<Board_Recruit_Results> repos;
        repos = getservice().Myboard_List(UNIQ.unique_id,User_INFO.getID());
        return repos;
    }

    public static Call<Default_result> Retros_MyboardDelete(Map <String,String> map){
        Call<Default_result> repos;
        map.put("mb_id",User_INFO.getID());
        map.put("unique_id",UNIQ.unique_id);
        repos=getservice().Myboard_Delete(map);
        return repos;
    }

    public static Call<Recruit_boards> Retros_Edit(Map<String,RequestBody> param,boolean is_delete)
    {
        Call<Recruit_boards> repos=getservice().Recruit_Edit(param,is_delete);
        return repos;

    }

    public static Call<Profile_View> Retros_ProfileView(){
        HashMap<String ,String> map=new HashMap<>();
        map.put("mb_id",User_INFO.getID());
        map.put("mb_password",User_INFO.getPASS());
        map.put("unique_id",UNIQ.unique_id);


        Call<Profile_View> repos=getservice().Profile_View(map);
        return repos;

    }

    public static Call<Default_result> Retros_ProfileEdit(Map<String ,RequestBody> param){
        Call<Default_result> repos = getservice().Profile_Edit(param);
        return repos;

    }

    public static void Retros_SetToken(final String Token)
    {
        HashMap<String,String> param=new HashMap<>();

        param.put("mb_id",User_INFO.getID());
        param.put("unique_id",UNIQ.unique_id);
        param.put("token",Token);


        Call<Default_result> repos=getservice().SetToken(param);
        repos.enqueue(new Callback<Default_result>() {
            @Override
            public void onResponse(Call<Default_result> call, Response<Default_result> response) {
                if(response.body().getError().equals("false")&&response.isSuccessful())
                {
                    Log.e("TOKEN",Token);
                }
                else{
                    Log.e("Retros Error",response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<Default_result> call, Throwable t) {
                Log.e("Server Error",t.getMessage());
            }
        });
    }

    public static Call<Search_Results> SearchResult(String type,String key)
    {
        Call<Search_Results> repos=getservice().GetSearchResult(type,key);
        return repos;
    }

    public static Call<Default_result> Create_Collabo(Map<String,String> map)
    {
        map.put("unique_id",UNIQ.unique_id);
        map.put("mb_id",User_INFO.getID());
        Call<Default_result> repos=getservice().Create_Collabo(map);
        return repos;
    }

    public static Call<ContestList_BannerLatest> GetBannerItems(){
        Call<ContestList_BannerLatest> repos=getApiService().BannerItems("true");
        return repos;
    }

    public static Call<TeamListitems> getTeamList(){
        Call<TeamListitems> repo=getApiService().getTeamList(User_INFO.getID(),UNIQ.unique_id);
        return repo;
    }
}