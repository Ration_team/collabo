package com.ration.dohee.collabo.DATA;

import com.ration.dohee.collabo.R;

/**
 * Created by Samsung on 2017-06-22.
 */

public class ActionBarData {
    public static final Integer DrawerLayout= R.id.union_navmenu_drawerlayout;
    public static final Integer DrawerLayout_Navmenu= R.id.union_navmenu_drawerlayout_menu;
    public static final Integer ActionBar =R.id.actionbar;

}
