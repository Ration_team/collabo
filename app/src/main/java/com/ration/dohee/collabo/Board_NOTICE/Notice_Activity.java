package com.ration.dohee.collabo.Board_NOTICE;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;

import com.ration.dohee.collabo.DATA.ActionBarData;
import com.ration.dohee.collabo.Https.Board_ListResult;
import com.ration.dohee.collabo.Https.Board_ListResults;
import com.ration.dohee.collabo.Https.HttpServiceClient;
import com.ration.dohee.collabo.ListUtil.List_Adapter_BoardNotice;
import com.ration.dohee.collabo.ListUtil.Notice_Board_item;
import com.ration.dohee.collabo.R;
import com.ration.dohee.collabo.Util.ActionBar;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Notice_Activity extends ActionBar implements AbsListView.OnScrollListener{

    String Board_list_INFO;
    ArrayList<Notice_Board_item> board_items;
    Notice_Board_item board_item;
    ListView notice_list;
    List_Adapter_BoardNotice notice_list_adapter;
    boolean mLockListView=false;
    public int idx=0;
    private List<String> TextList;
    ArrayList<Integer> lists_limits;

    @Override
    protected void onRestart() {
        super.onRestart();

    }

    @Override
    protected void onStart() {
        super.onStart();
        mLockListView=false;
        notice_list_adapter.arr.clear();
        board_items.clear();
        lists_limits.clear();
        notice_list_adapter.notifyDataSetChanged();
        notice_list.setOnScrollListener(this);
        idx=0;
        Retro();




    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        // 현재 가장 처음에 보이는 셀번호와 보여지는 셀번호를 더한값이
        // 전체의 숫자와 동일해지면 가장 아래로 스크롤 되었다고 가정합니다.
        int count = totalItemCount - visibleItemCount;
        if(firstVisibleItem >= count && totalItemCount != 0 && mLockListView == false)
        {
            Retro();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice_);
        lists_limits=new ArrayList<>();

        TextList= Arrays.asList("img","None");
        board_items=new ArrayList<>();
        notice_list=(ListView)findViewById(R.id.notice_contents_list);
        notice_list_adapter=new List_Adapter_BoardNotice();
        notice_list.setAdapter(notice_list_adapter);
        notice_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent inte=new Intent(getApplicationContext(),Notice_Seleceted_Activity.class);
                inte.putExtra("idx",notice_list_adapter.arr.get(position).getIdx());
                inte.putExtra("Vid",notice_list_adapter.arr.get(position).getId());
                startActivity(inte);


            }
        });
        SetActionBar(((View)findViewById(ActionBarData.ActionBar)));

    }

    private void Retro() {

        for (int i=0;i<lists_limits.size();i++)
        {
            if(lists_limits.get(i)==idx)
                return;
        }
        lists_limits.add(idx);
        HttpServiceClient.Retros_BoardList("board_notice", idx++).enqueue(new Callback<Board_ListResults>() {
            @Override
            public void onResponse(Call<Board_ListResults> call, Response<Board_ListResults> response) {
                Board_ListResults ref = response.body();
                ArrayList<Board_ListResult> res = ref.getResult();
                if (ref.getError().equals("false")) {
                    try{
                        board_items.clear();
                    if (res.get(0).getError().equals("true")) {
                        mLockListView=true;
                        return;
                    }
                    }
                    catch (NullPointerException e){
                    for(int i=0;i<res.size();i++)
                    {
                        String title, name, date, file,id;
                        int idxarr;
                        file="null";
                        title = res.get(i).getWr_subject();
                        date = res.get(i).getWr_date();
                        idxarr = res.get(i).getIdx();
                        id=res.get(i).getMb_id();
//                    조회수는   ;res.getHit_count();
                        board_item = new Notice_Board_item(title, date, idxarr,id);
                        board_items.add(board_item);
                    }
                    }

                    SetInit_ListView();
                }
            }

            @Override
            public void onFailure(Call<Board_ListResults> call, Throwable t) {
                Log.e("BoardList Error",t.getMessage().toString());
                mLockListView=true;

            }
        });
    }
    void SetInit_ListView(){
        notice_list_adapter.arr.addAll(board_items);
        notice_list_adapter.notifyDataSetChanged();
        mLockListView=false;

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        finish();

    }

}

