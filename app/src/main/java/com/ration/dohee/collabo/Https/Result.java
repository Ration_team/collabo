package com.ration.dohee.collabo.Https;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samsung on 2017-07-10.
 */

public class Result {

    @SerializedName("mb_id")
    String mb_id;

    @SerializedName("mb_email")
    String mb_email;
    @SerializedName("mb_tel")
    String mb_tel;
    @SerializedName("mb_auth")
    String mb_auth;
    @SerializedName("unique_id")
    String unique_id;


    public String getMb_id() {
        return mb_id;
    }



    public String getMb_email() {
        return mb_email;
    }

    public String getMb_auth() {
        return mb_auth;
    }

    public String getUnique_id() {
        return unique_id;
    }
}
