package com.ration.dohee.collabo.Https.Apply_Results_Box;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samsung on 2017-10-01.
 */

public class Apply_create {
    @SerializedName("error")
    String error;
    @SerializedName("result")
    String result;

    public String getError() {
        return error;
    }

    public String getResult() {
        return result;
    }
}
