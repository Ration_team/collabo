package com.ration.dohee.collabo;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.bumptech.glide.Glide;
import com.ration.dohee.collabo.Https.HttpServiceClient;
import com.ration.dohee.collabo.Https.IdCheckResult;
import com.ration.dohee.collabo.Https.Results;
import com.ration.dohee.collabo.Util.BackPressedHandler;
import com.ration.dohee.collabo.Util.CustomActivityForFonts;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Join_Activity extends CustomActivityForFonts {
    com.rengwuxian.materialedittext.MaterialEditText IdEditText,NameEditText,PasswordEditText,PassworConfirmEdt,PhoneEditText,EmailEdtiText;
    String ID,Name,Email,Pass,Phone;
    TextView IdConfirmTv;
    Animation slide_in,slide_out,prev_slide_in,prev_slide_out;
    Button EmailSendBtn,ConfirmBtn;
    LinearLayout IDCheckBtn;
    ImageButton prev,next;
    ImageView j1,j3,j4,j5,j6;
    ViewFlipper vf;
    boolean Flags[]=new boolean[4];
    View IDView,NameView,PasswordView,PhoneView,EmailView,ConfirmView;
    BackPressedHandler pressed=new BackPressedHandler(this,"한번더 뒤로 클릭시 이전화면으로 넘어갑니다.");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join_manager);
        slide_in = AnimationUtils.loadAnimation(this, R.anim.slide_in_right);
        slide_out = AnimationUtils.loadAnimation(this, R.anim.slide_out_left);
        prev_slide_in=AnimationUtils.loadAnimation(this,R.anim.slide_in_right_prev);
        prev_slide_out=AnimationUtils.loadAnimation(this,R.anim.slide_out_left_prev);
        vf = ((ViewFlipper) findViewById(R.id.joinmanager_viewflipper));

        IDView=(View)findViewById(R.id.join_view1);
        PasswordView=(View)findViewById(R.id.join_view3);
        PhoneView=(View)findViewById(R.id.join_view4);
        EmailView=(View)findViewById(R.id.join_view5);
        ConfirmView=(View)findViewById(R.id.join_view6);

        initViewFlipper();
        initViews();
    }

    public void initViewFlipper(){
        prev=(ImageButton) findViewById(R.id.joinmanager_viewflipper_prev_imgbtn);
        prev.setVisibility(View.INVISIBLE);
        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vf.setOutAnimation(prev_slide_out);
                vf.setInAnimation(prev_slide_in);
                vf.showPrevious();
                if(vf.getDisplayedChild()==0){
                    prev.setVisibility(View.INVISIBLE);
                }
                if(vf.getDisplayedChild()==2){
                    next.setVisibility(View.VISIBLE);
                }
            }
        });
        next=(ImageButton) findViewById(R.id.joinmanager_viewflipper_next_imgbtn);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vf.setOutAnimation(slide_out);
                vf.setInAnimation(slide_in);
                vf.showNext();
                if(vf.getDisplayedChild()==1){
                    prev.setVisibility(View.VISIBLE);
                }
                if(vf.getDisplayedChild()==3){
                    next.setVisibility(View.INVISIBLE);
                }
            }
        });
    }
    void SetColor(MaterialEditText edt,Integer Color){
        edt.setPrimaryColor(Color);
        edt.setErrorColor(Color);
    }
    // 뷰 컨트롤러 함수 들 각뷰마다 oncreate  처럼 사용하면되고
    // this.oncreate 에서 밑함수들 호출 요망
    public void initViews(){
        // 각 필드 내용 체크 Flags 초기화  ALL FALSE
        for (int i=0;i<Flags.length;i++)
        {
            Flags[i]=false;
        }
        IDViewController(IDView);
//        NameViewController(NameView);
        PasswordViewController(PasswordView);
        PhoneViewController(PhoneView);
        EmailViewController(EmailView);
        ConfirmViewController(ConfirmView);
    }
    void IDViewController(View v){

        // InputFilter   영어&숫자만 입력되도록 설정
        InputFilter filterAlphaNum = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                Pattern ps = Pattern.compile("^[a-zA-Z0-9]*$");
                if (!ps.matcher(source).matches()) {
                    return "";
                }
                return null;
            }
        };
        j1=(ImageView)v.findViewById(R.id.join1_title_img);
        Glide.with(v.getContext()).load(R.drawable.join1).into(j1);
        IdEditText=(MaterialEditText)v.findViewById(R.id.join_id_input_edt);
        IdConfirmTv=(TextView)v.findViewById(R.id.join_id_cross_tv);
        IdEditText.setFilters(new InputFilter[]{
                new InputFilter.LengthFilter(12),
                filterAlphaNum});
        IdEditText.setMaxCharacters(12);
        IdEditText.setMinCharacters(6);
        IDCheckBtn=(LinearLayout)v.findViewById(R.id.join_id_check_btn);
        IDCheckBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HttpServiceClient.Retros_IDCheck(IdEditText.getText().toString()).enqueue(new Callback<IdCheckResult>() {
                    @Override
                    public void onResponse(Call<IdCheckResult> call, Response<IdCheckResult> response) {

                        if(response.body().getError().equals("false")&&Flags[0]) {
                            IdConfirmTv.setText("사용이 가능한 아이디입니다.");
                            IdConfirmTv.setVisibility(View.VISIBLE);
                        }
                        else if(response.body().getError().equals("true")&&Flags[0]){
                            IdConfirmTv.setText("이미 사용중인 아이디입니다.");
                            IdConfirmTv.setVisibility(View.VISIBLE);
                        }
                        else{
                            IdConfirmTv.setText("사용이 불가능한 아이디입니다.");
                            IdConfirmTv.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onFailure(Call<IdCheckResult> call, Throwable t) {
                        Log.e("check error",t.getMessage());
                        Toaster("연결 실패");
                    }
                });
            }
        });
        IdEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String id=s.toString();
                if(id.length()>=6){
                    IdEditText.setPrimaryColor(Color.parseColor("#fc8527"));
                    Flags[0]=true;
                }
                else {
                    SetColor(IdEditText,Color.parseColor("#c23e3e"));
                    IdConfirmTv.setText("중복체크를 해주세요.");
                    IdConfirmTv.setVisibility(View.VISIBLE);
                    Flags[0]=false;
                }
            }
            @Override
            public void afterTextChanged(Editable s) {
                Flags[0]=true;
            }
        });
    }
    void PasswordViewController(View v){
        j3=(ImageView)v.findViewById(R.id.join3_title_img);
        Glide.with(v.getContext()).load(R.drawable.join3).into(j3);
        PasswordEditText=(MaterialEditText)v.findViewById(R.id.join_pass_input_edt);
        PassworConfirmEdt=(MaterialEditText)v.findViewById(R.id.join_pass_confirm_edt);
        PasswordEditText.setFilters(new InputFilter[]{
                new InputFilter.LengthFilter(15)});
        PasswordEditText.setMaxCharacters(15);
        PasswordEditText.setMinCharacters(8);
        PasswordEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String password=s.toString();
                if(password.length()<8){
                    SetColor(PasswordEditText,Color.parseColor("#c23e3e"));
                    PasswordEditText.setUnderlineColor(Color.parseColor("#c23e3e"));
                    PasswordEditText.setHelperText("8자 이상 15자 이하 입력");
                }
                else if(password.length()>=8&&password.length()<=15){
                    PasswordEditText.setPrimaryColor(Color.parseColor("#fc8527"));
                    PasswordEditText.setPrimaryColor(Color.parseColor("#fc8527"));
                    PasswordEditText.setHelperText("가능");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        PassworConfirmEdt.setFilters(new InputFilter[]{
                new InputFilter.LengthFilter(15)});
        PassworConfirmEdt.setMaxCharacters(15);
        PassworConfirmEdt.setMinCharacters(8);
        PassworConfirmEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String password=s.toString();
                if(!(password.equals(PasswordEditText.getText().toString()))){
                    PassworConfirmEdt.setPrimaryColor(Color.parseColor("#c23e3e"));
                    PassworConfirmEdt.setUnderlineColor(Color.parseColor("#c23e3e"));
                    PassworConfirmEdt.setHelperText("비밀번호와 동일하게 입력해주세요");
                    Flags[1]=false;
                }
                else if(password.equals(PasswordEditText.getText().toString())){
                    PassworConfirmEdt.setPrimaryColor(Color.parseColor("#fc8527"));
                    PassworConfirmEdt.setPrimaryColor(Color.parseColor("#fc8527"));
                    PassworConfirmEdt.setHelperText("동일함");
                    Flags[1]=true;
                }
            }
            @Override
            public void afterTextChanged(Editable s) {}
        });
    }
    void PhoneViewController(View v){
        j4=(ImageView)v.findViewById(R.id.join4_title_img);
        Glide.with(v.getContext()).load(R.drawable.join4).into(j4);
        PhoneEditText=(MaterialEditText)v.findViewById(R.id.join_number_input_edt);
        PhoneEditText.setFilters(new InputFilter[]{
                new InputFilter.LengthFilter(11)});
        PhoneEditText.setMaxCharacters(11);
        PhoneEditText.setMinCharacters(10);
        PhoneEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
               String phonenum=s.toString();
                if(phonenum.length()>=10){
                    PhoneEditText.setHelperText("");
                    PhoneEditText.setPrimaryColor(Color.parseColor("#fc8527"));
                    PhoneEditText.setPrimaryColor(Color.parseColor("#fc8527"));
                    Flags[2]=true;
                }
                else{
                    PhoneEditText.setHelperText("핸드폰 번호 입력 (- 제외)");
                    PhoneEditText.setUnderlineColor(Color.parseColor("#c23e3e"));
                    PhoneEditText.setPrimaryColor(Color.parseColor("#c23e3e"));
                    Flags[2]=false;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
    void EmailViewController(View v){
        j5=(ImageView)v.findViewById(R.id.join5_title_img);
        Glide.with(v.getContext()).load(R.drawable.join5).into(j5);
        EmailEdtiText=(MaterialEditText)v.findViewById(R.id.join_email_input_edt);
        EmailSendBtn=(Button)v.findViewById(R.id.join_email_send_btn);
        EmailEdtiText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String email=s.toString();
                if(checkEmail(email)){
                    EmailEdtiText.setHelperText("");
                    EmailEdtiText.setPrimaryColor(Color.parseColor("#fc8527"));
                    EmailEdtiText.setPrimaryColor(Color.parseColor("#fc8527"));
                    Flags[3]=true;
                }
                else{
                    EmailEdtiText.setHelperText("이메일 형식으로 입력해주세요");
                    EmailEdtiText.setPrimaryColor(Color.parseColor("#c23e3e"));
                    EmailEdtiText.setUnderlineColor(Color.parseColor("#c23e3e"));
                    Flags[3]=false;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        EmailSendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ID=IdEditText.getText().toString();
                Pass=PasswordEditText.getText().toString();
                Email=EmailEdtiText.getText().toString();
                Phone=PhoneEditText.getText().toString();
                /*
                * 텍스트 필드들 타입검사 로직
                * */
                if(CheckInFlags(Flags)){
                    HttpServiceClient.Retros_JOIN(ID,Pass,Email,Phone).enqueue(new Callback<Results>() {
                        @Override
                        public void onResponse(Call<Results> call, Response<Results> response) {
                            if (response.isSuccessful())
                                HttpComplete(response.body());
                            else{
                                Log.e("Server Error",response.errorBody()+"");
                            }
                        }

                        @Override
                        public void onFailure(Call<Results> call, Throwable t) {
                            Log.e("Error" , t.getMessage().toString());
                        }
                    });
                }
                else{
                    Toast.makeText(Join_Activity.this, "입력되지 않은 값이 있습니다.", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }
    void HttpComplete(Results result){
                String error=result.getError();
                if(error.equals("false")){
                    Toast.makeText(Join_Activity.this, "가입 되었습니다!", Toast.LENGTH_SHORT).show();
                    vf.showNext();
                    prev.setVisibility(View.INVISIBLE);
                    next.setVisibility(View.INVISIBLE);
                }
                else if(error.equals("true")) {
                    String error_msg = result.getMsg();
                    Toast.makeText(Join_Activity.this, error_msg, Toast.LENGTH_SHORT).show();
                    if (error_msg.equals("계정이 이미 있습니다")) {
                        Toast.makeText(Join_Activity.this, "중복된 아이디입니다", Toast.LENGTH_SHORT).show();
                        IdConfirmTv.setVisibility(View.VISIBLE);
                    }
                }
    }
    void ConfirmViewController(View v){
        j6=(ImageView)v.findViewById(R.id.join6_title_img);
        Glide.with(v.getContext()).load(R.drawable.join6).into(j6);
        ConfirmBtn=(Button)v.findViewById(R.id.join_confirm_btn);
        ConfirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),Login_Activity.class));
            }
        });
    }
    public static boolean checkEmail(String email){
        String regex = "^[_a-zA-Z0-9-\\.]+@[\\.a-zA-Z0-9-]+\\.[a-zA-Z]+$";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(email);
        boolean isNormal = m.matches();
        return isNormal;
    }

    @Override
    public void onBackPressed() {
        pressed.OnBackPressedClose(1);
    }



}


