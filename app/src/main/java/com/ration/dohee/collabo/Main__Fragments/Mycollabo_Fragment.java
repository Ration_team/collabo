package com.ration.dohee.collabo.Main__Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.ration.dohee.collabo.Https.HttpServiceClient;
import com.ration.dohee.collabo.Https.TeamListitem;
import com.ration.dohee.collabo.Https.TeamListitems;
import com.ration.dohee.collabo.Login_Activity;
import com.ration.dohee.collabo.Main__Fragments.Util.MyCollabo_Adapter;
import com.ration.dohee.collabo.R;
import com.ration.dohee.collabo.Util.FragmentsForSign;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by User on 2017-06-22.
 */

public class Mycollabo_Fragment extends FragmentsForSign {
    Unbinder unbinder;


    ArrayList<TeamListitem> items;
    public Mycollabo_Fragment() {
    }

    LinearLayout sign;
    ListView main;
    Button btn;
    MyCollabo_Adapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.activity_mycollabo, container, false);
        main = (ListView) view.findViewById(R.id.mycollabo_main_list);
        sign = (LinearLayout) view.findViewById(R.id.mycollabo_sign_view);
        btn = (Button) view.findViewById(R.id.mycollabo_sign_btn);


        adapter=new MyCollabo_Adapter();
        Retro();
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity().getApplicationContext(), Login_Activity.class));
            }
        });

        main.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SweetAlertDialog dlg=new SweetAlertDialog(getContext(),SweetAlertDialog.NORMAL_TYPE);
                dlg.setConfirmText("확인");
                dlg.setTitleText("준비중입니다");
                dlg.setContentText("빠른시일내로 업데이트하겠습니다.");
                dlg.show();
            }
        });


        SettingView(main, sign);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }
    void Setting_List(){

    }
    void Retro(){
        HttpServiceClient.getTeamList().enqueue(new Callback<TeamListitems>() {
            @Override
            public void onResponse(Call<TeamListitems> call, Response<TeamListitems> response) {
                if(response.body().getError().equals("false"))
                {
                    if(response.body().getResult()!=null)
                    adapter.items.addAll(response.body().getResult());

                    main.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<TeamListitems> call, Throwable t) {

            }
        });
    }



    @Override
    public void onStart() {
        super.onStart();
        SettingView(main, sign);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
