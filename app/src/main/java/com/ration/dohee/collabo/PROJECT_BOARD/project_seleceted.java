package com.ration.dohee.collabo.PROJECT_BOARD;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.ration.dohee.collabo.DATA.ActionBarData;
import com.ration.dohee.collabo.DATA.DATA;
import com.ration.dohee.collabo.DATA.UNIQ;
import com.ration.dohee.collabo.DATA.User_INFO;
import com.ration.dohee.collabo.Https.Favor_delresult;
import com.ration.dohee.collabo.Https.Favor_result;
import com.ration.dohee.collabo.Https.Favorite_info;
import com.ration.dohee.collabo.Https.HttpServiceClient;
import com.ration.dohee.collabo.Https.Recruit_Apply;
import com.ration.dohee.collabo.Https.Recruit_board;
import com.ration.dohee.collabo.Https.Recruit_boards;
import com.ration.dohee.collabo.R;
import com.ration.dohee.collabo.Util.ActionBar;
import com.sackcentury.shinebuttonlib.ShineButton;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ration.dohee.collabo.R.id.project_link_Tv;
import static com.ration.dohee.collabo.R.id.project_participate_Btn;

public class project_seleceted extends ActionBar {

    int idx;
    int Favposition = 0;
    Recruit_board board_data = null;
    @BindView(R.id.drawer_indicator)
    ImageView drawerIndicator;
    @BindView(R.id.imageButton)
    ImageButton imageButton;
    @BindView(R.id.searchBtn)
    ImageButton searchBtn;
    @BindView(R.id.project_seleceted_title_Tv)
    TextView projectSelecetedTitleTv;
    @BindView(R.id.project_seleceted_favorite_Btn)
    ShineButton projectSelecetedFavoriteBtn;
    @BindView(R.id.project_profile_Img)
    CircleImageView projectProfileImg;
    @BindView(R.id.project_profile_id_Tv)
    TextView projectProfileIdTv;
    @BindView(R.id.project_start_day_tv)
    TextView projectStartDayTv;
    @BindView(R.id.project_job_Tv)
    TextView projectJobTv;
    @BindView(R.id.project_number_Tv)
    TextView projectNumberTv;
    @BindView(R.id.project_end_day_Tv)
    TextView projectEndDayTv;
    @BindView(project_link_Tv)
    TextView projectLinkTv;
    @BindView(R.id.project_uplode_ImgV)
    ImageView projectUplodeImgV;
    @BindView(R.id.project_uplode_text_Tv)
    TextView projectUplodeTextTv;
    @BindView(project_participate_Btn)
    Button projectParticipateBtn;
    @BindView(R.id.union_navmenu_drawerlayout_menu)
    NavigationView unionNavmenuDrawerlayoutMenu;
    @BindView(R.id.union_navmenu_drawerlayout)
    DrawerLayout unionNavmenuDrawerlayout;
    String kakao;

    boolean host;
    String board_name;
    @BindView(R.id.project_seleceted_edit_btn)
    TextView projectSelecetedEditBtn;
    InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_seleceted);
        SetActionBar((View) findViewById(ActionBarData.ActionBar));
        ButterKnife.bind(this);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.admob_video_write));
        mInterstitialAd.loadAd(new AdRequest.Builder().addTestDevice("01D06A96016428712381997520D4CF5A").build());
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
                Log.i("Ads", "onAdFailedToLoad");
                mInterstitialAd.loadAd(new AdRequest.Builder().addTestDevice("01D06A96016428712381997520D4CF5A").build());
                Toaster("광고가 다 떨어졌어요 ! ");
                Retros_apply();
            }
            @Override
            public void onAdClosed() {
                // Code to be executed when when the interstitial ad is closed.
                Log.i("Ads", "onAdClosed");
                mInterstitialAd.loadAd(new AdRequest.Builder().addTestDevice("01D06A96016428712381997520D4CF5A").build());
                Retros_apply();
            }
        });

        host = getIntent().getBooleanExtra("host", false);
        idx = getIntent().getIntExtra("idx", -1);
        board_name = getIntent().getStringExtra("board_name");
        projectLinkTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), WebActivity.class);
                intent.putExtra("url", projectLinkTv.getText().toString());

                startActivity(intent);
            }
        });
        if (host) {
            projectSelecetedFavoriteBtn.setVisibility(View.GONE);
            projectSelecetedEditBtn.setVisibility(View.VISIBLE);
        }
        else{
            projectSelecetedFavoriteBtn.setVisibility(View.VISIBLE);
            projectSelecetedEditBtn.setVisibility(View.GONE);

        }

        projectSelecetedEditBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent inte=new Intent(getApplicationContext(),project_write.class);
                inte.putExtra("edit",true);
                inte.putExtra("board_name",board_name);
                project_write.board_result=board_data;
                startActivity(inte);
                finish();
            }
        });

            projectParticipateBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!(board_data.getMb_id().equals(User_INFO.getID()))) {
                        SweetAlertDialog dlg = new SweetAlertDialog(project_seleceted.this, SweetAlertDialog.SUCCESS_TYPE);
                        dlg.setCancelText("돌아가기");
                        dlg.setConfirmText("지원하기");
                        dlg.setTitleText("콜라보 지원");
                        dlg.setContentText("지원시 호스트와 연결된 오픈채팅으로 이동합니다.");
                        dlg.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.setConfirmText("시청후 지원");
                                sweetAlertDialog.setCancelText("지원취소");
                                sweetAlertDialog.setTitle("지원하기");
                                sweetAlertDialog.setContentText("무분별한 지원방지를 위해 광고시청후 오픈채팅으로 연결됩니다");
                                sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        mInterstitialAd.show();
                                        sweetAlertDialog.dismiss();
                                    }
                                });

                                sweetAlertDialog.show();
                            }
                        });
                        dlg.show();
                    } else {//TODO 호스트 접근  .. 지원목록
                        Intent inte = new Intent(getApplicationContext(), Main_Myboard_applicationlist.class);
                        inte.putExtra("idx", idx);
                        inte.putExtra("board_name", getIntent().getStringExtra("board_name"));
                        startActivity(inte);

                    }

                }
            });


        Log.e("IDx", idx + "");
        Retros();


    }

    protected void Retros_apply(){
        Map<String, String> map = new HashMap<>();
        map.put("board_name", board_name);
        map.put("mb_id", User_INFO.getID());
        map.put("unique_id", UNIQ.unique_id);
        map.put("idx", idx + "");
        HttpServiceClient.Retros_RecruitApply(map).enqueue(new Callback<Recruit_Apply>() {
            @Override
            public void onResponse(Call<Recruit_Apply> call, Response<Recruit_Apply> response) {
                if (response.isSuccessful()) {
                    if (response.body().getError().equals("false")) {
                        if (response.body().getMsg().equals("지원 성공")) {
                            // 카카오톡 링크 설정후 intent실행시 카카오톡 오픈채팅방  웹으로 연결됨
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(kakao));
                            startActivity(intent);
                        } else {
                            Toaster("이미 지원한 게시글 입니다.");
                        }
                    } else {
                        Toaster("Error 잠시후 다시 이용해주세요" + response.body().getMsg() == null ? "msg is null" : response.body().getMsg());
                    }
                } else {
                    Log.e("Retro Error", response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<Recruit_Apply> call, Throwable t) {
                Log.e("Server Error", t.getMessage());
            }
        });
    }


    @Override
    protected void onStart() {
        super.onStart();


    }

    private void Retros() {
        HttpServiceClient.Retros_RecruitView(getIntent().getStringExtra("board_name"), idx)
                .enqueue(new Callback<Recruit_boards>() {
                    @Override
                    public void onResponse(Call<Recruit_boards> call, Response<Recruit_boards> response) {
                        if (response.isSuccessful()) {
                            if (response.body().getError().equals("false"))
                                board_data = response.body().getResult();
                            if (board_data != null) {
                                projectSelecetedFavoriteBtn.setChecked(getIntent().getBooleanExtra("isfavor", false));
                                projectSelecetedTitleTv.setText(board_data.getWr_subject());
                                Glide.with(getApplicationContext()).load(DATA.ImgRootURL + "/" + board_data.getWr_file()).into(projectUplodeImgV);
                                Glide.with(getApplicationContext()).load(DATA.ImgRootURL + "/" + board_data.getMb_img()).into(projectProfileImg);
                                projectProfileIdTv.setText(board_data.getMb_id());
                                projectStartDayTv.setText(board_data.getRecruit_start_date().substring(2,10));
                                projectEndDayTv.setText("~ " + board_data.getRecruit_finish_date().substring(2,10));
                                projectJobTv.setText(board_data.getPart());
                                projectLinkTv.setText(board_data.getLink());
                                projectNumberTv.setText(board_data.getMax_num());
                                kakao = board_data.getKakaotalk();
                                projectUplodeTextTv.setText(board_data.getWr_content());
                            } else if (board_data == null) {
                                Toast.makeText(getApplicationContext(), "게시글이 존재하지 않습니다.", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(project_seleceted.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Log.e("ERROR", response.errorBody().toString());
                        }
                        if ((board_data.getMb_id().equals(User_INFO.getID()))) {
                            projectParticipateBtn.setText("지원 목록");
                        }
                    }

                    @Override
                    public void onFailure(Call<Recruit_boards> call, Throwable t) {
                        Log.e("SERVER ERROR", t.getMessage());
                    }
                });
    }


    @OnClick(R.id.project_seleceted_favorite_Btn)
    public void onViewClicked() {
        // 즐겨찾기 등록
        if (projectSelecetedFavoriteBtn.isChecked()) {
            HttpServiceClient.Retros_FavorAdd(idx, board_data.getWr_subject(), board_name)
                    .enqueue(new Callback<Favor_result>() {
                        @Override
                        public void onResponse(Call<Favor_result> call, Response<Favor_result> response) {
                            if (response.isSuccessful()) {
                                Favorite_info ref = response.body().getResult().get(0);
                                User_INFO.getfavors(board_name).add(new Favorite_info(ref.getId(), ref.getIdx(), ref.getBoard_name(), ref.getWr_subject()));
                            } else if (!response.isSuccessful()) {
                                Log.e("ERROR", response.errorBody().toString());
                            }
                        }

                        @Override
                        public void onFailure(Call<Favor_result> call, Throwable t) {
                            Log.e("SERVER ERROR", t.getMessage());
                        }
                    });
        }//즐겨찾기 해제
        else if (!projectSelecetedFavoriteBtn.isChecked()) {
            for (int i = 0; i < User_INFO.getfavors(board_name).size(); i++) {
                if (idx == User_INFO.getfavors(board_name).get(i).getIdx()) {
                    Favposition = i;
                    break;
                }
            }
            HttpServiceClient.Retros_FavorDel(Favposition, board_name).enqueue(new Callback<Favor_delresult>() {
                @Override
                public void onResponse(Call<Favor_delresult> call, Response<Favor_delresult> response) {
                    if (response.isSuccessful()) {


                    } else if (!response.isSuccessful()) {
                        Log.e("ERROR", response.errorBody().toString());
                    }
                }

                @Override
                public void onFailure(Call<Favor_delresult> call, Throwable t) {
                    Log.e("SERVER ERROR", t.getMessage());
                }
            });
        }
    }

}
