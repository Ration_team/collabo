package com.ration.dohee.collabo.Https;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samsung on 2017-08-08.
 */

public class Favorite_info {
    @SerializedName("id")
    int id;
    @SerializedName("idx")
    int idx;
    @SerializedName("board_name")
    String board_name;
    @SerializedName("wr_subject")
    String wr_subject;

    public Favorite_info() {
    }

    public Favorite_info(int id, int idx, String board_name, String wr_subject) {
        this.id = id;
        this.idx = idx;
        this.board_name = board_name;
        this.wr_subject = wr_subject;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setIdx(int idx) {
        this.idx = idx;
    }

    public String getBoard_name() {
        return board_name;
    }

    public void setBoard_name(String board_name) {
        this.board_name = board_name;
    }

    public String getWr_subject() {
        return wr_subject;
    }

    public void setWr_subject(String wr_subject) {
        this.wr_subject = wr_subject;
    }

    public int getId() {
        return id;
    }

    public int getIdx() {
        return idx;
    }
}
