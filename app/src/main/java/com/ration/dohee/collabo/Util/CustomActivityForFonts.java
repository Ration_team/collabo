package com.ration.dohee.collabo.Util;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.ration.dohee.collabo.DATA.DATA;
import com.ration.dohee.collabo.DATA.User_INFO;
import com.ration.dohee.collabo.R;
import com.tsengvn.typekit.TypekitContextWrapper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import okhttp3.MediaType;
import okhttp3.RequestBody;

import static android.os.Build.VERSION.SDK_INT;

/**
 * Created by Samsung on 2017-06-02.
 */

public class CustomActivityForFonts extends AppCompatActivity{
    // 폰트 변경 클래스이다 변경할 액티비티 마다 AppCompatActivity대신 CustomActivityForFonts를 상속해주면 된다

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            if(SDK_INT>=21) {
                Window window = getWindow();

                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

                window.setStatusBarColor(getResources().getColor(R.color.mainColor));
            }
        }
        catch (Exception e)
        {

        }


    }
    public void SetLogin(boolean logined){
        DATA.set_isLogin(getSharedPreferences("Auto_Login",MODE_PRIVATE),logined);
    }
    public void SetUser_INFO(String id,String pass,String email )
    {
        User_INFO.UserSetter(id,pass,email,getSharedPreferences("Auto_Login",MODE_PRIVATE));
    }
    public void Toaster(String msg){
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
    public void SetFlags(boolean Flags[]){
        for(int i=0;i<Flags.length;i++)
        {
            Flags[i]=false;
        }
    }
    public boolean CheckInFlags(boolean Flags[]){
        for(int i=0;i<Flags.length;i++){
            if(!Flags[i]){
                return false;
            }
        }
        return true;
    }
    public RequestBody ToRequesBody(String ref)
    {
        return RequestBody.create(MediaType.parse("text/plain"), ref);
    }

    public int exifOrientationToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
            return 90;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
            return 180;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return 270;
        }
        return 0;
    }
    public File SaveBitmapToFileCache(Bitmap bitmap, String strFilePath,
                                      String filename) {

        File file = new File(strFilePath);

        // If no folders
        if (!file.exists()) {
            file.mkdirs();
        }

        File fileCacheItem = new File(strFilePath + filename);
        if(fileCacheItem.exists())
        {
            fileCacheItem.delete();
        }
        OutputStream out = null;

        try {
            fileCacheItem.createNewFile();
            out = new FileOutputStream(fileCacheItem);

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return fileCacheItem ;
    }
    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);

    }
    public File SendPicture(Intent data ) {
        File ref,file=null;
        boolean flag=true;
        ref=new File("/storage/emulated/0/collabo/tester.jpeg");
        if(ref.exists()){
            ref.delete();
        }
        Uri imgUri = data.getData();
        String imagePath = getRealPathFromURI(imgUri); // path 경로
        ExifInterface exif = null;
        try {
            exif = new ExifInterface(imagePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Bitmap bitmap = null;
        Bitmap bit=null;
        try {
            bit = BitmapFactory.decodeFile(imagePath);
            ref=SaveBitmapToFileCache(bit,"/storage/emulated/0/collabo/","tester2.jpeg");
            bit.recycle();
            Log.e("filesize",ref.length()+"bytes");
            if(ref.length()<1048576){
                flag=false;
            }
            ref.delete();
        }catch (OutOfMemoryError e)
        {
            Log.e("OOM",e.getMessage());
            flag=false;
            Toast.makeText(this,"OOM이당"+ e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        catch(Exception e)
        {
            Log.e("ERROR",e.getMessage());
        }



        BitmapFactory.Options option = new BitmapFactory.Options                                                                            ();
        option.inSampleSize = 2;

        try {

            int exifOrientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            int exifDegree = exifOrientationToDegrees(exifOrientation);
            if(flag) {
                bitmap = BitmapFactory.decodeFile(imagePath, option);//경로를 통해 비트맵으로 전환
                Log.e("resize","true");
            }
            else{
                Log.e("resize","false");
                bitmap=BitmapFactory.decodeFile(imagePath);
            }
            Matrix matrix = new Matrix();
            matrix.postRotate(exifDegree);
            bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            file = SaveBitmapToFileCache(bitmap, "/storage/emulated/0/collabo/", "tester.jpeg");
            if (file.length() >= 1048576) {
                bitmap = BitmapFactory.decodeFile(file.getPath(), option);
                file = SaveBitmapToFileCache(bitmap, "/storage/emulated/0/collabo/", "tester.jpeg");
            }
            Log.e("File PATH",file.getPath());
        } catch (Exception e) {
            Log.e("IMAGEFILE", e.getMessage());
            Toaster("다시 시도해주세요!");
        }
        finally {
            if(bitmap!=null)
            bitmap.recycle();
        }
        return file;

    }
}

