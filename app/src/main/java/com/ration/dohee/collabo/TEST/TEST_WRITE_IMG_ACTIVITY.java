package com.ration.dohee.collabo.TEST;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ration.dohee.collabo.Board_NOTICE.Notice_Seleceted_Activity;
import com.ration.dohee.collabo.DATA.DATA;
import com.ration.dohee.collabo.DATA.UNIQ;
import com.ration.dohee.collabo.DATA.User_INFO;
import com.ration.dohee.collabo.Https.Board_Commits;
import com.ration.dohee.collabo.Https.HttpServiceClient;
import com.ration.dohee.collabo.R;
import com.ration.dohee.collabo.Util.CustomActivityForFonts;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ration.dohee.collabo.DATA.UNIQ.unique_id;

public class TEST_WRITE_IMG_ACTIVITY extends CustomActivityForFonts implements View.OnClickListener {
    MaterialEditText titleedt, contentsedt;
    Button submit, post;
    String TITLE, CONTENTS, ID;
    File file;
    ImageView img;
    SharedPreferences sharedPreferences;
    boolean Flags[] = new boolean[3];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test__write__img__activity);
        sharedPreferences = getSharedPreferences("Auto_Login", MODE_PRIVATE);
        titleedt = (MaterialEditText) findViewById(R.id.test_title_edt2);
        img = (ImageView) findViewById(R.id.test_imgview);
        post = (Button) findViewById(R.id.testbtn);
        post.setOnClickListener(this);
        contentsedt = (MaterialEditText) findViewById(R.id.test_contents_edt2);
        submit = (Button) findViewById(R.id.test_submit_btn);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (CheckFlags()) {
                    TITLE = titleedt.getText().toString();
                    CONTENTS = contentsedt.getText().toString();
                    RetroFit();
                }
            }
        });
        titleedt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String arr = s.toString();
                if (arr.length() <= 0) {
                    Flags[0] = false;
                } else {
                    Flags[0] = true;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        contentsedt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String arr = s.toString();
                if (arr.length() <= 0) {
                    Flags[1] = false;
                } else {
                    Flags[1] = true;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }

    void RetroFit() {
        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(TEST_WRITE_IMG_ACTIVITY.this);
        progressDialog.setMessage("Uploading");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();

        Map<String, RequestBody> map = new HashMap<>();
        ID = sharedPreferences.getString("id", "");
        Log.e("UNIQ", unique_id + "");
        map.put("board_name", ToRequesBody("board_notice"));
        map.put("mb_id", ToRequesBody(User_INFO.getID()));
        map.put("wr_subject", ToRequesBody(TITLE));
        map.put("wr_content", ToRequesBody(CONTENTS));
        map.put("unique_id", ToRequesBody(UNIQ.unique_id));

        RequestBody fileBody = RequestBody.create(MediaType.parse("image/jpeg"), file);
        map.put("wr_file\"; filename=\"" + file.getPath() + "\"", fileBody);
        map.put("wr_file", fileBody);


        Log.e("filename", file.getPath());

        HttpServiceClient.Retros_BoardCommit(map).enqueue(new Callback<Board_Commits>() {
            @Override
            public void onResponse(Call<Board_Commits> call, Response<Board_Commits> response) {
                Log.e("EROOR2", call.toString());
                progressDialog.dismiss();
                Board_Commits ref = response.body();
                Log.e("Success", ref.getError());
                Toast.makeText(TEST_WRITE_IMG_ACTIVITY.this, "성공", Toast.LENGTH_SHORT).show();
                file.delete();
                Snackbar.make(getWindow().getDecorView().getRootView(),ref.getMsg(),Snackbar.LENGTH_SHORT).show();
                Intent inte=new Intent(getApplicationContext(), Notice_Seleceted_Activity.class);
                inte.putExtra("idx",ref.getResult().getIdx());
                startActivity(inte);
                finish();
            }

            @Override
            public void onFailure(Call<Board_Commits> call, Throwable t) {
                Toast.makeText(TEST_WRITE_IMG_ACTIVITY.this, "실패" + t.getMessage().toString(), Toast.LENGTH_SHORT).show();
                Log.e("EROOR", t.getMessage().toString());
                progressDialog.dismiss();
                Log.e("EROOR2", call.toString());
                file.delete();

            }

        });
    }

    boolean CheckFlags() {
//        for(int i=0;i<Flags.length;i++){
//            if(!Flags[i]){
//                return false;
//            }
        //if(i==0)
        TITLE = titleedt.getText().toString();
        CONTENTS = contentsedt.getText().toString();
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.testbtn:
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setData(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult(intent, DATA.GALLERY_DIRECT_CODE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if(file!=null)
            {
                file.delete();
            }
            file=SendPicture(data); //갤러리에서 가져오기

            Log.e("???",file.getPath());

            Glide.with(getApplicationContext()).load(file).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(img);



        }
    }



}
