package com.ration.dohee.collabo.Https;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Samsung on 2017-08-08.
 */

public class Board_Recruit_Results {
    @SerializedName("error")
    String error;

    @SerializedName("result")
    ArrayList<Board_Recruit_Result> result;

    @SerializedName("favorite_list")
    ArrayList<Favorite_info> favors;

    @SerializedName("total_count")
    int total_count;

    @SerializedName("page_count")
    int Page;

    public int getPage() {
        return Page;
    }

    public ArrayList<Favorite_info> getFavors() {
        return favors;
    }

    public int getTotal_count() {
        return total_count;
    }

    public String getError() {
        return error;
    }

    public ArrayList<Board_Recruit_Result> getResult() {
        return result;
    }
}
