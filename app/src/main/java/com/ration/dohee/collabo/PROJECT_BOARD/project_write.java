package com.ration.dohee.collabo.PROJECT_BOARD;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ration.dohee.collabo.DATA.ActionBarData;
import com.ration.dohee.collabo.DATA.DATA;
import com.ration.dohee.collabo.DATA.UNIQ;
import com.ration.dohee.collabo.DATA.User_INFO;
import com.ration.dohee.collabo.Https.HttpServiceClient;
import com.ration.dohee.collabo.Https.Recruit_board;
import com.ration.dohee.collabo.Https.Recruit_boards;
import com.ration.dohee.collabo.R;
import com.ration.dohee.collabo.Util.ActionBar;
import com.ration.dohee.collabo.Util.EditTextWatcher;

import java.io.File;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class project_write extends ActionBar {

    public static Recruit_board board_result;
    int Resultok = 15;
    String TITLE, JOB, STARTDATE, ENDDATE, NUM, KAKAO, LINK, CONTENTS;
    TextView today_date_TV, end_date_TV;
    ImageButton linkBtn, imgupload;
    ImageView Img;
    Button uploadbtn;
    EditText job_Et, number_Et, url_Et, write_Et, title_Et;
    int year, month, day;
    File file=null;
    boolean editable,is_delete;
    boolean Flags[] = new boolean[6];
    @BindView(R.id.project_selected_imgdel_btn)
    TextView projectSelectedImgdelBtn;
    private DatePickerDialog.OnDateSetListener dateSetListener;

    @Override
    protected void onStart() {
        super.onStart();
        imgupload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setData(MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult(intent, DATA.GALLERY_DIRECT_CODE);
            }
        });
        uploadbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Map<String, RequestBody> map = new HashMap<String, RequestBody>();
                if (!editable) {
                if (CheckInFlags(Flags)) {
                    //TODO...  Retrofit uses...


                    map.put("board_name", ToRequesBody(getIntent().getStringExtra("board_name")));
                    map.put("wr_subject", ToRequesBody(TITLE));
                    map.put("recruit_start_date", ToRequesBody(STARTDATE));
                    map.put("recruit_finish_date", ToRequesBody(ENDDATE));
                    map.put("max_num", ToRequesBody(NUM));
                    map.put("wr_content", ToRequesBody(CONTENTS));
                    map.put("mb_id", ToRequesBody(User_INFO.getID()));
                    map.put("kakaotalk", ToRequesBody(KAKAO));
                    map.put("part", ToRequesBody(JOB));
                    map.put("unique_id", ToRequesBody(UNIQ.unique_id));
                    if (file != null) {
                        RequestBody fileBody = RequestBody.create(MediaType.parse("image/jpeg"), file);
                        map.put("wr_file\"; filename=\"" + file.getPath() + "\"", fileBody);
                        map.put("wr_file", fileBody);
                        Log.e("filename", file.getPath());
                    } else {
                        map.put("wr_file", ToRequesBody("none"));
                    }
                    map.put("link", ToRequesBody(LINK));


                        HttpServiceClient.Retros_ReqruitWrite(map).enqueue(new Callback<Recruit_boards>() {
                            @Override
                            public void onResponse(Call<Recruit_boards> call, Response<Recruit_boards> response) {
                                if (response.isSuccessful()) {
                                    if(response.body().getResult()!=null) {
                                        Log.e("Sucess", "Succes");
                                        Toaster("올리기 성공!");
                                        finish();
                                    }
                                    else{
                                        Toaster("사진을 첨부해주세요!");
                                    }

                                } else {
                                    Toaster("누락된값이 있습니다");
                                    Log.e("Http Failed ", response.errorBody().toString());
                                }
                            }

                            @Override
                            public void onFailure(Call<Recruit_boards> call, Throwable t) {
                                Log.e("Retros Failed", t.getMessage());
                                finish();
                            }
                        });
                    }
                    else {
                    Toaster("누락된 사항이 있습니다.");
                    //TODO.. 필수사항 입력 누락
                    }
                } else if (editable) {
                        // TODO: 2017-10-12  editable Retros logical

                    map.put("board_name", ToRequesBody(getIntent().getStringExtra("board_name")));
                    map.put("wr_subject", ToRequesBody(TITLE));
                    map.put("recruit_start_date", ToRequesBody(STARTDATE));
                    map.put("recruit_finish_date", ToRequesBody(ENDDATE));
                    map.put("max_num", ToRequesBody(NUM));
                    map.put("wr_content", ToRequesBody(CONTENTS));
                    map.put("mb_id", ToRequesBody(User_INFO.getID()));
                    map.put("kakaotalk", ToRequesBody(KAKAO));
                    map.put("part", ToRequesBody(JOB));
                    map.put("unique_id", ToRequesBody(UNIQ.unique_id));
                    if (file != null) {
                        RequestBody fileBody = RequestBody.create(MediaType.parse("image/jpeg"), file);
                        map.put("wr_file\"; filename=\"" + file.getPath() + "\"", fileBody);
                        map.put("wr_file", fileBody);
                        Log.e("filename", file.getPath());
                    } else {
                        map.put("wr_file", ToRequesBody("none"));
                    }
                    map.put("link", ToRequesBody(LINK));


                    map.put("idx",ToRequesBody(board_result.getIdx()+""));
                        HttpServiceClient.Retros_Edit(map,is_delete).enqueue(new Callback<Recruit_boards>() {
                            @Override
                            public void onResponse(Call<Recruit_boards> call, Response<Recruit_boards> response) {
                                if (response.body().getError().equals("false"))
                                {
                                    Log.e("Sucess", "Succes");
                                    Toaster("올리기 성공!");
                                    Intent intent = new Intent(getApplicationContext(), project_seleceted.class);
                                    intent.putExtra("idx", board_result.getIdx());
                                    intent.putExtra("board_name",getIntent().getStringExtra("board_name"));
                                    startActivity(intent);
                                }
                            }

                            @Override
                            public void onFailure(Call<Recruit_boards> call, Throwable t) {
                                Log.e("SERVER ERROR",t.getMessage());
                                finish();
                            }
                        });

                    }



            }
        });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_write);
        ButterKnife.bind(this);
        file = null;
        is_delete=false;
        LINK = "none";
        long now = System.currentTimeMillis();
        Date date = new Date(now);
        SimpleDateFormat sdfNow = new SimpleDateFormat("yy.MM.dd");
        STARTDATE = sdfNow.format(date);
        SetFlags(Flags);
        dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                String msg = String.format("%d.%02d.%02d", year - 2000, monthOfYear + 1, dayOfMonth);
                end_date_TV.setText(msg);
                Flags[1] = true;
                ENDDATE = msg;

            }
        };
        editable = getIntent().getBooleanExtra("edit", false);
        SetActionBar(findViewById(ActionBarData.ActionBar));
        Img = (ImageView) findViewById(R.id.project_write_img);
        uploadbtn = (Button) findViewById(R.id.project_uplode_Btn);
        imgupload = (ImageButton) findViewById(R.id.project_wirte_imgbtn);
        linkBtn = (ImageButton) findViewById(R.id.project_linkBtn);
        today_date_TV = (TextView) findViewById(R.id.project_today_date_TV);
        end_date_TV = (TextView) findViewById(R.id.project_end_date_TV);
        job_Et = (EditText) findViewById(R.id.project_job_Et);
        number_Et = (EditText) findViewById(R.id.project_number_Et);
        url_Et = (EditText) findViewById(R.id.project_url_Et);
        write_Et = (EditText) findViewById(R.id.project_write_Et);
        title_Et = (EditText) findViewById(R.id.project_title_Et);

        today_date_TV.setText(STARTDATE + " ~ ");
        end_date_TV.setHint(Html.fromHtml("<u>" + STARTDATE + "</u>"));
        GregorianCalendar calendar = new GregorianCalendar();

        year = calendar.get(Calendar.YEAR);

        month = calendar.get(Calendar.MONTH);

        day = calendar.get(Calendar.DAY_OF_MONTH);

        end_date_TV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(project_write.this, dateSetListener, year, month, day).show();
            }
        });

        linkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), project_write_dialog.class);
                startActivityForResult(intent, 1);
            }

        });

        title_Et.addTextChangedListener(new EditTextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    Flags[0] = true;
                    TITLE = s.toString();
                } else {
                    Flags[0] = false;
                }
            }
        });

        number_Et.addTextChangedListener(new EditTextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    Flags[2] = true;
                    NUM = s.toString();
                } else
                    Flags[2] = false;
            }
        });

        job_Et.addTextChangedListener(new EditTextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    Flags[3] = true;
                    JOB = s.toString();
                } else
                    Flags[3] = false;
            }
        });

        url_Et.addTextChangedListener(new EditTextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    Flags[4] = true;
                    KAKAO = s.toString();
                    if (!KAKAO.matches("https://open.kakao.com/.*")) {
                        url_Et.setTextColor(Color.RED);
                        Flags[4] = false;
                    } else {
                        url_Et.setTextColor(Color.BLACK);
                        Flags[4] = true;
                    }
                } else
                    Flags[4] = false;
            }
        });
        write_Et.addTextChangedListener(new EditTextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    Flags[5] = true;
                    CONTENTS = s.toString();
                } else
                    Flags[5] = false;
            }
        });

        if (editable&&board_result!=null) {
            uploadbtn.setText("수정 완료");
            title_Et.setText(board_result.getWr_subject());
            today_date_TV.setText(board_result.getRecruit_start_date().substring(2,10)+" ~ ");
            end_date_TV.setText(board_result.getRecruit_finish_date().substring(2,10));
            job_Et.setText(board_result.getPart());
            number_Et.setText(board_result.getMax_num());
            url_Et.setText(board_result.getKakaotalk());
            Glide.with(getApplicationContext()).load(DATA.ImgRootURL+board_result.getWr_file()).placeholder(R.drawable.default_img).into(Img);


            TITLE=board_result.getWr_subject();
            STARTDATE=board_result.getRecruit_start_date();
            ENDDATE=board_result.getRecruit_finish_date();
            NUM=board_result.getMax_num();
            CONTENTS=board_result.getWr_content();
            KAKAO=board_result.getKakaotalk();
            JOB=board_result.getPart();
            LINK=board_result.getLink();





            Log.e("img error",DATA.ImgRootURL+board_result.getWr_file());
            write_Et.setText(board_result.getWr_content());


        }
        projectSelectedImgdelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                projectSelectedImgdelBtn.setVisibility(View.INVISIBLE);
                Img.setVisibility(View.GONE);
                file.delete();
                file=null;
                is_delete=true;
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (file != null) {
                file.delete();
            }
            file = SendPicture(data); //갤러리에서 가져오기


            if(editable)
                is_delete=true;
            Glide.with(getApplicationContext()).load(file).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(Img);
            Img.setVisibility(View.VISIBLE);
            projectSelectedImgdelBtn.setVisibility(View.VISIBLE);
        } else if (requestCode == 1 && resultCode == Resultok) {
            LINK = data.getStringExtra("linkurl");
        }
    }
}
