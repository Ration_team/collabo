package com.ration.dohee.collabo.Https;

import com.ration.dohee.collabo.Https.Apply_Results_Box.Apply_create;
import com.ration.dohee.collabo.Https.Apply_Results_Box.Apply_list;
import com.ration.dohee.collabo.Https.HttpContestInfo.ContestInfoViewResult;
import com.ration.dohee.collabo.Https.HttpContestInfo.ContestInfotListResult;
import com.ration.dohee.collabo.Https.HttpContestInfo.Default_result;

import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Created by Samsung on 2017-07-07.
 */

public interface PostSet {
    @FormUrlEncoded
    @POST("member/login.php")
    Call<Results> Sign_in(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("member/logout.php")
    Call<Logout_Results> Logout(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("member/register.php")
    Call<Results> Join_in(@FieldMap Map<String, String> params);

    @GET("notice/notice_view.php")
    Call<Board_Results> Board_View(@Query("board_name") String board_name, @Query("idx") int idx);

    @GET("notice/notice_list.php")
    Call<Board_ListResults> Board_Notice_List(@Query("board_name") String board_name, @Query("cur") int cur);
    @FormUrlEncoded
    @POST("recruit/recruit_list.php")
    Call<Board_Recruit_Results> Board_Reqruit_List(@Field("board_name") String board_name, @Field("cur") int cur, @Field("unique_id") String unique_id, @Field("mb_id") String mb_id);
    @FormUrlEncoded
    @POST("member/favorite_delete.php")
    Call<Favor_delresult> Favorite_Delete(@Field("id") int id, @Field("unique_id") String unique_id, @Field("mb_id") String mb_id, @Field("cur") int cur);
    @FormUrlEncoded
    @POST("member/favorite_add.php")
    Call<Favor_result> Favorite_Add(@Field("board_name") String board_name, @Field("idx") int cur, @Field("unique_id") String unique_id, @Field("mb_id") String mb_id,@Field("wr_subject")String title);

    @FormUrlEncoded
    @POST("notice/notice_delete.php")
    Call<Board_Statment> Board_Delete(@FieldMap Map <String,String> params);

    @GET("member/id_check.php")
    Call<IdCheckResult> Join_IDCHECK(@Query("mb_id") String id);

    @Multipart
    @POST("notice/notice_write.php")
    Call<Board_Commits> Board_Upload(@PartMap Map<String, RequestBody> params);

    @Multipart
    @POST("notice/notice_edit.php")
    Call<Board_Commits> Board_Edit(@PartMap Map<String, RequestBody> params);

    @Multipart
    @POST("recruit/recruit_write.php")
    Call<Recruit_boards> Recruit_write(@PartMap Map<String , RequestBody> params);

    @GET("recruit/recruit_view.php")
    Call<Recruit_boards>Recruit_View(@Query("board_name")String board_name,@Query("idx")int idx);

    @GET("member/favorite_list.php")
    Call<Favor_result>Favorite_list(@QueryMap Map<String ,String> params);

    @FormUrlEncoded
    @POST("recruit/recruit_apply.php")
    Call<Recruit_Apply> Recruit_Apply(@FieldMap Map<String,String> param);

    @FormUrlEncoded
    @POST("recruit/recruit_apply_list.php")
    Call<Apply_list> Recruit_ApplyList(@FieldMap Map<String,String> param);

    @GET("contest/contest_list.php")
    Call<ContestInfotListResult> getContestInfoListResult(@Query("board_name") String board_name
            , @Query("cur") int cur);
    @GET("contest/contest_view.php")
    Call<ContestInfoViewResult> getContestInfoViewResult(@Query("board_name") String board_name
    , @Query("idx") int idx);

    @FormUrlEncoded
    @POST("recruit/recruit_apply_member.php")
    Call<Apply_create> Create_Team(@FieldMap Map<String ,String> map);

    @FormUrlEncoded
    @POST("member/my_article.php")
    Call<Board_Recruit_Results> Myboard_List( @Field("unique_id") String unique_id, @Field("mb_id") String mb_id);


    @FormUrlEncoded
    @POST("recruit/recruit_delete.php")
    Call<Default_result> Myboard_Delete(@FieldMap Map<String,String> map);

    @Multipart
    @POST("recruit/recruit_edit.php")
    Call<Recruit_boards> Recruit_Edit(@PartMap Map<String , RequestBody> params ,@Part("is_delete") boolean is_delete);


    @FormUrlEncoded
    @POST("member/profile_view.php")
    Call<Profile_View> Profile_View(@FieldMap Map<String,String> map);


    @Multipart
    @POST("member/profile_edit.php")
    Call<Default_result> Profile_Edit(@PartMap Map<String,RequestBody> map);

    @FormUrlEncoded
    @POST("member/set_token.php")
    Call<Default_result> SetToken(@FieldMap Map<String,String> map);


    @GET("search/search.php")
    Call<Search_Results> GetSearchResult(@Query("type") String type,@Query("keyword") String keyword);

    @FormUrlEncoded
    @POST("team/set_team.php")
    Call<Default_result> Create_Collabo(@FieldMap Map<String,String> map);

    @GET("lastest/lastest.php")
    Call<ContestList_BannerLatest> BannerItems(@Query("lastest") String trues);


    @FormUrlEncoded
    @POST("team/get_team.php")
    Call<TeamListitems> getTeamList(@Field("mb_id") String id,@Field("unique_id") String uniq);

}
