package com.ration.dohee.collabo.MENU.ContestInfoBoard;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.ration.dohee.collabo.DATA.ActionBarData;
import com.ration.dohee.collabo.Https.Favorite_info;
import com.ration.dohee.collabo.Https.HttpContestInfo.ContestInfoList;
import com.ration.dohee.collabo.Https.HttpContestInfo.ContestInfotListResult;
import com.ration.dohee.collabo.Https.HttpServiceClient;
import com.ration.dohee.collabo.MENU.ContestInfoBoard.Util.ContestInfoAdapter;
import com.ration.dohee.collabo.MENU.ContestInfoBoard.Util.ContestInfoItem;
import com.ration.dohee.collabo.R;
import com.ration.dohee.collabo.Util.ActionBar;

import java.util.ArrayList;

/**
 * Created by User on 2017-09-03.
 */

public class ContestInfoListActivity extends ActionBar implements AdapterView.OnItemClickListener,AdapterView.OnItemSelectedListener{

    Spinner mSpinner;
    ContestInfoAdapter mContestInfoAdapter;
    ListView contestInfoListView;
    boolean mLockListView=false;
    int idx=0;
    AdView mAdView;
    ArrayList<Favorite_info> favors;
    ArrayList<ContestInfoList> result,resultall;
    ArrayList<ContestInfoItem> mItem;


    final static String[] SpinnerItem={"전체","문학/수기","캐릭터/만화","기획/아이디어","과학/공학", "게임/소프트웨어",
            "광고/마케팅", "건축/건설/인테리어", "영상/사진", "전시/페스티벌", "UCC"};

    @Override
    protected void onStart() {
        super.onStart();
        SetActionBar(findViewById(ActionBarData.ActionBar));
        idx=0;

        mContestInfoAdapter.items.clear();
        mItem.clear();
        mContestInfoAdapter.notifyDataSetChanged();
        Retro();
        contestInfoListView.setOnItemClickListener(this);
        mSpinner.setSelection(0);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contest_info_list);
        mSpinner=(Spinner)findViewById(R.id.spinner);


        resultall=new ArrayList<>();
        result=new ArrayList<>();
        ArrayAdapter<CharSequence> adapter=ArrayAdapter.createFromResource(
                this,R.array.spinnerlist,R.layout.spinner_normal);
        adapter.setDropDownViewResource(R.layout.spinner_normal);
        mSpinner.setAdapter(adapter);
        mSpinner.setOnItemSelectedListener(this);
        mContestInfoAdapter=new ContestInfoAdapter();
//        admob
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice("01D06A96016428712381997520D4CF5A").build();
        mAdView.loadAd(adRequest);
        mAdView.setAdListener(new AdListener(){
            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                Log.e("Banner Ad Error","Error Code: "+i);
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
                Log.e("Banner Ad Succes","Successsss");
            }
        });
        mContestInfoAdapter=new ContestInfoAdapter();
        contestInfoListView=(ListView)findViewById(R.id.contestInfo_list);
        contestInfoListView.setAdapter(mContestInfoAdapter);

        mItem=new ArrayList<>();
    }



    public void Retro() {
       new ContestAsync(this).execute();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent=new Intent(this,ContestInfoViewActivity.class);
        ContestInfoItem ref=mContestInfoAdapter.items.get(position);
        intent.putExtra("idx",ref.idx);
        intent.putExtra("isfavor",ref.isfavor);
        startActivity(intent);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if(!mLockListView) {
            mLockListView = true;
            return;
        }

        mContestInfoAdapter.items.clear();
        mContestInfoAdapter.notifyDataSetChanged();
        mItem.clear();
        if(position==0){
            getItemResult(position);
        }
        else if(position>0){
            getItemResult(position);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void getItemResult(int categoryPos){



        for(int i=0;i<resultall.size();i++){
            ContestInfoList res=resultall.get(i);

            Log.e("Cat","cat "+res.getCategory()+" / "+SpinnerItem[categoryPos].toString()+"  number : "+categoryPos);


            if(res.getCategory().equals(SpinnerItem[categoryPos].toString())&&categoryPos!=0){
                mItem.add(new ContestInfoItem(res.getWr_file(),res.getWr_subject(),
                        res.getCategory(),res.getStart_date(),res.getResv_date(),res.getIdx()));

            }
            else if(categoryPos==0)
            {
                mItem.add(new ContestInfoItem(res.getWr_file(),res.getWr_subject(),
                        res.getCategory(),res.getStart_date(),res.getResv_date(),res.getIdx()));
            }
        }


        mContestInfoAdapter.items.addAll(mItem);
        mContestInfoAdapter.notifyDataSetChanged();
    }








    class ContestAsync extends AsyncTask<Void,Void,Boolean>{

        int idx=0;
        int MaxPage;
        ProgressDialog dlg;

        Context context;
        public ContestAsync(Context _context) {
            super();
            idx=0;
            MaxPage=0;
            context=_context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dlg=new ProgressDialog(context);
            dlg.show();
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            dlg.dismiss();
            mContestInfoAdapter.items.addAll(mItem);

            Log.e("onPostExecute", "onPostExecute: "+mItem.size()+"  size  " +mContestInfoAdapter.items.size() );
            mContestInfoAdapter.notifyDataSetChanged();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {


            mItem.clear();
            result.clear();
            resultall.clear();
            do{
                try{
                    ContestInfotListResult body=HttpServiceClient.retrosListResult("contest_list",idx).execute().body();

                    if(body.getError().equals("false"))
                    {


                        MaxPage=body.getPage();

                        result=body.getResult();
                        resultall.addAll(result);
                        for(int i=0;i<result.size();i++){
                            ContestInfoList res=result.get(i);
                            mItem.add(new ContestInfoItem(res.getWr_file(),res.getWr_subject(),
                                    res.getCategory(),res.getStart_date().substring(2,10),res.getResv_date().substring(2,10),res.getIdx()));
                        }
                    }
                    else{
                        return false;
                    }

                }catch (Exception e){

                    return false;
                }
                finally {
                    idx++;
                }

            }while(idx<MaxPage);


            return true;

        }
    }
}



