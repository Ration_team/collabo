package com.ration.dohee.collabo.Https;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samsung on 2017-08-08.
 */

public class Recruit_board {

    @SerializedName("idx")
    int idx;
    @SerializedName("wr_subject")
    String wr_subject;
    @SerializedName("recruit_start_date")
    String recruit_start_date;
    @SerializedName("recruit_finish_date")
    String recruit_finish_date;
    @SerializedName("max_num")
    String max_num;
    @SerializedName("wr_content")
    String wr_content;
    @SerializedName("mb_id")
    String mb_id;
    @SerializedName("kakaotalk")
    String kakaotalk;
    @SerializedName("part")
    String part;
    @SerializedName("wr_file")
    String wr_file;
    @SerializedName("link")
    String link;
    @SerializedName("wr_date")
    String wr_date;
    @SerializedName("hit_count")
    String hit_count;
    @SerializedName("mb_img")
    String mb_img;
    int favid;


    public String getLink() {
        return link;
    }

    public String getMb_img() {
        return mb_img;
    }

    public int getIdx() {
        return idx;
    }

    public void setIdx(int idx) {
        this.idx = idx;
    }

    public String getWr_subject() {
        return wr_subject;
    }

    public void setWr_subject(String wr_subject) {
        this.wr_subject = wr_subject;
    }


    public String getRecruit_start_date() {
        return recruit_start_date;
    }

    public void setRecruit_start_date(String recruit_start_date) {
        this.recruit_start_date = recruit_start_date;
    }

    public String getRecruit_finish_date() {
        return recruit_finish_date;
    }

    public void setRecruit_finish_date(String recruit_finish_date) {
        this.recruit_finish_date = recruit_finish_date;
    }

    public String getMax_num() {
        return max_num;
    }

    public void setMax_num(String max_num) {
        this.max_num = max_num;
    }

    public String getWr_content() {
        return wr_content;
    }

    public void setWr_content(String wr_content) {
        this.wr_content = wr_content;
    }

    public String getMb_id() {
        return mb_id;
    }

    public void setMb_id(String mb_id) {
        this.mb_id = mb_id;
    }

    public String getKakaotalk() {
        return kakaotalk;
    }

    public void setKakaotalk(String kakaotalk) {
        this.kakaotalk = kakaotalk;
    }

    public String getPart() {
        return part;
    }

    public void setPart(String part) {
        this.part = part;
    }

    public String getWr_file() {
        return wr_file;
    }

    public void setWr_file(String wr_file) {
        this.wr_file = wr_file;
    }

    public String getWr_date() {
        return wr_date;
    }

    public void setWr_date(String wr_date) {
        this.wr_date = wr_date;
    }

    public String getHit_count() {
        return hit_count;
    }

    public void setHit_count(String hit_count) {
        this.hit_count = hit_count;
    }
}
