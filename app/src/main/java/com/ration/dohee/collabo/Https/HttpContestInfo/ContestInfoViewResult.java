package com.ration.dohee.collabo.Https.HttpContestInfo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by User on 2017-09-10.
 */

public class ContestInfoViewResult {
    @SerializedName("error")
    String error;
    @SerializedName("result")
    ContestInfoView result;
    @SerializedName("message")
    String msg;

    public String getMsg() {
        return msg;
    }

    public String getError() {
        return error;
    }

    public ContestInfoView getResult() {
        return result;
    }
}
