package com.ration.dohee.collabo.Https.HttpContestInfo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by User on 2017-09-10.
 */

public class ContestInfoView {
    @SerializedName("idx")
    int idx;
    @SerializedName("wr_subject")
    String wr_subject;
    @SerializedName("topic")
    String topic;
    @SerializedName("category")
    String category;
    @SerializedName("qualification")
    String qualification;
    @SerializedName("host")
    String host;
    @SerializedName("supervision")
    String supervision;
    @SerializedName("start_date")
    String start_date;
    @SerializedName("resv_date")
    String resv_date;
    @SerializedName("prize")
    String prize;
    @SerializedName("home_page")
    String link;
    @SerializedName("resv_method")
    String resv_method;
    @SerializedName("reference")
    String reference;
    @SerializedName("inquiry")
    String inquiry;
    @SerializedName("wr_date")
    String wr_date;
    @SerializedName("mb_id")
    String mb_id;
    @SerializedName("hit_count")
    int hit_count;
    @SerializedName("wr_file")
    String wr_file;

    public int getIdx() {
        return idx;
    }

    public String getWr_subject() {
        return wr_subject;
    }

    public String getTopic() {
        return topic;
    }

    public String getCategory() {
        return category;
    }

    public String getQualification() {
        return qualification;
    }

    public String getHost() {
        return host;
    }

    public String getSupervision() {
        return supervision;
    }

    public String getStart_date() {
        return start_date;
    }

    public String getResv_date() {
        return resv_date;
    }

    public String getPrize() {
        return prize;
    }

    public String getLink() {
        return link;
    }

    public String getResv_method() {
        return resv_method;
    }

    public String getReference() {
        return reference;
    }

    public String getInquiry() {
        return inquiry;
    }

    public String getWr_date() {
        return wr_date;
    }

    public String getWr_file() {
        return wr_file;
    }

    public String getMb_id() {
        return mb_id;
    }

    public int getHit_count() {
        return hit_count;
    }
}
