package com.ration.dohee.collabo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.ration.dohee.collabo.DATA.ActionBarData;
import com.ration.dohee.collabo.Https.Board_Recruit_Result;
import com.ration.dohee.collabo.Https.HttpContestInfo.ContestInfoList;
import com.ration.dohee.collabo.Https.HttpServiceClient;
import com.ration.dohee.collabo.Https.Search_Results;
import com.ration.dohee.collabo.MENU.ContestInfoBoard.ContestInfoViewActivity;
import com.ration.dohee.collabo.MENU.ContestInfoBoard.Util.ContestInfoAdapter;
import com.ration.dohee.collabo.MENU.ContestInfoBoard.Util.ContestInfoItem;
import com.ration.dohee.collabo.PROJECT_BOARD.IT_CustomAdapterListView;
import com.ration.dohee.collabo.PROJECT_BOARD.IT_ListViewItem;
import com.ration.dohee.collabo.PROJECT_BOARD.project_seleceted;
import com.ration.dohee.collabo.Util.ActionBar;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import lib.kingja.switchbutton.SwitchMultiButton;

public class SearchActivity extends ActionBar {
    Handler handler;
    int idx = 0;
    Search_Results results;
    SwitchMultiButton s;
    ListView search_list;

    Button searchBtn;
    @BindView(R.id.Search_message_tv)
    TextView SearchMessageTv;
    private List<String> TextList;
    LinkedList<ContestInfoList> contest;
    LinkedList<Board_Recruit_Result> recruit;
    String message;
    String type;
    EditText search_Et;
    ProgressDialog dlg;
    public static int Recruit_Search = 201;
    public static int Contest_Search = 202;
    public static int No_Search = 404;
    public static int underByte = 303;
    IT_CustomAdapterListView IT_Adapter;
    ContestInfoAdapter Con_Adapter;

    @Override
    protected void onStart() {
        super.onStart();
        SetActionBar(findViewById(ActionBarData.ActionBar));


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);
        type="recruit";
        searchBtn=(Button)findViewById(R.id.Search_execute_btn);
        search_Et = (EditText) findViewById(R.id.search_Et);
        TextList = Arrays.asList("매칭게시판", "공모전정보");
        search_list = (ListView) findViewById(R.id.search_list);
        s = (SwitchMultiButton) findViewById(R.id.search_switchbtn);
        s.setText(TextList);
        s.setOnSwitchListener(new SwitchMultiButton.OnSwitchListener() {
            @Override
            public void onSwitch(int position, String tabText) {
                type=tabText.equals("매칭게시판")?"recruit":"contest";
            }
        });

        contest = new LinkedList<>();
        recruit = new LinkedList<>();
        IT_Adapter = new IT_CustomAdapterListView("");
        Con_Adapter = new ContestInfoAdapter();

        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Retro_Thread threads=new Retro_Thread();
                threads.choice=type.equals("recruit")?201:202;
                threads.keyword=search_Et.getText().toString();

                dlg=new ProgressDialog(SearchActivity.this);
                dlg.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                dlg.show();
                threads.start();
            }
        });

        handler = new Handler() {


            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                dlg.dismiss();
                IT_Adapter.items.clear();
                Con_Adapter.items.clear();
                if (msg.arg1 == 1) {
                    switch (results.state) {
                        case 201: // recruit result
                            for (int i = 0; i < recruit.size(); i++) {
                                Board_Recruit_Result res = recruit.get(i);
                                IT_Adapter.items.add(new IT_ListViewItem(res.getWr_file(), res.getWr_subject(),
                                        res.getMb_id(), res.getWr_date(), res.getNum(), res.getIdx(),res.getBoard_name()));
                                SettingList();
                            }

                            break;
                        case 202: // contest result
                            for (int i = 0; i < contest.size(); i++) {
                                ContestInfoList res = contest.get(i);
                                Con_Adapter.items.add(new ContestInfoItem(res.getWr_file(), res.getWr_subject(),
                                        res.getCategory(), res.getStart_date(), res.getResv_date(), res.getIdx()));
                                SettingList();
                            }
                            break;
                        case 303: // under bytes
                        case 404: //  Not Found Boards
                            SearchMessageTv.setVisibility(View.VISIBLE);
                            search_list.setVisibility(View.GONE);
                            SearchMessageTv.setText(message);

                    }
                } else if (msg.arg1 == -1) {
                    Toaster("서버와의 연결이 불안정합니다");
                }
            }
        };


        search_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if(search_list.getAdapter()==Con_Adapter)
                {
                    Intent intent=new Intent(getApplicationContext(),ContestInfoViewActivity.class);
                    ContestInfoItem ref=Con_Adapter.items.get(position);
                    intent.putExtra("idx",ref.idx);
                    intent.putExtra("isfavor",ref.isfavor);
                    startActivity(intent);
                }
                else if(search_list.getAdapter()==IT_Adapter){
                    Intent inte = new Intent(getApplicationContext(), project_seleceted.class);
                    IT_ListViewItem ref = IT_Adapter.items.get(position);
                    inte.putExtra("idx", ref.getIdx());
                    inte.putExtra("isfavor", ref.isfavor);
                    inte.putExtra("board_name",ref.board_name);
                    startActivity(inte);
                }
            }
        });
    }

    private void SettingList() {
        search_list.setVisibility(View.VISIBLE);
        SearchMessageTv.setVisibility(View.GONE);
        if (results.state == Recruit_Search) {
            search_list.setAdapter(IT_Adapter);
        } else {
            search_list.setAdapter(Con_Adapter);
        }
    }

    class Retro_Thread extends Thread {


        public String keyword;
        public int choice;

        @Override
        public void run() {
            super.run();

            Message msg = new Message();
            if (Recruit_Search == choice) {
                try {
                    results = HttpServiceClient.SearchResult("recruit", keyword).execute().body();
                    if (results.getError().equals("false")) {
                        if (results.getMsg() == null) {
                            // Recruit 결과 수신
                            results.state = Recruit_Search;
                            recruit.clear();
                            recruit.addAll(results.getRecruit());
                        } else {
                            results.state = No_Search;
                            message = results.getMsg();
                        }
                    } else {
                        message = results.getMsg();
                        results.state = underByte;
                    }

                } catch (Exception e) {
                    msg.arg1 = -1;
                    handler.sendMessage(msg);
                    Log.e("SEARCH ERROR",e.getMessage());return;
                }
            } else if (Contest_Search == choice) {
                try {
                    results = HttpServiceClient.SearchResult("contest", keyword).execute().body();
                    if (results.getError().equals("false")) {
                        if (results.getMsg() == null) {
                            // Recruit 결과 수신
                            results.state = Contest_Search;
                            contest.clear();
                            contest.addAll(results.getContest());
                        } else {
                            results.state = No_Search;
                            message = results.getMsg();
                        }
                    } else {
                        message = results.getMsg();
                        results.state = underByte;
                    }

                } catch (Exception e) {
                    msg.arg1 = -1;
                    handler.sendMessage(msg);
                    Log.e("SEARCH ERROR",e.getMessage());return;
                }
            }
            msg.arg1 = 1;
            handler.sendMessage(msg);
        }
    }
}

