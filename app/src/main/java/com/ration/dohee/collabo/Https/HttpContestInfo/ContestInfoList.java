package com.ration.dohee.collabo.Https.HttpContestInfo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by User on 2017-08-19.
 */

public class ContestInfoList {
    @SerializedName("idx")
    int idx;
    @SerializedName("start_date")
    String start_date;
    @SerializedName("category")
    String category;
    @SerializedName("wr_subject")
    String wr_subject;
    @SerializedName("resv_date")
    String resv_date;
    @SerializedName("hit_count")
    int hit_count;
    @SerializedName("wr_file")
    String wr_file;

    public int getIdx() {
        return idx;
    }

    public String getCategory() {return category;}

    public String getStart_date() {return start_date;}

    public String getWr_subject() {
        return wr_subject;
    }

    public String getResv_date() {
        return resv_date;
    }

    public int getHit_count() {
        return hit_count;
    }

    public String getWr_file() {
        return wr_file;
    }
}
