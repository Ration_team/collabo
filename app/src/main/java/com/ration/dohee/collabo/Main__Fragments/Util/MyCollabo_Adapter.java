package com.ration.dohee.collabo.Main__Fragments.Util;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ration.dohee.collabo.Https.TeamListitem;
import com.ration.dohee.collabo.R;

import java.util.ArrayList;

/**
 * Created by Samsung on 2017-10-25.
 */

public class MyCollabo_Adapter extends BaseAdapter {
    public ArrayList<TeamListitem> items=new ArrayList<>();
    static int img_count=0;

    Integer []imgs={R.drawable.mycollabo_test0,R.drawable.mycollabo_test1,
            R.drawable.mycollabo_test2,R.drawable.mycollabo_test3,R.drawable.mycollabo_test4
    ,R.drawable.mycollabo_test5,R.drawable.mycollabo_test6};
    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Context context=parent.getContext();
        Viewholder viewHolder=new Viewholder();
        if(convertView==null){
            LayoutInflater inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate(R.layout.collabo_team_item,parent,false);

            viewHolder.img=(ImageView) convertView.findViewById(R.id.mycollabo_back_imageview);
            viewHolder.title=(TextView)convertView.findViewById(R.id.mycollabo_teamname_tv);
            viewHolder.guest=(TextView)convertView.findViewById(R.id.mycollabo_guest_tv);
//            viewHolder.cimg=(CircleImageView)convertView.findViewById(R.id.myboard_imageview);
//            viewHolder. devider=(TextView)convertView.findViewById(R.id.myboard_devider);
            convertView.setTag(viewHolder);
        }
        else{
            viewHolder=(Viewholder)convertView.getTag();
        }

        Log.e("MyCollabo title",viewHolder.title==null?"null":"notnull");
        Log.e("MyCollabo items",items.get(position)==null?"null":"notnull");
        viewHolder.title.setText(items.get(position).getTeam_name());
        viewHolder.guest.setText(items.get(position).getHost()+" "+items.get(position).getMember());
        Glide.with(parent.getContext()).load(imgs[position%7]).into(viewHolder.img);
        // TODO: 2017-10-26   image random 
        return convertView;
    }

    class Viewholder{
        RelativeLayout backgroud;
        TextView title,guest;
        ImageView img;



        public Viewholder() {
        }

        public Viewholder(View v) {

//            title=(TextView)v.findViewById(R.id.it_project_board_title_tv);
//            master=(TextView)v.findViewById(R.id.it_project_board_date_tv);
//            guest=(TextView)v.findViewById(R.id.it_project_board_num_tv);
//            img=(ImageView)v.findViewById(R.id.it_project_board_writer_tv);

        }
    }
}
