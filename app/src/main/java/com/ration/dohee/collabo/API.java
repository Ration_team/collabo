package com.ration.dohee.collabo;

/**
 * Created by jongsunchoi on 2017. 6. 16..
 */

public class API {

    // 오류 발생 시 조치사항
    //
    // svn update 후 "Installation fai led with message Failed to finalize session : INSTALL_FAILED_INVALID_APK:............"
    // 오류가 발생하면,
    // File -> Settings ->Build, Execution, Deployment ->Instant Run -> Enable Instant Run 어쩌구저쩌구 있는 거 체크 해제하면 실행됨....
    //
    //
    //
    //
/*
SharedPreferences "Auto_Login"

        "id",id
        "pass",pass
        "email",email
        "name",name
        "Login",true
// */
//<form method="GET" action="/id_check.php">
//		<input type="text" name="mb_id" value="sunni75">
//		<button id="submit">submit</button>
////	</form>
//<form method="POST" action="/board/board_edit.php" enctype="multipart/form-data">
//		<input type="text" name="board_name" value="board_notice">
//		<input type="text" name="mb_id" value="sunni75">
//		<input type="text" name="idx" value="29">
//		<input type="text" name="wr_subject" value="수정되냐?">
//		<input type="text" name="wr_content" value="크크크크">
//		<input type="text" name="mb_id" value="sunni75">
//		<input type="text" name="mb_name" value="최종선">
//		<input type="file" name="wr_file">
//		<input type="text" name="unique_id" value="uaRbNimSQWlVQTLoYtIWgI0UsYpat372RF3SnJS2">
//		<input type="checkbox" name="is_delete"> on/off <br>
//		<input type="submit" value="submit">
//	</form>
    // API 내용
//    1. 회원 가입
//        - 전송 : post
//        - 경로 : http://주소/register.php
//                - 필요 파라미터
            //        mb_id : 아이디
            //        mb_password : 비번
            //        mb_name : 이름
            //        mb_email : 이메일
            //        mb_tel : 전번
//
//    2. 로그인
//        - 전송 : post
//        - 경로 : http://주소/login.php
//                - 필요 파라미터
            //        mb_id : 아이디
            //        mb_password : 비밀번호
//                    * 로그인이 성공하면 unique_id를 받는다. 본인인증 세션 대용이다. unique_id는 로그인할때마다 바뀜
//                      따라서, 앱이 종료될때는 unique_id 값을 삭제해야하고, 앱이 시작되면 서버에 자동으로 로그인 폼 전송 및 unique_id 값을
//                      sharedpreference에 저장해서 사용하자.
//                      이 값은 쓰기 액션이 발생할 때 서버 보안용이다. 졸라 졸라 졸라 졸라 중요하다.
//
//    3. 게시물 리스트
//        - 전송 : get
//        - 경로 : http://주소/board/board_list.php
//                - 필요 파라미터
            //        board_name : 게시판 명
            //        cur : 현재 페이지
            //        - 예시 : http://주소/board/board_list.php?board_name=board_notice?cur=0 (공지사항의 0번째 페이지를 출력한다)
    //                      /// 페이징이 필요한 게시물은 한번에 5개씩 불러오게 설정되어 있다. 필요시 알려주삼...
            //
//    4. 게시물 검색
//        - 전송 : get
//        - 경로 : http://주소/board/board_search.php
//                - 필요 파라미터
            //        board_name : 게시판 명
            //        searchField : 검색 조건
            //        keyword : 검색어
            //        - 예시 : http://주소/board/board_list.php?board_name=board_notice?searchField=wr_subject?keyword=검색어 (공지사항의 제목이 '검색어' 인 게시물을 찾는다)
//
//    5. 게시물 작성
//        - 전송 : post
//        - 경로 : http://주소/board/board_write.php
//                - 필요 파라미터
            //        board_name : 게시판 명
            //        wr_subject : 제목
            //        wr_content : 내용
            //        mb_id : 아이디
            //        mb_name : 이름
            //        wr_file : 업로드 파일 (첨부파일이 없으면 생략 가능 // 최대 용량은 1메가)
            //        unique_id : mb_id의 unique_id (unique_id가 다르거나 누락되면 글쓰기 불가)
//
//    6. 게시물 보기
//        - 전송 : get
//        - 경로 : http://주소/board/board_view.php
//                - 필요 파라미터
            //        board_name : 게시판명
            //        idx : 게시물 번호
            //        - 예시 : http://주소/board/board_list.php?board_name=board_notice?idx=5 (공지사항의 5번 게시물을 본다)
//
//    7. 게시물 수정
//        - 전송 : post
//        - 경로 : http://주소/board/board_edit.php
//                - 필요 파라미터
            //        board_name : 게시판 명
            //        idx : 게시물 번호
            //        wr_subject : 제목
            //        wr_content : 내용
            //        mb_id : 아이디
            //        mb_name : 이름
            //        wr_file : 업로드 파일 (첨부파일이 없으면 생략 가능)
//

/*
        게시물 수정 <form method="POST" action="/board/board_edit.php">
		<input type="text" name="board_name" value="board_notice">
		<input type="text" name="mb_id" value="sunni75">
		<input type="text" name="idx" value="29">
		<input type="text" name="wr_subject" value="수정되냐?">
		<input type="text" name="wr_content" value="크크크크">
		<input type="text" name="mb_id" value="sunni75">
		<input type="text" name="mb_name" value="최종선">
		<input type="text" name="unique_id" value="uaRbNimSQWlVQTLoYtIWgI0UsYpat372RF3SnJS2">
		<input type="submit" value="submit">
	</form>
            */
//    8. 게시물 삭제
//        - 전송 : post
//        - 경로 : http://주소/board/board_delete.php
//                - 필요 파라미터
            //        board_name : 게시판 명
            //        idx : 게시물 번호
            //        mb_id : 아이디
            //        unique_id : uniq
//

//
//
//    ** JSON 구조
//        - 오류 여부 : error (true or false)
//        - 결과값 : result


    // 이미지 추출 시  사이즈 1mb 이하로
/*
    로그아웃 시 로직

    -UNIQ.unique_id  값 null 또는 "" 로 초기화
    -SharedPreference 에 저장된 이름 아이디 비번 이메일 번호  초기화 또는 삭제
    -SharedPreference 에 저장된 Login 값 false 로 바꿔주기
    */

/*
* 액션바 사용  issue
*
*       액션바  인클루드시  id값은 actionbar 로 통일함
*       ActionBarData  클래스 내 ActionBar 사용
* 메뉴 구현시 DrawerLayout  과 NavigationView id값 통일함
*       xml :
*       DrawerLayout    Id = union_navmenu_drawerlayout
*       NavigationView  Id = union_navmenu_drawerlayout_menu
*
*       JAVA :
*       ActionBarData 클래스 내 필드 사용
*       DrawerLayout    = DrawerLayout
*       NavigationView  = DrawerLayout_Navmenu
* */
}
