package com.ration.dohee.collabo.Https;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samsung on 2017-08-08.
 */

public class Board_Recruit_Result {
    @SerializedName("error")
    String error;
    @SerializedName("message")
    String msg;
    @SerializedName("idx")
    int idx;
    @SerializedName("mb_id")
    String mb_id;
    @SerializedName("wr_subject")
    String wr_subject;
    @SerializedName("wr_date")
    String wr_date;
    @SerializedName("max_num")
    int num;
    @SerializedName("hit_count")
    int hit_count;
    @SerializedName("mb_img")
    String wr_file;
    @SerializedName("board_name")
    String board_name;

    public String getImg() {
        return img;
    }

    @SerializedName("wr_file")
    String img

            ;

    public String getBoard_name() {
        return board_name;
    }

    public int getNum() {
        return num;
    }

    public String getWr_file() {
        return wr_file;
    }

    public String getMb_id() {
        return mb_id;
    }

    public String getError() {
        return error;
    }

    public String getMsg() {
        return msg;
    }

    public int getIdx() {
        return idx;
    }

    public String getWr_subject() {
        return wr_subject;
    }



    public String getWr_date() {
        return wr_date;
    }



    public int getHit_count() {
        return hit_count;
    }
}
