package com.ration.dohee.collabo.Util;

import android.app.Application;
import android.content.res.Configuration;

import com.bumptech.glide.Glide;
import com.tsengvn.typekit.Typekit;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

/**
 * Created by Samsung on 2017-06-02.
 */

public class BaseApplication extends Application {
    // 폰트 변경 클래스
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Glide.get(this).clearMemory();
    }


    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        Glide.get(this).trimMemory(level);
    }
        @Override
        public void onCreate() {
            super.onCreate();
            Fabric.with(this, new Crashlytics());
            Typekit.getInstance()
                    .addNormal(Typekit.createFromAsset(this, "NanumGothic.otf"))
                    .addBold(Typekit.createFromAsset(this, "NanumGothicBold.otf"));

        }
    }

