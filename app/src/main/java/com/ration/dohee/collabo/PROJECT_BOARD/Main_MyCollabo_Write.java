package com.ration.dohee.collabo.PROJECT_BOARD;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.ration.dohee.collabo.DATA.ActionBarData;
import com.ration.dohee.collabo.R;
import com.ration.dohee.collabo.Util.ActionBar;
import com.rengwuxian.materialedittext.MaterialEditText;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Main_MyCollabo_Write extends ActionBar {

    @BindView(R.id.drawer_indicator)
    ImageView drawerIndicator;
    @BindView(R.id.imageButton)
    ImageButton imageButton;
    @BindView(R.id.searchBtn)
    ImageButton searchBtn;
    @BindView(R.id.timboard_title_et1)
    EditText timboardTitleEt1;
    @BindView(R.id.timboard_write_Et)
    MaterialEditText timboardWriteEt;
    @BindView(R.id.timboard_uplode_Btn)
    Button timboardUplodeBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main__my_collabo__write);
        ButterKnife.bind(this);
        SetActionBar((View) findViewById(ActionBarData.ActionBar));

    }
}
