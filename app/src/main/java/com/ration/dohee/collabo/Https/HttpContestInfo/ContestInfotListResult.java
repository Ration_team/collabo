package com.ration.dohee.collabo.Https.HttpContestInfo;

import com.ration.dohee.collabo.Https.Favorite_info;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by User on 2017-08-19.
 */

public class ContestInfotListResult {
    @SerializedName("error")
    String error;

    @SerializedName("result")
    ArrayList<ContestInfoList> result;

    @SerializedName("favorite_list")
    ArrayList<Favorite_info> favors;

    @SerializedName("total_count")
    int total_count;

    @SerializedName("page_count")
    int Page;

    public int getPage() {
        return Page;
    }

    public int getTotal_count() {
        return total_count;
    }

    public String getError() {
        return error;
    }

    public ArrayList<ContestInfoList> getResult() {
        return result;
    }

    public ArrayList<Favorite_info> getFavors() {
        return favors;
    }
}
