package com.ration.dohee.collabo.Https;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Samsung on 2017-10-26.
 */

public class TeamListitems {
    @SerializedName("error")
    String error;
    @SerializedName("result")
    ArrayList<TeamListitem> result;

    public String getError() {
        return error;
    }

    public ArrayList<TeamListitem> getResult() {
        return result;
    }
}
