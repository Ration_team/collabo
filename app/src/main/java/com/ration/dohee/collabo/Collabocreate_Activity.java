package com.ration.dohee.collabo;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.ration.dohee.collabo.DATA.User_INFO;
import com.ration.dohee.collabo.Https.HttpContestInfo.Default_result;
import com.ration.dohee.collabo.Https.HttpServiceClient;
import com.ration.dohee.collabo.Util.CustomActivityForFonts;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;

import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Collabocreate_Activity extends CustomActivityForFonts {
    ImageButton prev, next;
    ViewFlipper vf;
    View create1, create2, create3, create4, create5;
    Animation slide_in, slide_out, prev_slide_in, prev_slide_out;
    EditText collabo_create_name, collabo_create_objective, collabo_create_rull;
    TextView collabo_create_startDate, collabo_create_andDate, collabo_create_nameView;
    Button collabo_create_pass_btn, collabo_create_fail_btn, collabo_create_rull_add_btn;
    String STARTDATE, ENDDATE;
    String TeamName,Approach,start,end;
    ArrayList<String> rulls;
    LinearLayout create_rull_add_L;
    public static Editable create_name;
    public static boolean collabo_create_result=false;
    int year, month, day;
    int num = 1;

    private DatePickerDialog.OnDateSetListener dateSetListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collabo_create_manager);
        ButterKnife.bind(this);
        rulls=new ArrayList<>();
        slide_in = AnimationUtils.loadAnimation(this, R.anim.slide_in_right);
        slide_out = AnimationUtils.loadAnimation(this, R.anim.slide_out_left);
        prev_slide_in = AnimationUtils.loadAnimation(this, R.anim.slide_in_right_prev);
        prev_slide_out = AnimationUtils.loadAnimation(this, R.anim.slide_out_left_prev);
        vf = ((ViewFlipper) findViewById(R.id.collabo_create_manager_viewflipper));

        initViewFlipper();

        initViews();


    }

    public void initViews() {
        create1 = (View) findViewById(R.id.collabo_create1);
        create2 = (View) findViewById(R.id.collabo_create2);
        create3 = (View) findViewById(R.id.collabo_create3);
        create4 = (View) findViewById(R.id.collabo_create4);
        create5 = (View) findViewById(R.id.collabo_create5);
        create01init(create1);
        create02init(create2);
        create03init(create3);
        create04init(create4);
        create05init(create5);

    }


    public void create01init(View v) {
        collabo_create_name = (EditText) v.findViewById(R.id.collabo_create_name);
        create_name = collabo_create_name.getText();
        collabo_create_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()==0)
                {
                    TeamName=null;
                }
                TeamName=s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void create02init(View v) {
        collabo_create_objective = (EditText) v.findViewById(R.id.collabo_create_objective);
        collabo_create_objective.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()==0)
                {
                    Approach=null;
                }
                Approach=s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void create03init(View v) {

        collabo_create_rull = (EditText) v.findViewById(R.id.collabo_create_rull);
        collabo_create_rull_add_btn = (Button) v.findViewById(R.id.collabo_create_rull_add_btn);
        create_rull_add_L = (LinearLayout) v.findViewById(R.id.collabo_create_rull_add_L);
        collabo_create_rull.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                rulls.add(0,s.toString());

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        num=1;
        collabo_create_rull_add_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(num<=4){
                    num++;
                    LayoutInflater inflater =  (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    LinearLayout addLinear = null;

                    addLinear  = (LinearLayout) inflater.inflate(R.layout.activity_collabo_create3_add, null);

                    TextView rull_num= (TextView) addLinear.findViewById(R.id.collabo_create_rull_num2);
                    EditText rull= (EditText) addLinear.findViewById(R.id.collabo_create_rull2);

                    rull.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            if(s.length()==0)
                            {
                                rulls.add(num,null);
                            }
                            if(rulls.get(num)!=null)
                                rulls.remove(num);

                            rulls.add(num,s.toString());
                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    });

                    rull_num.setText(num+".");


                    create_rull_add_L.addView(addLinear);

                } else {
                    Toast.makeText(getApplication(), "규칙은 5개만 만들 수 있습니다.", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    public void create04init(View v) {
        long now = System.currentTimeMillis();
        Date date = new Date(now);
        SimpleDateFormat sdfNow = new SimpleDateFormat("yyyy년 MM월 dd일");
        STARTDATE = sdfNow.format(date);
        SimpleDateFormat sdfNow2 = new SimpleDateFormat("yyyy-MM-dd");
        start= sdfNow2.format(date);

        collabo_create_startDate = (TextView) v.findViewById(R.id.collabo_create_startDate);
        collabo_create_andDate = (TextView) v.findViewById(R.id.collabo_create_andDate);
        collabo_create_startDate.setText(STARTDATE);
        collabo_create_andDate.setText(Html.fromHtml("<u>" + STARTDATE + "</u>"));

        GregorianCalendar calendar = new GregorianCalendar();

        year = calendar.get(Calendar.YEAR);

        month = calendar.get(Calendar.MONTH);

        day = calendar.get(Calendar.DAY_OF_MONTH);
        dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                String msg = String.format("%d년 %02d월 %02d일", year, monthOfYear + 1, dayOfMonth);
                collabo_create_andDate.setText(msg);
                ENDDATE = msg;
                end = String.format("%d-%02d-%02d", year, monthOfYear + 1, dayOfMonth);

            }
        };
        collabo_create_andDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(Collabocreate_Activity.this, dateSetListener, year, month, day).show();
            }
        });
    }

    public void create05init(View v) {
        collabo_create_nameView = (TextView) v.findViewById(R.id.collabo_create_nameView);
        collabo_create_pass_btn = (Button) v.findViewById(R.id.collabo_create_pass_btn);
        collabo_create_fail_btn = (Button) v.findViewById(R.id.collabo_create_fail_btn);

        collabo_create_pass_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String rullstring="";
                if(TeamName!=null&&Approach!=null&&getIntent().getStringExtra("member")!=null
                &&rulls.size()!=0&&end!=null) {
                    for (int i=0;i<rulls.size();i++)
                    {
                        rullstring+=rulls.get(i);
                    }
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("name", TeamName);
                    map.put("master", User_INFO.getID());
                    map.put("member", getIntent().getStringExtra("member"));
                    map.put("goal", Approach);
                    map.put("rule", rullstring);
                    map.put("start_date", start);
                    map.put("end_date", end);

                    HttpServiceClient.Create_Collabo(map).enqueue(new Callback<Default_result>() {
                        @Override
                        public void onResponse(Call<Default_result> call, Response<Default_result> response) {
                            if (response.isSuccessful())
                            {
                                if(response.body().getError().equals("false"))
                                {
                                    if(response.body().getMessage().equals("팀 생성 완료")){
                                        Intent intent = new Intent(Collabocreate_Activity.this, MainActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        Toaster(response.body().getMessage()+"");
                                        startActivity(intent);
                                    }
                                    else if(response.body().getMessage().equals("팀 생성 실패")){
                                        Toaster("서버가 불안정합니다 잠시후 다시 시도해주세요");

                                    }
                                    else if(response.body().getMessage().equals("팀명 중복")){
                                        Toaster("팀 이름이 중복 되었습니다!");
                                    }
                                    else if(response.body().getMessage().equals("올바른 방법으로 이용 하세요.")){
                                    // unique_id  error
                                    }
                                    else if(response.body().getMessage().equals("올바른 방법으로 이용하세요.")){
                                        Toaster("누락된 값이 있습니다! (규칙 최소 한가지는 설정해주세요!)");
                                    }

                                }
                                else if(response.body().getError().equals("true"))
                                {
                                    Log.e("ServerError",response.body().getMessage());
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<Default_result> call, Throwable t) {

                        }
                    });


                }
            }
        });

        collabo_create_fail_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void initViewFlipper() {
        prev = (ImageButton) findViewById(R.id.collabo_create_viewflipper_prev_imgbtn);
        prev.setVisibility(View.INVISIBLE);
        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vf.setOutAnimation(prev_slide_out);
                vf.setInAnimation(prev_slide_in);
                vf.showPrevious();
                if (vf.getDisplayedChild() == 0) {
                    prev.setVisibility(View.INVISIBLE);
                }
                if (vf.getDisplayedChild() == 2) {
                    next.setVisibility(View.VISIBLE);
                }
            }
        });
        next = (ImageButton) findViewById(R.id.collabo_create_viewflipper_next_imgbtn);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vf.setOutAnimation(slide_out);
                vf.setInAnimation(slide_in);
                vf.showNext();
                if (vf.getDisplayedChild() == 1) {
                    prev.setVisibility(View.VISIBLE);
                }
                if (vf.getDisplayedChild() == 4) {
                    next.setVisibility(View.INVISIBLE);
                }
            }
        });
    }
}
