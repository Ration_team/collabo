package com.ration.dohee.collabo.Main__Fragments.Util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ration.dohee.collabo.DATA.DATA;
import com.ration.dohee.collabo.Https.Board_Recruit_Result;
import com.ration.dohee.collabo.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Samsung on 2017-10-11.
 */

public class Myboard_lists  extends BaseAdapter{

    public ArrayList<Board_Recruit_Result> items;

    public Myboard_lists() {
        this.items = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Context context=parent.getContext();
        Viewholder viewHolder=new Viewholder();
        if(convertView==null){
            LayoutInflater inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate(R.layout.my_board_listview,parent,false);

            viewHolder.title=(TextView)convertView.findViewById(R.id.myboard_title_tv);
            viewHolder.date=(TextView)convertView.findViewById(R.id.myboard_date_tv);
            viewHolder.num=(TextView)convertView.findViewById(R.id.myboard_num_tv);
            viewHolder.writer=(TextView)convertView.findViewById(R.id.myboard_writer_tv);
            viewHolder.cimg=(CircleImageView)convertView.findViewById(R.id.myboard_imageview);
            viewHolder. devider=(TextView)convertView.findViewById(R.id.myboard_devider);
            convertView.setTag(viewHolder);
        }
        else{
            viewHolder=(Viewholder)convertView.getTag();
        }


        Board_Recruit_Result ref=items.get(position);


        viewHolder.title.setText(ref.getWr_subject());
        viewHolder.title.setSelected(true);
        viewHolder.date.setText(ref.getWr_date().substring(2,10));
        viewHolder.num.setText(ref.getNum()+"");
        viewHolder.writer.setText(ref.getMb_id());
        viewHolder.devider.setText("");
        Glide.with(parent.getContext()).load(DATA.ImgRootURL+ref.getImg()).into(viewHolder.cimg);

        return convertView;
    }

    class Viewholder{
        TextView title,writer,date,num,devider;
        CircleImageView cimg;



        public Viewholder() {
        }

        public Viewholder(View v) {
            title=(TextView)v.findViewById(R.id.it_project_board_title_tv);
            date=(TextView)v.findViewById(R.id.it_project_board_date_tv);
            num=(TextView)v.findViewById(R.id.it_project_board_num_tv);
            writer=(TextView)v.findViewById(R.id.it_project_board_writer_tv);
            cimg=(CircleImageView)v.findViewById(R.id.it_project_board_imageview);
            devider=(TextView)v.findViewById(R.id.myboard_devider);

        }
    }
}
