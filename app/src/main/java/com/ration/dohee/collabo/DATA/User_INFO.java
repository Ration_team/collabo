package com.ration.dohee.collabo.DATA;

import android.content.SharedPreferences;

import com.ration.dohee.collabo.Https.Favorite_info;

import java.util.ArrayList;

/**
 * Created by Samsung on 2017-07-17.
 */

public class User_INFO {
    private static String ID="ref";
    private static String PASS="ref";
    private static ArrayList<Favorite_info> RecruitFavor=new ArrayList<>();
    private static ArrayList<Favorite_info> ContestFavor=new ArrayList<>();
    private static ArrayList<Favorite_info> ConInfoFavor=new ArrayList<>();

    private static String EMAIL="";
    public static void Logout()
    {
        ID=PASS=EMAIL="";RecruitFavor=ConInfoFavor=ContestFavor=null;
    }
    public static void UserSetting(SharedPreferences sp){
        ID=sp.getString("id","ref");
        PASS=sp.getString("pass","ref");
        EMAIL=sp.getString("email","ref");


    }
    public static void UserSetter(String id, String pass, String email, SharedPreferences sp)
    {
        if(id.equals(""))
        {
            Logout();
            sp.edit().clear().apply();
            return;
        }
        ID=id;PASS=pass;EMAIL=email;
        SharedPreferences.Editor editor=sp.edit();
        editor.putString("id",id);
        editor.putString("pass",pass);
        editor.putString("email",email);

        editor.commit();
    }

    public static ArrayList<Favorite_info> getRecruitFavor() {
        if(RecruitFavor==null)
            RecruitFavor=new ArrayList<>();
        return RecruitFavor;
    }

    public static void setRecruitFavor(ArrayList<Favorite_info> recruitFavor) {
        if (RecruitFavor == null) {
            RecruitFavor=new ArrayList<>();
        }

        RecruitFavor = recruitFavor;
    }

    public static ArrayList<Favorite_info> getContestFavor() {
        return ContestFavor;
    }

    public static void setContestFavor(ArrayList<Favorite_info> contestFavor) {
        if (ContestFavor == null) {
            ContestFavor=new ArrayList<>();
        }
        ContestFavor = contestFavor;
    }

    public static ArrayList<Favorite_info> getConInfoFavor() {
        if (ContestFavor == null) {
            ContestFavor=new ArrayList<>();
        }
        return ConInfoFavor;
    }

    public static void setConInfoFavor(ArrayList<Favorite_info> conInfoFavor) {
        if (ConInfoFavor == null) {
            ConInfoFavor=new ArrayList<>();
        }
        ConInfoFavor = conInfoFavor;
    }

    public static void initFavorList(){
        RecruitFavor.clear();
        ConInfoFavor.clear();
        ContestFavor.clear();
    }

    public static String getID() {
        return ID;
    }

    public static void setID(String ID) {
        User_INFO.ID = ID;
    }

    public static String getPASS() {
        return PASS;
    }

    public static void setPASS(String PASS) {
        User_INFO.PASS = PASS;
    }


    public static ArrayList<Favorite_info> getfavors(String name) {

        return name.equals("board_recruit")?RecruitFavor:ContestFavor;
    }


    public static String getEMAIL() {
        return EMAIL;
    }

    public static void setEMAIL(String EMAIL) {
        User_INFO.EMAIL = EMAIL;
    }
}
