package com.ration.dohee.collabo.ListUtil;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ration.dohee.collabo.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class List_Adapter_BoardNotice extends BaseAdapter {
    public ArrayList<Notice_Board_item> arr=new ArrayList<>();




    @Override
    public int getCount() {

        return arr!=null?arr.size():0;
    }

    @Override
    public Object getItem(int position) {
        return arr.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView title,date;
        Context context = parent.getContext();
        Notice_Board_item ref=arr.get(position);
        if(convertView==null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.notice_list, parent, false);
        }

        title=((TextView)convertView.findViewById(R.id.board_notice_list_title));
        date=((TextView)convertView.findViewById(R.id.board_notice_list_date));
        Date to=new Date();

        try {
            String from = ref.getDate();

            SimpleDateFormat transFormat = new SimpleDateFormat("yyyy-MM-dd");

            to = transFormat.parse(from);
        }catch(Exception e)
        {

        }
        finally {
            title.setText(ref.getTitle());
            date.setText((to.getYear()+1900)+"-"+(to.getMonth()+1)+"-"+(to.getDay()+1));

        }

        return convertView;
    }
}

