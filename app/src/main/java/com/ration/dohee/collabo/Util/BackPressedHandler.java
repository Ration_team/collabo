package com.ration.dohee.collabo.Util;

import android.app.Activity;
import android.widget.Toast;

/**
 * Created by Samsung on 2017-06-19.
 */

public class BackPressedHandler {

    //  뒤로가기 눌렀을때 이벤트 처리 클래스
    //  activity에 BackPressedHandler객체 생성후 OnBackPressedClose 함수 호출  매개변수인
    //  idx 는 1입력시 이전 화면으로 돌아가게되고 그외값은 앱종료를 나타낸다
    //  생성자에 는 activity  즉  액티비티에서 this를 넣어주고 ref는 버튼클릭시 나타낼 문자열
    private long BackPressTimeInMillis=0;
    Toast toast;
    Activity activity;
    String ref;

    public BackPressedHandler(Activity activity,String ref) {
        this.activity = activity;
        this.ref=ref;
    }
    public void OnBackPressedClose(int idx){
        if(System.currentTimeMillis()>BackPressTimeInMillis+2000)
        {
            ShowGuide();
            BackPressTimeInMillis=System.currentTimeMillis();
            return ;
        }
        if(System.currentTimeMillis()<=BackPressTimeInMillis+2000)
        {
            toast.cancel();
            if(idx==1)
            activity.finish();
            else{
                activity.finishAffinity();
            }
        }
    }
    private void ShowGuide(){
        toast=Toast.makeText(activity,ref,Toast.LENGTH_LONG);
        toast.show();
    }
}
