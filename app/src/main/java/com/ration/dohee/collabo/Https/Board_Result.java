package com.ration.dohee.collabo.Https;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samsung on 2017-07-13.
 */

public class Board_Result {


        @SerializedName("hit_count")
        int hit_count;
        @SerializedName("wr_subject")
        String wr_subject;
        @SerializedName("wr_content")
        String wr_content;
        @SerializedName("mb_id")
        String mb_id;
        @SerializedName("wr_date")
        String wr_date;
        @SerializedName("wr_file")
        String wr_file;

    public String getMb_id() {
        return mb_id;
    }

    public int getHit_count() {
            return hit_count;
        }

        public String getWr_subject() {
            return wr_subject;
        }

        public String getWr_content() {
            return wr_content;
        }



        public String getWr_date() {
            return wr_date;
        }

        public String getWr_file() {
            return wr_file;
        }


}
