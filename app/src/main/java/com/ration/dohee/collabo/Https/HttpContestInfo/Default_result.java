package com.ration.dohee.collabo.Https.HttpContestInfo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Samsung on 2017-10-12.
 */

public class Default_result {
    @SerializedName("error")
    String error;
    @SerializedName("message")
    String message;

    public String getError() {
        return error;
    }

    public String getMessage() {
        return message;
    }
}
