package com.ration.dohee.collabo.TEST;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.ration.dohee.collabo.DATA.UNIQ;
import com.ration.dohee.collabo.MainActivity;
import com.ration.dohee.collabo.R;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

public class TEST_WRITE_ACTIVITY extends AppCompatActivity implements View.OnClickListener{
    MaterialEditText titleedt,contentsedt;
    Button submit;
    String TITLE,CONTENTS;
    String WRITE_INFO;
    SharedPreferences sharedPreferences;
    boolean Flags[]=new boolean[2];
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test__write__activity);
        sharedPreferences=getSharedPreferences("Auto_Login",MODE_PRIVATE);
        titleedt=(MaterialEditText)findViewById(R.id.test_title_edt);
        contentsedt=(MaterialEditText)findViewById(R.id.test_contents_edet);
        submit=(Button)findViewById(R.id.text_submit_btn);
        titleedt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String arr=s.toString();
                if(arr.length()<=0){
                    Flags[0]=false;
                }
                else{
                    Flags[0]=true;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        contentsedt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String arr=s.toString();
                if(arr.length()<=0){
                    Flags[1]=false;
                }
                else{
                    Flags[1]=true;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        submit.setOnClickListener(this);

    }


    boolean CheckFlags(){
        for(int i=0;i<Flags.length;i++){
            if(!Flags[i]){
                return false;
            }
            if(i==0)TITLE= titleedt.getText().toString();
            else if(i==1)CONTENTS=contentsedt.getText().toString();
        }
        return true;
    }
    @Override
    public void onClick(View v) {

        if(CheckFlags()){
            new SendPostRequest().execute();
        }
    }


    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while(itr.hasNext()){

            String key= itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }

    public class SendPostRequest extends AsyncTask<String, Void, String> {
        private final String ip1 = "http://10.107.3.81";
        private final String ip2 = "http://collabo.boasmedia.co.kr";
        String id,pass;
        protected void onPreExecute(){
            id=sharedPreferences.getString("id","");
        }

        protected String doInBackground(String... arg0) {
            try {
                URL url = new URL(ip2 + "/notice/notice_write.php");
                JSONObject postDataParams = new JSONObject();
                postDataParams.put("board_name","board_notice");
                postDataParams.put("wr_subject",TITLE);
                postDataParams.put("wr_content",CONTENTS);
                postDataParams.put("mb_id", id);
                postDataParams.put("wr_file",null);
                postDataParams.put("unique_id",UNIQ.unique_id+"");

                Log.e("params",postDataParams.toString());
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));
                writer.flush();
                writer.close();
                os.close();
                int responseCode=conn.getResponseCode();
                if (responseCode == HttpsURLConnection.HTTP_OK) {
                    BufferedReader in=new BufferedReader(new
                            InputStreamReader(
                            conn.getInputStream()));
                    StringBuffer sb = new StringBuffer("");
                    String line="";
                    while((line = in.readLine()) != null) {
                        sb.append(line);
                        break;
                    }
                    in.close();

                    return sb.toString();
                }
                else {
                    return new String("false : "+responseCode);
                }
            }
            catch(Exception e){

                return new String("Exception: " + e.getMessage());
            }

        }

        @Override
        protected void onPostExecute(String result) {
            WRITE_INFO=result;
            HttpComplete();
            Toast.makeText(getApplicationContext(), result,
                    Toast.LENGTH_LONG).show();
        }
    }

    void HttpComplete(){
        if(WRITE_INFO!=null){
            try{
                JSONObject ref=null;
                ref=new JSONObject(WRITE_INFO);
                String result=ref.get("error").toString();

                if(result.equals("false")){
                    JSONObject authObj=ref.getJSONObject("result");
                    String auth=authObj.getString("mb_auth");
                    if(auth.equals("active")){
                        Toast.makeText(TEST_WRITE_ACTIVITY.this, "Welcome to Collabo!", Toast.LENGTH_SHORT).show();
                        Intent intent =new Intent(TEST_WRITE_ACTIVITY.this, MainActivity.class);
                        intent.putExtra("Login_Activity",true);// 로딩및 로그인 완료  플래그 전달
                        startActivity(intent);
                        return;
                    }

                }

            }catch (Exception e) {
                Log.e("JSON ERROR",e.getMessage().toString());
                Toast.makeText(TEST_WRITE_ACTIVITY.this, "JSON 파싱중 오류 발생", Toast.LENGTH_SHORT).show();
            }
            finally {
                Log.e("FINNALLY RESULT",WRITE_INFO);
            }
        }

    }
}
